/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/akiry-player-console/akiry-player-console.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { AkiryPlayerConsoleService } from '../../services/akiry-player-console/akiry-player-console.service';
export class AkiryPlayerConsoleComponent {
    /**
     * @param {?} akiryConsole
     */
    constructor(akiryConsole) {
        this.akiryConsole = akiryConsole;
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
}
AkiryPlayerConsoleComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-akiry-player-console',
                template: "<section>\n    <h4>Akiry Secret Console Logs</h4>\n    <div class='fixed-infos'>\n        {{akiryConsole.playerStatusInfos}}\n    </div>\n    <div class='rolling-infos'>\n        <p *ngFor='let log of akiryConsole.logs'>{{log}}</p>\n    </div>\n</section>",
                styles: ["section{position:fixed!important;top:20vh!important;left:5%!important;z-index:100;background-color:rgba(0,0,0,.5);color:#fff;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-align:start;align-items:flex-start;-webkit-box-pack:start;justify-content:flex-start;width:90%;margin:0;height:60vh}.fixed-infos{width:100%;font-size:12px;white-space:pre-line}.rolling-infos{overflow-y:scroll;width:100%}"]
            }] }
];
/** @nocollapse */
AkiryPlayerConsoleComponent.ctorParameters = () => [
    { type: AkiryPlayerConsoleService }
];
if (false) {
    /** @type {?} */
    AkiryPlayerConsoleComponent.prototype.akiryConsole;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWtpcnktcGxheWVyLWNvbnNvbGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWtpcnktcGxheWVyLyIsInNvdXJjZXMiOlsibGliL2NvbXBvbmVudHMvYWtpcnktcGxheWVyLWNvbnNvbGUvYWtpcnktcGxheWVyLWNvbnNvbGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUNsRCxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxrRUFBa0UsQ0FBQztBQVM3RyxNQUFNLE9BQU8sMkJBQTJCOzs7O0lBQ3RDLFlBQ1MsWUFBdUM7UUFBdkMsaUJBQVksR0FBWixZQUFZLENBQTJCO0lBQzVDLENBQUM7Ozs7SUFFRSxRQUFRLEtBQUssQ0FBQzs7O1lBWnRCLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsMEJBQTBCO2dCQUNwQywyUUFBb0Q7O2FBSXJEOzs7O1lBUlEseUJBQXlCOzs7O0lBVzlCLG1EQUE4QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBa2lyeVBsYXllckNvbnNvbGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvYWtpcnktcGxheWVyLWNvbnNvbGUvYWtpcnktcGxheWVyLWNvbnNvbGUuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi1ha2lyeS1wbGF5ZXItY29uc29sZScsXG4gIHRlbXBsYXRlVXJsOiAnLi9ha2lyeS1wbGF5ZXItY29uc29sZS5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogW1xuICAgICcuL2FraXJ5LXBsYXllci1jb25zb2xlLmNvbXBvbmVudC5zYXNzJyxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgQWtpcnlQbGF5ZXJDb25zb2xlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGFraXJ5Q29uc29sZTogQWtpcnlQbGF5ZXJDb25zb2xlU2VydmljZSxcbiAgKSB7IH1cblxuICBwdWJsaWMgbmdPbkluaXQoKSB7IH1cbn1cbiJdfQ==