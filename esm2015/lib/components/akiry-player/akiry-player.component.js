/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/akiry-player/akiry-player.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild, Renderer2, Input, ElementRef } from '@angular/core';
import { AkiryPlayerDirective } from '../../directives/akiry-player/akiry-player.directive';
import { AkiryPlayerConsoleService } from '../../services/akiry-player-console/akiry-player-console.service';
import { LoadingCircleComponent } from '../loading-circle/loading-circle.component';
/** @type {?} */
export const DEFAULT_TIME_TO_HIDE_CONTROLS = 3000;
/**
 * @record
 */
export function ControlsListener() { }
if (false) {
    /** @type {?|undefined} */
    ControlsListener.prototype.showControls;
    /** @type {?|undefined} */
    ControlsListener.prototype.timeToHide;
}
export class AkiryPlayerComponent {
    /**
     * @param {?} akiryConsole
     * @param {?} _renderer
     */
    constructor(akiryConsole, _renderer) {
        this.akiryConsole = akiryConsole;
        this._renderer = _renderer;
        this._consoleCallerCount = 0;
        this._timeToHideControls = 0;
        this._isShowing = false;
        this.setConsoleCaller();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.setEvents();
        this.setPlayerInternalCallbacks();
    }
    /**
     * @return {?}
     */
    play() {
        this._akiryPlayer.playOrPause();
    }
    /**
     * @param {?} url
     * @param {?} opts
     * @return {?}
     */
    load(url, opts) {
        this._akiryPlayer.load(url, opts);
    }
    /**
     * @return {?}
     */
    release() {
        this._akiryPlayer.release();
    }
    /**
     * @private
     * @return {?}
     */
    setConsoleCaller() {
        document.addEventListener('touchstart', (/**
         * @param {?} zEvent
         * @return {?}
         */
        (zEvent) => {
            if (this._consoleCallerCount > 1) {
                this.akiryConsole.isVisible = !this.akiryConsole.isVisible;
                this._consoleCallerCount = 0;
            }
            this._lastClickInit = new Date();
        }));
        document.addEventListener('touchend', (/**
         * @param {?} zEvent
         * @return {?}
         */
        (zEvent) => {
            if (this._lastClickInit && Date.now() - this._lastClickInit.valueOf() > 4000) {
                this._consoleCallerCount++;
            }
            this._lastClickInit = undefined;
        }));
        document.addEventListener('keydown', (/**
         * @param {?} zEvent
         * @return {?}
         */
        (zEvent) => {
            // console.log('keydown - SHOW')
            this.showControls();
            if (zEvent.ctrlKey && zEvent.altKey && zEvent.code === 'KeyD') {
                this.akiryConsole.isVisible = !this.akiryConsole.isVisible;
            }
        }));
    }
    /**
     * @private
     * @return {?}
     */
    setEvents() {
        /** @type {?} */
        const section = this.section.nativeElement;
        section.addEventListener('mouseover', (/**
         * @param {?} zEvent
         * @return {?}
         */
        (zEvent) => {
            // console.log('over - SHOW')
            this.showControls();
        }));
        section.addEventListener('mousemove', (/**
         * @param {?} zEvent
         * @return {?}
         */
        (zEvent) => {
            // console.log('move - SHOW')
            this.showControls();
        }));
        // section.addEventListener('mouseleave', (zEvent) => {
        //   // console.log('leave - HIDE')
        //   this.hideControls();
        // });
        section.addEventListener('click', (/**
         * @param {?} zEvent
         * @return {?}
         */
        (zEvent) => {
            // this._akiryPlayer.playOrPause();
            this.showControls();
        }));
    }
    /**
     * @return {?}
     */
    showControls() {
        this._timeToHideControls =
            Date.now() + (this.controlsListener && this.controlsListener.timeToHide ?
                this.controlsListener.timeToHide : DEFAULT_TIME_TO_HIDE_CONTROLS);
        if (!this._isShowing) {
            this._renderer.setStyle(this.topControls.nativeElement, 'opacity', 1);
            this._renderer.setStyle(this.bottomControls.nativeElement, 'opacity', 1);
            if (this.controlsListener && this.controlsListener.showControls) {
                this.controlsListener.showControls(true);
            }
            this._isShowing = true;
            this.controlTimerWatch();
        }
    }
    /**
     * @return {?}
     */
    hideControls() {
        this._renderer.setStyle(this.topControls.nativeElement, 'opacity', 0);
        this._renderer.setStyle(this.bottomControls.nativeElement, 'opacity', 0);
        if (this.controlsListener && this.controlsListener.showControls) {
            this.controlsListener.showControls(false);
        }
        this._isShowing = false;
    }
    /**
     * @private
     * @return {?}
     */
    controlTimerWatch() {
        if (Date.now() > this._timeToHideControls) {
            this.hideControls();
        }
        else {
            setTimeout((/**
             * @return {?}
             */
            () => {
                this.controlTimerWatch();
            }), 500);
        }
    }
    /**
     * @return {?}
     */
    clickPlayButton() {
        this._akiryPlayer.playOrPause();
        return false;
    }
    /**
     * @return {?}
     */
    clickFullscreenButton() {
        this._akiryPlayer.fullscreen();
    }
    /**
     * @private
     * @return {?}
     */
    setPlayerInternalCallbacks() {
        this._akiryPlayer.setOnTimeUpdatedListener((/**
         * @param {?} currentTime
         * @param {?} bufferedTime
         * @return {?}
         */
        (currentTime, bufferedTime) => {
            /** @type {?} */
            const timeInBuffer = bufferedTime < currentTime ? 0 : bufferedTime - currentTime;
            this.libLoading.percentage = timeInBuffer / 15 < 1 ? Math.ceil(((5 + timeInBuffer) / 20) * 100) : 100;
            if (this.libLoading.percentage >= 100 && ((/** @type {?} */ (this.divLoading.nativeElement))).style.display !== 'none') {
                this._renderer.setStyle(this.divLoading.nativeElement, 'display', 'none');
                this._akiryPlayer.play();
            }
        }));
        this._akiryPlayer.setOnWaitingListener((/**
         * @return {?}
         */
        () => {
            this.startLoading();
        }));
    }
    /**
     * @private
     * @return {?}
     */
    startLoading() {
        this.libLoading.percentage = 0;
        this._renderer.setStyle(this.divLoading.nativeElement, 'display', 'flex');
        this._akiryPlayer.pause();
    }
}
AkiryPlayerComponent.decorators = [
    { type: Component, args: [{
                selector: 'lib-akiry-player',
                template: "<section #sectionControl>\n  <video libAkiryPlayer #akiryPlayer id='akiryPlayerId' preload playsinline controls>\n    <!-- <p class=\"vjs-no-js\">To view this video, please enable JavaScript, and consider upgrading to a web browser that <a href=\"http://videojs.com/html5-video-support/\">supports HTML5 video</a></p> -->\n    <!-- <source\n      src=\"http://10.42.0.1/videos/live3/live3.m3u8\"\n      type=\"application/x-mpegURL\"> -->\n  </video>\n\n  <div class='akiry-player-warns' #divLoading>\n    <div class='put-center'>\n      <lib-loading-circle #libLoading></lib-loading-circle>\n    </div>\n  </div>\n\n  <div class='akiry-player-controls'>\n    <div class='top' #topControls>\n\n    </div>\n    <div class='put-center' #centerControls>\n      \n    </div>\n    <div class='put-bottom'>\n      <div class='bottom' #bottomControls>\n        <button (click)='clickPlayButton()'>PLAY</button>\n        <button (click)='clickFullscreenButton()'>FULLSCREEN</button>\n      </div>\n      <div class='transparent-body'></div>\n    </div>\n  </div>\n\n  <!-- <lib-akiry-player-console *ngIf=\"akiryConsole.isVisible\"></lib-akiry-player-console> -->\n</section>",
                styles: ["section{background-color:#000;display:-webkit-box;display:flex;-webkit-box-pack:center;justify-content:center;width:100%;height:100vh}video{width:100%;height:100vh}.video-js{position:relative!important;width:100%!important;height:auto!important}.akiry-player-warns{position:absolute;width:100%;height:100vh;background-color:rgba(255,0,0,0);padding:3% 5% 0;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center}.akiry-player-warns .put-center{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center}.akiry-player-controls{position:absolute;width:100%;height:100vh;background-color:rgba(255,0,0,0);padding:3% 5% 0;display:none;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:justify;justify-content:space-between}.akiry-player-controls .top{background-color:rgba(0,255,0,.5);width:100%;height:100px;-webkit-transition:.2s linear;transition:.2s linear;opacity:0}.akiry-player-controls .put-center{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center}.akiry-player-controls .put-bottom{width:100%;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:reverse;flex-direction:column-reverse}.akiry-player-controls .put-bottom .transparent-body{width:100%;height:100%}.akiry-player-controls .put-bottom .bottom{background-color:rgba(0,0,255,.5);width:100%;height:70px;-webkit-transition:.2s linear;transition:.2s linear;opacity:0}"]
            }] }
];
/** @nocollapse */
AkiryPlayerComponent.ctorParameters = () => [
    { type: AkiryPlayerConsoleService },
    { type: Renderer2 }
];
AkiryPlayerComponent.propDecorators = {
    _akiryPlayer: [{ type: ViewChild, args: [AkiryPlayerDirective, { static: true },] }],
    topControls: [{ type: ViewChild, args: ['topControls', { static: true },] }],
    bottomControls: [{ type: ViewChild, args: ['bottomControls', { static: true },] }],
    section: [{ type: ViewChild, args: ['sectionControl', { static: true },] }],
    libLoading: [{ type: ViewChild, args: ['libLoading', { static: true },] }],
    divLoading: [{ type: ViewChild, args: ['divLoading', { static: true },] }],
    controlsListener: [{ type: Input }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._akiryPlayer;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.topControls;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.bottomControls;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.section;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.libLoading;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.divLoading;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.controlsListener;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._consoleCallerCount;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._lastClickInit;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._timeToHideControls;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._isShowing;
    /** @type {?} */
    AkiryPlayerComponent.prototype.akiryConsole;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWtpcnktcGxheWVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FraXJ5LXBsYXllci8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2FraXJ5LXBsYXllci9ha2lyeS1wbGF5ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxTQUFTLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0YsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDNUYsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sa0VBQWtFLENBQUM7QUFDN0csT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNENBQTRDLENBQUM7O0FBS3BGLE1BQU0sT0FBTyw2QkFBNkIsR0FBRyxJQUFJOzs7O0FBRWpELHNDQUdDOzs7SUFGQyx3Q0FBdUM7O0lBQ3ZDLHNDQUFvQjs7QUFVdEIsTUFBTSxPQUFPLG9CQUFvQjs7Ozs7SUFvQi9CLFlBQ1MsWUFBdUMsRUFDN0IsU0FBb0I7UUFEOUIsaUJBQVksR0FBWixZQUFZLENBQTJCO1FBQzdCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFSL0Isd0JBQW1CLEdBQUcsQ0FBQyxDQUFDO1FBR3hCLHdCQUFtQixHQUFHLENBQUMsQ0FBQztRQUN4QixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBTXpCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzFCLENBQUM7Ozs7SUFFTSxRQUFRO1FBQ2IsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQywwQkFBMEIsRUFBRSxDQUFDO0lBQ3BDLENBQUM7Ozs7SUFFTSxJQUFJO1FBQ1QsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNsQyxDQUFDOzs7Ozs7SUFFTSxJQUFJLENBQUMsR0FBVyxFQUFFLElBQXdCO1FBQy9DLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDOzs7O0lBRU0sT0FBTztRQUNaLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDOUIsQ0FBQzs7Ozs7SUFFTyxnQkFBZ0I7UUFDdEIsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFlBQVk7Ozs7UUFBRSxDQUFDLE1BQU0sRUFBRSxFQUFFO1lBQ2pELElBQUksSUFBSSxDQUFDLG1CQUFtQixHQUFHLENBQUMsRUFBRTtnQkFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFDM0QsSUFBSSxDQUFDLG1CQUFtQixHQUFHLENBQUMsQ0FBQzthQUM5QjtZQUNELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUNuQyxDQUFDLEVBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVOzs7O1FBQUUsQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUMvQyxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxFQUFFLEdBQUcsSUFBSSxFQUFFO2dCQUM1RSxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQzthQUM1QjtZQUNELElBQUksQ0FBQyxjQUFjLEdBQUcsU0FBUyxDQUFDO1FBQ2xDLENBQUMsRUFBQyxDQUFDO1FBRUgsUUFBUSxDQUFDLGdCQUFnQixDQUFDLFNBQVM7Ozs7UUFBRSxDQUFDLE1BQU0sRUFBRSxFQUFFO1lBQzlDLGdDQUFnQztZQUNoQyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDcEIsSUFBSSxNQUFNLENBQUMsT0FBTyxJQUFJLE1BQU0sQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxNQUFNLEVBQUU7Z0JBQzdELElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUM7YUFDNUQ7UUFDSCxDQUFDLEVBQUMsQ0FBQztJQUNMLENBQUM7Ozs7O0lBRU8sU0FBUzs7Y0FDVCxPQUFPLEdBQWdCLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYTtRQUV2RCxPQUFPLENBQUMsZ0JBQWdCLENBQUMsV0FBVzs7OztRQUFFLENBQUMsTUFBTSxFQUFFLEVBQUU7WUFDL0MsNkJBQTZCO1lBQzdCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUN0QixDQUFDLEVBQUMsQ0FBQztRQUVILE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXOzs7O1FBQUUsQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUMvQyw2QkFBNkI7WUFDN0IsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3RCLENBQUMsRUFBQyxDQUFDO1FBRUgsdURBQXVEO1FBQ3ZELG1DQUFtQztRQUNuQyx5QkFBeUI7UUFDekIsTUFBTTtRQUVOLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPOzs7O1FBQUUsQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUMzQyxtQ0FBbUM7WUFDbkMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3RCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVNLFlBQVk7UUFDakIsSUFBSSxDQUFDLG1CQUFtQjtZQUN0QixJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN2RSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO1FBRXRFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUN0RSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDekUsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRTtnQkFDL0QsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMxQztZQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQzFCO0lBQ0gsQ0FBQzs7OztJQUVNLFlBQVk7UUFDakIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN6RSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFO1lBQy9ELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDM0M7UUFDRCxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztJQUMxQixDQUFDOzs7OztJQUVPLGlCQUFpQjtRQUN2QixJQUFJLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDekMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1NBQ3JCO2FBQU07WUFDTCxVQUFVOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFDM0IsQ0FBQyxHQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ1Q7SUFDSCxDQUFDOzs7O0lBRU0sZUFBZTtRQUNwQixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRWhDLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7OztJQUVNLHFCQUFxQjtRQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBRU8sMEJBQTBCO1FBQ2hDLElBQUksQ0FBQyxZQUFZLENBQUMsd0JBQXdCOzs7OztRQUFDLENBQUMsV0FBVyxFQUFFLFlBQVksRUFBRSxFQUFFOztrQkFDakUsWUFBWSxHQUFHLFlBQVksR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxHQUFHLFdBQVc7WUFDaEYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEdBQUcsWUFBWSxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxZQUFZLENBQUMsR0FBRyxFQUFFLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO1lBQ3RHLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLElBQUksR0FBRyxJQUFJLENBQUMsbUJBQUEsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQWUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEtBQUssTUFBTSxFQUFFO2dCQUNoSCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQzFFLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDMUI7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxZQUFZLENBQUMsb0JBQW9COzs7UUFBQyxHQUFHLEVBQUU7WUFDMUMsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3RCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFTyxZQUFZO1FBQ2xCLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7WUFwS0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLDJwQ0FBNEM7O2FBSTdDOzs7O1lBbkJRLHlCQUF5QjtZQUZLLFNBQVM7OzsyQkF1QjdDLFNBQVMsU0FBQyxvQkFBb0IsRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7MEJBRWhELFNBQVMsU0FBQyxhQUFhLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFOzZCQUN6QyxTQUFTLFNBQUMsZ0JBQWdCLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFO3NCQUU1QyxTQUFTLFNBQUMsZ0JBQWdCLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFO3lCQUU1QyxTQUFTLFNBQUMsWUFBWSxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTt5QkFDeEMsU0FBUyxTQUFDLFlBQVksRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7K0JBR3hDLEtBQUs7Ozs7Ozs7SUFYTiw0Q0FBdUc7Ozs7O0lBRXZHLDJDQUF5RTs7Ozs7SUFDekUsOENBQStFOzs7OztJQUUvRSx1Q0FBd0U7Ozs7O0lBRXhFLDBDQUErRjs7Ozs7SUFDL0YsMENBQW1GOzs7OztJQUduRixnREFBNkQ7Ozs7O0lBRTdELG1EQUFnQzs7Ozs7SUFDaEMsOENBQTZCOzs7OztJQUU3QixtREFBZ0M7Ozs7O0lBQ2hDLDBDQUEyQjs7SUFHekIsNENBQThDOzs7OztJQUM5Qyx5Q0FBcUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBSZW5kZXJlcjIsIElucHV0LCBFbGVtZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBa2lyeVBsYXllckRpcmVjdGl2ZSB9IGZyb20gJy4uLy4uL2RpcmVjdGl2ZXMvYWtpcnktcGxheWVyL2FraXJ5LXBsYXllci5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgQWtpcnlQbGF5ZXJDb25zb2xlU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FraXJ5LXBsYXllci1jb25zb2xlL2FraXJ5LXBsYXllci1jb25zb2xlLnNlcnZpY2UnO1xuaW1wb3J0IHsgTG9hZGluZ0NpcmNsZUNvbXBvbmVudCB9IGZyb20gJy4uL2xvYWRpbmctY2lyY2xlL2xvYWRpbmctY2lyY2xlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBBa2lyeVBsYXllck9wdGlvbnMgfSBmcm9tICcuLi8uLi9kaXJlY3RpdmVzL2FraXJ5LXBsYXllci9pbnRlcmZhY2VzJztcblxuZGVjbGFyZSBjb25zdCBkb2N1bWVudDogYW55O1xuXG5leHBvcnQgY29uc3QgREVGQVVMVF9USU1FX1RPX0hJREVfQ09OVFJPTFMgPSAzMDAwO1xuXG5leHBvcnQgaW50ZXJmYWNlIENvbnRyb2xzTGlzdGVuZXIge1xuICBzaG93Q29udHJvbHM/OiAoc2hvdzogYm9vbGVhbikgPT4gdm9pZDtcbiAgdGltZVRvSGlkZT86IG51bWJlcjtcbn1cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnbGliLWFraXJ5LXBsYXllcicsXG4gIHRlbXBsYXRlVXJsOiAnLi9ha2lyeS1wbGF5ZXIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFtcbiAgICAnLi9ha2lyeS1wbGF5ZXIuY29tcG9uZW50LnNhc3MnLFxuICBdLFxufSlcbmV4cG9ydCBjbGFzcyBBa2lyeVBsYXllckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gIEBWaWV3Q2hpbGQoQWtpcnlQbGF5ZXJEaXJlY3RpdmUsIHsgc3RhdGljOiB0cnVlIH0pIHByaXZhdGUgcmVhZG9ubHkgX2FraXJ5UGxheWVyOiBBa2lyeVBsYXllckRpcmVjdGl2ZTtcblxuICBAVmlld0NoaWxkKCd0b3BDb250cm9scycsIHsgc3RhdGljOiB0cnVlIH0pIHByaXZhdGUgcmVhZG9ubHkgdG9wQ29udHJvbHM7XG4gIEBWaWV3Q2hpbGQoJ2JvdHRvbUNvbnRyb2xzJywgeyBzdGF0aWM6IHRydWUgfSkgcHJpdmF0ZSByZWFkb25seSBib3R0b21Db250cm9scztcblxuICBAVmlld0NoaWxkKCdzZWN0aW9uQ29udHJvbCcsIHsgc3RhdGljOiB0cnVlIH0pIHByaXZhdGUgcmVhZG9ubHkgc2VjdGlvbjtcblxuICBAVmlld0NoaWxkKCdsaWJMb2FkaW5nJywgeyBzdGF0aWM6IHRydWUgfSkgcHJpdmF0ZSByZWFkb25seSBsaWJMb2FkaW5nOiBMb2FkaW5nQ2lyY2xlQ29tcG9uZW50O1xuICBAVmlld0NoaWxkKCdkaXZMb2FkaW5nJywgeyBzdGF0aWM6IHRydWUgfSkgcHJpdmF0ZSByZWFkb25seSBkaXZMb2FkaW5nOiBFbGVtZW50UmVmO1xuXG5cbiAgQElucHV0KCkgcHJpdmF0ZSByZWFkb25seSBjb250cm9sc0xpc3RlbmVyOiBDb250cm9sc0xpc3RlbmVyO1xuXG4gIHByaXZhdGUgX2NvbnNvbGVDYWxsZXJDb3VudCA9IDA7XG4gIHByaXZhdGUgX2xhc3RDbGlja0luaXQ6IERhdGU7XG5cbiAgcHJpdmF0ZSBfdGltZVRvSGlkZUNvbnRyb2xzID0gMDtcbiAgcHJpdmF0ZSBfaXNTaG93aW5nID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoXG4gICAgcHVibGljIGFraXJ5Q29uc29sZTogQWtpcnlQbGF5ZXJDb25zb2xlU2VydmljZSxcbiAgICBwcml2YXRlIHJlYWRvbmx5IF9yZW5kZXJlcjogUmVuZGVyZXIyLFxuICApIHtcbiAgICB0aGlzLnNldENvbnNvbGVDYWxsZXIoKTtcbiAgfVxuXG4gIHB1YmxpYyBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLnNldEV2ZW50cygpO1xuICAgIHRoaXMuc2V0UGxheWVySW50ZXJuYWxDYWxsYmFja3MoKTtcbiAgfVxuXG4gIHB1YmxpYyBwbGF5KCkge1xuICAgIHRoaXMuX2FraXJ5UGxheWVyLnBsYXlPclBhdXNlKCk7XG4gIH1cblxuICBwdWJsaWMgbG9hZCh1cmw6IHN0cmluZywgb3B0czogQWtpcnlQbGF5ZXJPcHRpb25zKSB7XG4gICAgdGhpcy5fYWtpcnlQbGF5ZXIubG9hZCh1cmwsIG9wdHMpO1xuICB9XG5cbiAgcHVibGljIHJlbGVhc2UoKSB7XG4gICAgdGhpcy5fYWtpcnlQbGF5ZXIucmVsZWFzZSgpO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRDb25zb2xlQ2FsbGVyKCkge1xuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNoc3RhcnQnLCAoekV2ZW50KSA9PiB7XG4gICAgICBpZiAodGhpcy5fY29uc29sZUNhbGxlckNvdW50ID4gMSkge1xuICAgICAgICB0aGlzLmFraXJ5Q29uc29sZS5pc1Zpc2libGUgPSAhdGhpcy5ha2lyeUNvbnNvbGUuaXNWaXNpYmxlO1xuICAgICAgICB0aGlzLl9jb25zb2xlQ2FsbGVyQ291bnQgPSAwO1xuICAgICAgfVxuICAgICAgdGhpcy5fbGFzdENsaWNrSW5pdCA9IG5ldyBEYXRlKCk7XG4gICAgfSk7XG5cbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaGVuZCcsICh6RXZlbnQpID0+IHtcbiAgICAgIGlmICh0aGlzLl9sYXN0Q2xpY2tJbml0ICYmIERhdGUubm93KCkgLSB0aGlzLl9sYXN0Q2xpY2tJbml0LnZhbHVlT2YoKSA+IDQwMDApIHtcbiAgICAgICAgdGhpcy5fY29uc29sZUNhbGxlckNvdW50Kys7XG4gICAgICB9XG4gICAgICB0aGlzLl9sYXN0Q2xpY2tJbml0ID0gdW5kZWZpbmVkO1xuICAgIH0pO1xuXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigna2V5ZG93bicsICh6RXZlbnQpID0+IHtcbiAgICAgIC8vIGNvbnNvbGUubG9nKCdrZXlkb3duIC0gU0hPVycpXG4gICAgICB0aGlzLnNob3dDb250cm9scygpO1xuICAgICAgaWYgKHpFdmVudC5jdHJsS2V5ICYmIHpFdmVudC5hbHRLZXkgJiYgekV2ZW50LmNvZGUgPT09ICdLZXlEJykge1xuICAgICAgICB0aGlzLmFraXJ5Q29uc29sZS5pc1Zpc2libGUgPSAhdGhpcy5ha2lyeUNvbnNvbGUuaXNWaXNpYmxlO1xuICAgICAgfVxuICAgIH0pO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRFdmVudHMoKSB7XG4gICAgY29uc3Qgc2VjdGlvbjogSFRNTEVsZW1lbnQgPSB0aGlzLnNlY3Rpb24ubmF0aXZlRWxlbWVudDtcblxuICAgIHNlY3Rpb24uYWRkRXZlbnRMaXN0ZW5lcignbW91c2VvdmVyJywgKHpFdmVudCkgPT4ge1xuICAgICAgLy8gY29uc29sZS5sb2coJ292ZXIgLSBTSE9XJylcbiAgICAgIHRoaXMuc2hvd0NvbnRyb2xzKCk7XG4gICAgfSk7XG5cbiAgICBzZWN0aW9uLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlbW92ZScsICh6RXZlbnQpID0+IHtcbiAgICAgIC8vIGNvbnNvbGUubG9nKCdtb3ZlIC0gU0hPVycpXG4gICAgICB0aGlzLnNob3dDb250cm9scygpO1xuICAgIH0pO1xuXG4gICAgLy8gc2VjdGlvbi5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWxlYXZlJywgKHpFdmVudCkgPT4ge1xuICAgIC8vICAgLy8gY29uc29sZS5sb2coJ2xlYXZlIC0gSElERScpXG4gICAgLy8gICB0aGlzLmhpZGVDb250cm9scygpO1xuICAgIC8vIH0pO1xuXG4gICAgc2VjdGlvbi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsICh6RXZlbnQpID0+IHtcbiAgICAgIC8vIHRoaXMuX2FraXJ5UGxheWVyLnBsYXlPclBhdXNlKCk7XG4gICAgICB0aGlzLnNob3dDb250cm9scygpO1xuICAgIH0pO1xuICB9XG5cbiAgcHVibGljIHNob3dDb250cm9scygpIHtcbiAgICB0aGlzLl90aW1lVG9IaWRlQ29udHJvbHMgPVxuICAgICAgRGF0ZS5ub3coKSArICh0aGlzLmNvbnRyb2xzTGlzdGVuZXIgJiYgdGhpcy5jb250cm9sc0xpc3RlbmVyLnRpbWVUb0hpZGUgP1xuICAgICAgICB0aGlzLmNvbnRyb2xzTGlzdGVuZXIudGltZVRvSGlkZSA6IERFRkFVTFRfVElNRV9UT19ISURFX0NPTlRST0xTKTtcblxuICAgIGlmICghdGhpcy5faXNTaG93aW5nKSB7XG4gICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh0aGlzLnRvcENvbnRyb2xzLm5hdGl2ZUVsZW1lbnQsICdvcGFjaXR5JywgMSk7XG4gICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmJvdHRvbUNvbnRyb2xzLm5hdGl2ZUVsZW1lbnQsICdvcGFjaXR5JywgMSk7XG4gICAgICBpZiAodGhpcy5jb250cm9sc0xpc3RlbmVyICYmIHRoaXMuY29udHJvbHNMaXN0ZW5lci5zaG93Q29udHJvbHMpIHtcbiAgICAgICAgdGhpcy5jb250cm9sc0xpc3RlbmVyLnNob3dDb250cm9scyh0cnVlKTtcbiAgICAgIH1cbiAgICAgIHRoaXMuX2lzU2hvd2luZyA9IHRydWU7XG4gICAgICB0aGlzLmNvbnRyb2xUaW1lcldhdGNoKCk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGhpZGVDb250cm9scygpIHtcbiAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh0aGlzLnRvcENvbnRyb2xzLm5hdGl2ZUVsZW1lbnQsICdvcGFjaXR5JywgMCk7XG4gICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUodGhpcy5ib3R0b21Db250cm9scy5uYXRpdmVFbGVtZW50LCAnb3BhY2l0eScsIDApO1xuICAgIGlmICh0aGlzLmNvbnRyb2xzTGlzdGVuZXIgJiYgdGhpcy5jb250cm9sc0xpc3RlbmVyLnNob3dDb250cm9scykge1xuICAgICAgdGhpcy5jb250cm9sc0xpc3RlbmVyLnNob3dDb250cm9scyhmYWxzZSk7XG4gICAgfVxuICAgIHRoaXMuX2lzU2hvd2luZyA9IGZhbHNlO1xuICB9XG5cbiAgcHJpdmF0ZSBjb250cm9sVGltZXJXYXRjaCgpIHtcbiAgICBpZiAoRGF0ZS5ub3coKSA+IHRoaXMuX3RpbWVUb0hpZGVDb250cm9scykge1xuICAgICAgdGhpcy5oaWRlQ29udHJvbHMoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIHRoaXMuY29udHJvbFRpbWVyV2F0Y2goKTtcbiAgICAgIH0sIDUwMCk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGNsaWNrUGxheUJ1dHRvbigpIHtcbiAgICB0aGlzLl9ha2lyeVBsYXllci5wbGF5T3JQYXVzZSgpO1xuXG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgcHVibGljIGNsaWNrRnVsbHNjcmVlbkJ1dHRvbigpIHtcbiAgICB0aGlzLl9ha2lyeVBsYXllci5mdWxsc2NyZWVuKCk7XG4gIH1cblxuICBwcml2YXRlIHNldFBsYXllckludGVybmFsQ2FsbGJhY2tzKCkge1xuICAgIHRoaXMuX2FraXJ5UGxheWVyLnNldE9uVGltZVVwZGF0ZWRMaXN0ZW5lcigoY3VycmVudFRpbWUsIGJ1ZmZlcmVkVGltZSkgPT4ge1xuICAgICAgY29uc3QgdGltZUluQnVmZmVyID0gYnVmZmVyZWRUaW1lIDwgY3VycmVudFRpbWUgPyAwIDogYnVmZmVyZWRUaW1lIC0gY3VycmVudFRpbWU7XG4gICAgICB0aGlzLmxpYkxvYWRpbmcucGVyY2VudGFnZSA9IHRpbWVJbkJ1ZmZlciAvIDE1IDwgMSA/IE1hdGguY2VpbCgoKDUgKyB0aW1lSW5CdWZmZXIpIC8gMjApICogMTAwKSA6IDEwMDtcbiAgICAgIGlmICh0aGlzLmxpYkxvYWRpbmcucGVyY2VudGFnZSA+PSAxMDAgJiYgKHRoaXMuZGl2TG9hZGluZy5uYXRpdmVFbGVtZW50IGFzIEhUTUxFbGVtZW50KS5zdHlsZS5kaXNwbGF5ICE9PSAnbm9uZScpIHtcbiAgICAgICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUodGhpcy5kaXZMb2FkaW5nLm5hdGl2ZUVsZW1lbnQsICdkaXNwbGF5JywgJ25vbmUnKTtcbiAgICAgICAgdGhpcy5fYWtpcnlQbGF5ZXIucGxheSgpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgdGhpcy5fYWtpcnlQbGF5ZXIuc2V0T25XYWl0aW5nTGlzdGVuZXIoKCkgPT4ge1xuICAgICAgdGhpcy5zdGFydExvYWRpbmcoKTtcbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgc3RhcnRMb2FkaW5nKCkge1xuICAgIHRoaXMubGliTG9hZGluZy5wZXJjZW50YWdlID0gMDtcbiAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh0aGlzLmRpdkxvYWRpbmcubmF0aXZlRWxlbWVudCwgJ2Rpc3BsYXknLCAnZmxleCcpO1xuICAgIHRoaXMuX2FraXJ5UGxheWVyLnBhdXNlKCk7XG4gIH1cbn1cbiJdfQ==