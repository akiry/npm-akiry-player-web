/**
 * @fileoverview added by tsickle
 * Generated from: lib/libs/utils/ios.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function iOSToolsInterface() { }
if (false) {
    /** @type {?|undefined} */
    iOSToolsInterface.prototype.processStringForPlatform;
    /** @type {?|undefined} */
    iOSToolsInterface.prototype.iOS;
}
/** @type {?} */
var iOSTools = {};
iOSTools.processStringForPlatform = (/**
 * @param {?} url
 * @return {?}
 */
(url) => {
    /** @type {?} */
    const names = url.split('/');
    /** @type {?} */
    let newUrl = [];
    names.forEach((/**
     * @param {?} name
     * @return {?}
     */
    (name) => {
        if (name === 'videos') {
            newUrl.push('ios');
        }
        newUrl.push(name);
    }));
    url = newUrl.join('/');
    return url;
});
iOSTools.iOS = (/**
 * @return {?}
 */
() => {
    /** @type {?} */
    var iDevices = [
        'iPad Simulator',
        'iPhone Simulator',
        'iPod Simulator',
        'iPad',
        'iPhone',
        'iPod'
    ];
    if (!!navigator.platform) {
        while (iDevices.length) {
            if (navigator.platform === iDevices.pop()) {
                return true;
            }
        }
    }
    return false;
});
export { iOSTools };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW9zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vYWtpcnktcGxheWVyLyIsInNvdXJjZXMiOlsibGliL2xpYnMvdXRpbHMvaW9zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsdUNBR0M7OztJQUZHLHFEQUEwQjs7SUFDMUIsZ0NBQUs7OztJQUdMLFFBQVEsR0FBc0IsRUFBRTtBQUVwQyxRQUFRLENBQUMsd0JBQXdCOzs7O0FBQUcsQ0FBQyxHQUFXLEVBQVUsRUFBRTs7VUFDbEQsS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOztRQUN4QixNQUFNLEdBQUcsRUFBRTtJQUNmLEtBQUssQ0FBQyxPQUFPOzs7O0lBQUMsQ0FBQyxJQUFZLEVBQUUsRUFBRTtRQUMzQixJQUFHLElBQUksS0FBSyxRQUFRLEVBQUU7WUFDbEIsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN0QjtRQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdEIsQ0FBQyxFQUFDLENBQUM7SUFFSCxHQUFHLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUV2QixPQUFPLEdBQUcsQ0FBQztBQUNmLENBQUMsQ0FBQSxDQUFBO0FBRUQsUUFBUSxDQUFDLEdBQUc7OztBQUFHLEdBQUcsRUFBRTs7UUFDWixRQUFRLEdBQUc7UUFDZixnQkFBZ0I7UUFDaEIsa0JBQWtCO1FBQ2xCLGdCQUFnQjtRQUNoQixNQUFNO1FBQ04sUUFBUTtRQUNSLE1BQU07S0FDTDtJQUVELElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLEVBQUU7UUFDdEIsT0FBTyxRQUFRLENBQUMsTUFBTSxFQUFFO1lBQ3BCLElBQUksU0FBUyxDQUFDLFFBQVEsS0FBSyxRQUFRLENBQUMsR0FBRyxFQUFFLEVBQUM7Z0JBQUUsT0FBTyxJQUFJLENBQUM7YUFBRTtTQUM3RDtLQUNKO0lBRUQsT0FBTyxLQUFLLENBQUM7QUFDakIsQ0FBQyxDQUFBLENBQUE7QUFFRCxPQUFPLEVBQUUsUUFBUSxFQUFFLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIGlPU1Rvb2xzSW50ZXJmYWNlIHtcbiAgICBwcm9jZXNzU3RyaW5nRm9yUGxhdGZvcm0/O1xuICAgIGlPUz87XG59XG5cbnZhciBpT1NUb29sczogaU9TVG9vbHNJbnRlcmZhY2UgPSB7fTtcblxuaU9TVG9vbHMucHJvY2Vzc1N0cmluZ0ZvclBsYXRmb3JtID0gKHVybDogc3RyaW5nKTogc3RyaW5nID0+IHtcbiAgICBjb25zdCBuYW1lcyA9IHVybC5zcGxpdCgnLycpO1xuICAgIGxldCBuZXdVcmwgPSBbXTtcbiAgICBuYW1lcy5mb3JFYWNoKChuYW1lOiBzdHJpbmcpID0+IHtcbiAgICAgICAgaWYobmFtZSA9PT0gJ3ZpZGVvcycpIHtcbiAgICAgICAgICAgIG5ld1VybC5wdXNoKCdpb3MnKTtcbiAgICAgICAgfVxuICAgICAgICBuZXdVcmwucHVzaChuYW1lKTtcbiAgICB9KTtcblxuICAgIHVybCA9IG5ld1VybC5qb2luKCcvJyk7XG5cbiAgICByZXR1cm4gdXJsO1xufVxuXG5pT1NUb29scy5pT1MgPSAoKSA9PiB7XG4gICAgdmFyIGlEZXZpY2VzID0gW1xuICAgICdpUGFkIFNpbXVsYXRvcicsXG4gICAgJ2lQaG9uZSBTaW11bGF0b3InLFxuICAgICdpUG9kIFNpbXVsYXRvcicsXG4gICAgJ2lQYWQnLFxuICAgICdpUGhvbmUnLFxuICAgICdpUG9kJ1xuICAgIF07XG5cbiAgICBpZiAoISFuYXZpZ2F0b3IucGxhdGZvcm0pIHtcbiAgICAgICAgd2hpbGUgKGlEZXZpY2VzLmxlbmd0aCkge1xuICAgICAgICAgICAgaWYgKG5hdmlnYXRvci5wbGF0Zm9ybSA9PT0gaURldmljZXMucG9wKCkpeyByZXR1cm4gdHJ1ZTsgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGZhbHNlO1xufVxuXG5leHBvcnQgeyBpT1NUb29scyB9Il19