/**
 * @fileoverview added by tsickle
 * Generated from: lib/directives/akiry-player/akiry-player.directive.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Directive, ElementRef, Renderer2 } from '@angular/core';
import { isUndefined } from 'util';
import { C } from '../../config/c';
import { iOSTools } from '../../libs/utils/ios';
import { AkiryPlayerConsoleService } from '../../services/akiry-player-console/akiry-player-console.service';
// import * as videojs from '../../libs/min/videojs/dist/video.min.js';
/** @type {?} */
const videojs = require('../../libs/min/videojs/dist/video.min.js');
/** @type {?} */
const DEFAULT_INITIAL_PLAYLIST = 1;
// TODO: Importar MUX.JS
/**
 * Importing VideoJS
 */
// tslint:disable-next-line:no-require-imports
// const videojs = require('../../libs/min/videojs/dist/video.min.js'); // tslint:disable-line:no-var-requires
((/** @type {?} */ (window))).videojs = videojs;
// tslint:disable-next-line:no-require-imports
/**
 * Importing ShakaPlayer
 * @type {?}
 */
const shaka = require('../../libs/min/shaka/shaka-player.compiled.js');
// tslint:disable-line:no-var-requires
// tslint:disable-next-line:no-require-imports
require('../../libs/min/shaka/shaka-player.compiled.debug.js'); // tslint:disable-line:no-var-requires
// tslint:disable-line:no-var-requires
export class AkiryPlayerDirective {
    /**
     * @param {?} el
     * @param {?} _renderer
     * @param {?} _akiryConsole
     */
    constructor(el, _renderer, _akiryConsole) {
        this._renderer = _renderer;
        this._akiryConsole = _akiryConsole;
        this._internalListeners = {};
        this.currentPlaylist = DEFAULT_INITIAL_PLAYLIST;
        this._videoTag = el.nativeElement;
        // console.log('VideoJS lib: ', videojs);
        // console.log('Shaka lib: ', shaka);
    }
    /**
     * Play, if the player is paused, and pause, if playing
     * @return {?}
     */
    playOrPause() {
        if (this._videoTag !== undefined) {
            if (this._videoTag.paused) {
                if (iOSTools.iOS()) {
                    switch (this._playerOptions.player) {
                        case C.PLAYERS.VIDEOJS:
                            this._videojs.play();
                            break;
                        default:
                            this._videoTag.play();
                            break;
                    }
                }
                else {
                    this._videoTag.play();
                }
            }
            else {
                this._videoTag.pause();
            }
        }
        else {
            console.error('Player is null');
        }
    }
    /**
     * @return {?}
     */
    fullscreen() {
        if (this._videoTag.requestFullscreen) {
            this._videoTag.requestFullscreen();
        }
        else if (((/** @type {?} */ (this._videoTag))).mozRequestFullScreen) {
            ((/** @type {?} */ (this._videoTag))).mozRequestFullScreen();
        }
        else if (((/** @type {?} */ (this._videoTag))).webkitRequestFullscreen) {
            ((/** @type {?} */ (this._videoTag))).webkitRequestFullscreen();
        }
        if (iOSTools.iOS()) {
            if (this._videoTag.webkitEnterFullscreen) {
                this._videoTag.webkitEnterFullscreen();
            }
            if (((/** @type {?} */ (this._videoTag))).enterFullscreen) {
                ((/** @type {?} */ (this._videoTag))).enterFullscreen();
            }
        }
    }
    /**
     * Play only
     * @return {?}
     */
    play() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this._videoTag !== undefined) {
                if (iOSTools.iOS()) {
                    this._videoTag.play()
                        .then((/**
                     * @return {?}
                     */
                    () => { }))
                        .catch((/**
                     * @param {?} error
                     * @return {?}
                     */
                    (error) => {
                        console.error('Error on autoplay video', error);
                        this._videoTag.muted = true;
                        this._videoTag.play();
                    }));
                }
                else {
                    this._videoTag.play() // FIXME: not working when refresh the page
                        .then((/**
                     * @return {?}
                     */
                    () => { }))
                        .catch((/**
                     * @param {?} error
                     * @return {?}
                     */
                    (error) => {
                        console.error('Error on autoplay video', error);
                        this._videoTag.muted = true;
                        this._videoTag.play();
                    }));
                }
            }
            else {
                console.error('Player is null');
            }
        });
    }
    /**
     * Pause only
     * @return {?}
     */
    pause() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            if (this._videoTag !== undefined) {
                this._videoTag.pause();
            }
            else {
                console.error('Player is null');
            }
        });
    }
    /**
     * Method to unbuild player, for release resources
     * @return {?}
     */
    release() {
        if (this._playerOptions) {
            switch (this._playerOptions.player) {
                case C.PLAYERS.VIDEOJS:
                    if (this._videojs) {
                        this._videojs.dispose();
                    }
                    break;
                default:
                    console.error('Nothing todo. There is not player selected.');
                    break;
            }
        }
    }
    /**
     * Method for build player and prepare to start.
     * @param {?} url
     * @param {?} playerOptions
     * @return {?}
     */
    load(url, playerOptions) {
        this._playerOptions = playerOptions;
        // if (this._playerOptions.evaluator === undefined) {
        //   this._playerOptions.evaluator = new FixFormat(0, () => this._videojs);
        // }
        if (iOSTools.iOS()) {
            this.loadNativeFor_iOS(url);
        }
        else {
            switch (this._playerOptions.player) {
                case C.PLAYERS.SHAKA:
                    this.loadShaka(url);
                    break;
                case C.PLAYERS.VIDEOJS:
                    this.loadVideoJs(url);
                    break;
                default:
                    console.error('There is not player selected. Select one with `opt.player = C.PLAYERS.${SELECTED_PLAYER}`.');
                    break;
            }
        }
        this.generalBuild();
    }
    /**
     * *****************************************************
     *
     *
     * GENERAL AREA
     *
     *
     * ********************************************************
     * @private
     * @return {?}
     */
    generalBuild() {
        if (this._playerOptions.thumb) {
            this._videoTag.poster = this._playerOptions.thumb;
        }
        this.buildListeners();
        // set key 'space' to play/pause
        /** @type {?} */
        const oldOnKeyUp = document.body.onkeyup;
        document.body.onkeyup = (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            // console.log(event.key);
            switch (event.key) {
                case ' ' || 'Spacebar':
                    this.playOrPause();
                    break;
                case 'ArrowLeft':
                    this._videoTag.currentTime = this._videoTag.currentTime < 5 ? 0 : this._videoTag.currentTime - 5;
                    break;
                case 'ArrowRight':
                    this._videoTag.currentTime =
                        this._videoTag.currentTime > this._videoTag.duration - 5 ?
                            this._videoTag.currentTime :
                            this._videoTag.currentTime + 5;
                    break;
                case 'ArrowUp':
                    this._videoTag.volume = this._videoTag.volume > 0.9 ? 1 : this._videoTag.volume + 0.1;
                    break;
                case 'ArrowDown':
                    this._videoTag.volume = this._videoTag.volume < 0.1 ? 0 : this._videoTag.volume - 0.1;
                    break;
                default:
                    if (oldOnKeyUp) {
                        oldOnKeyUp(event);
                    }
                    break;
            }
        });
    }
    /**
     * @private
     * @return {?}
     */
    buildPlayerSkin() {
        /**
         * Insert personal company loader
         */
        // const spinners = document.getElementsByClassName('vjs-loading-spinner');
        // const spinner: Element = spinners && spinners.length > 0 ? spinners[0] : undefined;
        // if (spinner && this._playerOptions && this._playerOptions.controlsImgPath && this._playerOptions.controlsImgPath.loadingButton) {
        //   spinner.innerHTML =
        //     `<img src="${this._playerOptions.controlsImgPath.loadingButton}" class="vjs-loading-spinner-img" alt="Loading...">`;
        // }
        /**
         * Others controls powered by Akiry
         */
    }
    /**
     * @private
     * @return {?}
     */
    buildListeners() {
        // this._videoTag.on('timeupdate', () => {
        //   this._akiryConsole.addLogs('timeupdated');
        //   // this.previousTime = this.currentTime;
        //   // this.currentTime = this._videojs.cache_.currentTime;
        //   // //        console.log('timeUpdate: ' + this.currentTime);
        // this._videoTag.on('timeupdate', () => {
        //   this._akiryConsole.addLogs('timeupdated');
        //   // this.previousTime = this.currentTime;
        //   // this.currentTime = this._videojs.cache_.currentTime;
        //   // //        console.log('timeUpdate: ' + this.currentTime);
        //   // const buffered = this._videojs.buffered();
        //   // const bufferStart = buffered.start ? buffered.start(buffered.length - 1) : -1;
        //   // const bufferEnd = buffered.end ? buffered.end(buffered.length - 1) : -1;
        //   // this.onTimeUpdated(this.currentTime, bufferEnd);
        //   // // const bufferStartProt = buffered.__prot__.end ? buffered.__prot__.end(0) : 'não rolou';
        //   // //        console.log('buffered: ', bufferStart, bufferEnd, buffered.length);
        // });
        // this._videoTag.on('progress', () => {
        //   this._akiryConsole.addLogs('progress');
        // });
        /** @type {?} */
        const events = [
            'abort',
            'canplay',
            'canplaythrough',
            'durationchange',
            'emptied',
            'ended',
            'error',
            'loadeddata',
            'loadedmetadata',
            'loadstart',
            'pause',
            'play',
            'playing',
            // 'progress',         // Works on iOS - 5 and ever
            'ratechange',
            'seeked',
            'seeking',
            'stalled',
            'suspend',
            // 'timeupdate',       // Works on iOS - onplay 3 and while is playing
            // 'volumechange',     // Works on iOS - when volume change
            'waiting',
        ];
        for (const event of events) {
            if (iOSTools.iOS()) {
                this._videoTag.addEventListener(event, (/**
                 * @return {?}
                 */
                () => { this._akiryConsole.addLogs('Event: ' + event); }), false);
            }
            else {
                this._videoTag.addEventListener(event, (/**
                 * @return {?}
                 */
                () => { this._akiryConsole.addLogs('Event: ' + event); }), false);
            }
        }
        this._videoTag.addEventListener('progress', (/**
         * @return {?}
         */
        () => {
            this.onTimeUpdated();
        }), false);
        this._videoTag.addEventListener('waiting', (/**
         * @return {?}
         */
        () => {
            if (this._internalListeners.onWaiting) {
                this._internalListeners.onWaiting();
            }
        }));
        this._videoTag.addEventListener('ended', (/**
         * @return {?}
         */
        () => {
            if (this._playerOptions && this._playerOptions.callbacks && this._playerOptions.callbacks.onEnded) {
                this._playerOptions.callbacks.onEnded();
            }
        }));
        // this._videojs.on('progress', () => {
        //   this.previousTime = this.currentTime;
        //   this.currentTime = this._videojs.cache_.currentTime;
        //   //        console.log('timeUpdate: ' + this.currentTime);
        //   const buffered = this._videojs.buffered();
        //   const bufferStart = buffered.start ? buffered.start(buffered.length - 1) : -1;
        //   const bufferEnd = buffered.end ? buffered.end(buffered.length - 1) : -1;
        //   console.log('--', !this._isNotInitialBuffering, '--|--', bufferEnd > 0)
        //   if (!this._isNotInitialBuffering && bufferEnd > 0) {
        //     if (!this._videoTag.paused) {
        //       console.log('pause')
        //       this._isNotInitialBuffering = true;
        //       this._videojs.pause();
        //     } else {
        //       console.log('delayed play')
        //       this._delayedPlayed = true;
        //       // method to start initial buffering on iOS but it also autostart on others devices
        //       const playButtonBottom: HTMLButtonElement =
        //         Array.from(document.getElementsByClassName('vjs-play-control vjs-control vjs-button'))[0] as HTMLButtonElement;
        //       playButtonBottom.click();
        //       this._videojs.play().catch(() => {
        //         this._videoTag.volume = 0;
        //         this._delayedPlayed = false;
        //         this._isNotInitialBuffering = false;
        //       });
        //     }
        //   }
        //   this.onTimeUpdated(this.currentTime, bufferEnd);
        //   // const bufferStartProt = buffered.__prot__.end ? buffered.__prot__.end(0) : 'não rolou';
        //   //        console.log('buffered: ', bufferStart, bufferEnd, buffered.length);
        // });
        // this._videojs.on('seeking', () => {
        //   this.seekStart = this.previousTime;
        //   //        console.log('seeking: ' + this.currentTime);
        // });
        // this._videojs.on('seeked', () => {
        //   //        console.log('seeked from', this.seekStart, 'to', this.currentTime, '; delta:', this.currentTime - this.seekStart);
        // });
        // this._videojs.on('loadedmetadata', () => {
        //   this._akiryConsole.addLogs('Loaded Metadata.');
        //   //  console.log('loadedmetadata.');
        //   if (this._playerOptions.autoplay) {
        //     this.play();
        //   }
        // });
        // this._videojs.on('play', () => {
        //   this._akiryConsole.addLogs('play.');
        //   //      console.log('play.');
        // });
        // this._videojs.on('volumechange', () => {
        //   this._akiryConsole.addLogs('volumechange.');
        //   //      console.log('volumechange.');
        // });
        // this._videojs.on('useractive', () => {
        //   //      console.log('useractive.');
        // });
        // this._videojs.on('userinactive', () => {
        //   //      console.log('userinactive.');
        // });
        // this._videojs.on('error', () => {
        //   //      console.log('error.');
        //   if (this._playerOptions && this._playerOptions.callbacks && this._playerOptions.callbacks.onError) {
        //     this._playerOptions.callbacks.onError();
        //   }
        // });
        // this._videojs.on('pause', () => {
        //   //      console.log('pause.');
        // });
        // this._videojs.on('fullscreenchange', () => {
        //   //      console.log('fullscreenchange.');
        // });
        // this._videojs.on('waiting', () => {
        //   //      console.log('waiting.', this._videojs);
        // });
        // this._videojs.on('playing', () => {
        //   console.log('playing.', this._videojs);
        // });
        // this._videojs.on('suspend', () => {
        //   console.log('suspend.', this._videojs);
        // });
        // this._videojs.on('stalled', () => {
        //   console.log('stalled.', this._videojs);
        // });
        // this._videojs.on('ready', () => {
        //   //      console.log('ready.');
        // });
    }
    /**
     * @param {?} onTimeUpdate
     * @return {?}
     */
    setOnTimeUpdatedListener(onTimeUpdate) {
        this._internalListeners.onTimeUpdate = onTimeUpdate;
    }
    /**
     * @param {?} onWaiting
     * @return {?}
     */
    setOnWaitingListener(onWaiting) {
        this._internalListeners.onWaiting = onWaiting;
    }
    /**
     * @return {?}
     */
    onTimeUpdated() {
        /** @type {?} */
        const currentTime = this._videoTag.currentTime;
        /** @type {?} */
        const bufferedTime = this._videoTag.buffered && this._videoTag.buffered.length > 0 ? this._videoTag.buffered.end(0) : 0;
        /** @type {?} */
        const duration = this._videoTag.duration;
        /** @type {?} */
        const videoHeight = this._videoTag.videoHeight;
        this._akiryConsole.setPlayerStatus(currentTime, bufferedTime, duration, videoHeight);
        if (this._internalListeners.onTimeUpdate) {
            this._internalListeners.onTimeUpdate(currentTime, bufferedTime);
        }
    }
    /********************************************************
       *
       *
       * SHAKA AREA
       *
       *
       **********************************************************/
    /**
     * Private method for build Shaka player and prepare to start.
     * @private
     * @param {?} url
     * @return {?}
     */
    loadShaka(url) {
        // Install built-in polyfills to patch browser incompatibilities.
        shaka.polyfill.installAll();
        // Check to see if the browser supports the basic APIs Shaka needs.
        if (!shaka.Player.isBrowserSupported()) {
            // This browser does not have the minimum set of APIs we need.
            console.error('Browser not supported!');
            return;
        }
        // Everything looks good!
        switch (this._playerOptions.mode) {
            case C.TYPE.HLS:
                this.buildShakaPlayer(url);
                break;
            case C.TYPE.DASH:
                this.buildShakaPlayer(url);
                break;
            case C.TYPE.MP4:
                this.buildShakaPlayer(url);
                break;
            default:
                break;
        }
    }
    /**
     * @private
     * @param {?} url
     * @return {?}
     */
    buildShakaPlayer(url) {
        /** @type {?} */
        const onError = (/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            // Log the error.
            console.error('Error code', error.code, 'object', error);
        });
        /** @type {?} */
        const onErrorEvent = (/**
         * @param {?} event
         * @return {?}
         */
        (event) => {
            // Extract the shaka.util.Error object from the event.
            onError(event.detail);
        });
        // Create a Player instance.
        this._shaka = new shaka.Player(this._videoTag);
        // Attach player to the window to make it easy to access in the JS console.
        ((/** @type {?} */ (window))).player = this._shaka;
        // Listen for error events.
        this._shaka.addEventListener('error', onErrorEvent);
        // Configure buffer length
        this._shaka.configure({
            streaming: {
                bufferingGoal: 120,
                rebufferingGoal: 15,
                retryParameters: {
                    timeout: 0,
                    // timeout in ms, after which we abort; 0 means never
                    maxAttempts: 4,
                    // the maximum number of requests before we fail
                    baseDelay: 500,
                    // the base delay in ms between retries
                    backoffFactor: 2,
                    // the multiplicative backoff factor between retries
                    fuzzFactor: 0.5,
                },
            },
        });
        // Configure all requests
        this._shaka.getNetworkingEngine().registerRequestFilter((/**
         * @param {?} type
         * @param {?} request
         * @return {?}
         */
        (type, request) => {
            if (this._playerOptions.token) {
                if (!request.headers) {
                    request.header = {};
                }
                request.headers['Authorization'] = 'Bearer ' + this._playerOptions.token;
            }
        }));
        // Try to load a manifest.
        // This is an asynchronous process.
        this._shaka.load(url).then((/**
         * @return {?}
         */
        () => {
            // This runs if the asynchronous load is successful.
            console.info('The video has now been loaded!'); // tslint:disable-line:no-console
        })).catch(onError); // onError is executed if the asynchronous load fails.
    }
    /********************************************************
       *
       *
       * VIDEOJS AREA
       *
       *
       **********************************************************/
    /**
     * Private method for build VideoJs player and prepare to start.
     * @private
     * @param {?} url
     * @return {?}
     */
    loadVideoJs(url) {
        this._renderer.addClass(this._videoTag, 'video-js');
        this._renderer.addClass(this._videoTag, 'vjs-default-skin');
        switch (this._playerOptions.mode) {
            case C.TYPE.HLS:
                this.buildVjsHlsPlayer(url);
                break;
            case C.TYPE.MP4:
                this.buildVjsMp4Player(url);
                break;
            default:
                this.buildVjsMp4Player(url);
                break;
        }
    }
    /**
     * Method for build HLS Player with VideoJS
     * @private
     * @param {?} url
     * @return {?}
     */
    buildVjsHlsPlayer(url) {
        /** @type {?} */
        const overrideNative = true;
        this._opts = {
            html5: {
                hls: {
                    debug: true,
                    // bandwidth: 500000, // initial bandwidth
                    overrideNative: overrideNative,
                },
                nativeVideoTracks: !overrideNative,
                nativeAudioTracks: !overrideNative,
                nativeTextTracks: !overrideNative,
            },
            bigPlayButton: this._playerOptions.bigPlayButton === undefined ? true : this._playerOptions.bigPlayButton,
            autoplay: this._playerOptions.autoplay || false,
            playsinline: true,
            errorDisplay: !this._playerOptions.prod,
        };
        videojs.Hls.xhr('error', (/**
         * @param {?} anything
         * @return {?}
         */
        (anything) => { console.error('Erro XHR capturado.', anything); }));
        videojs.Hls.xhr.beforeRequest = (/**
         * @param {?} options
         * @return {?}
         */
        (options) => {
            if (!isUndefined(this._playerOptions.token) && !isUndefined(options)) {
                if (isUndefined(options.headers)) {
                    options.headers = {};
                }
                options.headers.Authorization = 'Bearer ' + this._playerOptions.token;
                options.timeout = 45000;
            }
        });
        /**
         * Initialize player
         */
        this._videojs = videojs('akiryPlayerId', this._opts);
        this._videojs.src({
            src: url,
            type: 'application/x-mpegURL',
            withCredentials: false,
        });
        /**
         * OnReady -> Before the download of first chunk
         */
        this._videojs.ready((/**
         * @return {?}
         */
        () => {
            this._akiryConsole.addLogs('Executing onReady...');
            // force to show bottom controls
            /** @type {?} */
            const vjsControlBars = Array.from(document.querySelectorAll('.vjs-default-skin.vjs-paused .vjs-control-bar'));
            for (const vjsControlBar of vjsControlBars) {
                this._renderer.setStyle(vjsControlBar, 'display', 'flex');
            }
            // method to start initial buffering on iOS but it also autostart on others devices
            /** @type {?} */
            const playButtonBottom = (/** @type {?} */ (Array.from(document.getElementsByClassName('vjs-play-control vjs-control vjs-button'))[0]));
            // playButtonBottom.click();
            /**
             * Setup adaptation strategy
             */
            if (this._videojs && this._videojs.tech_ && this._videojs.tech_.hls) {
                this._videojs.tech_.hls.selectPlaylist = (/**
                 * @return {?}
                 */
                () => {
                    /** @type {?} */
                    const hls = this._videojs.tech_.hls;
                    /** @type {?} */
                    const playlists = hls.playlists.master.playlists;
                    /** @type {?} */
                    const formats = playlists.length;
                    /** @type {?} */
                    const currentTime = this._videojs.cache_.currentTime;
                    /** @type {?} */
                    const bandwidth = hls.bandwidth;
                    console.log(playlists);
                    /** @type {?} */
                    let selectedPlaylist;
                    if (this.currentPlaylist < playlists.length) {
                        selectedPlaylist = this.currentPlaylist;
                    }
                    else {
                        console.error('Current playlist doesn\'t exist');
                        selectedPlaylist = 0;
                    }
                    //   this._akiryConsole.addLogs(JSON.stringify({ currentTime, formats, bandwidth }));
                    if (this._playerOptions.evaluator) {
                        selectedPlaylist = this._playerOptions.evaluator.evaluate(currentTime, 0, playlists, bandwidth);
                    }
                    //   this._akiryConsole.addLogs(
                    //     'Bandwidth: ' + bandwidth +
                    //     '\nSelected: ' + (selectedPlaylist + 1) + '/' + playlists.length,
                    //   );
                    return playlists[selectedPlaylist];
                });
            }
            else {
                this._akiryConsole.addLogs('Akiry Adaptation is disabled. Native player executing.');
            }
            this.buildPlayerSkin();
            if (this._playerOptions.callbacks && this._playerOptions.callbacks.onReady) {
                this._playerOptions.callbacks.onReady();
            }
            // this._videojs.play();
            // this._videoTag.play();
            // const playButton: HTMLButtonElement = document.getElementsByClassName('vjs-big-play-button')[0] as HTMLButtonElement;
            // playButton.click();
            // playButton.style.display = 'none';
            this._akiryConsole.addLogs('onReady executed.');
        }));
        // this._videoTag.load(); // Não obrigatório
    }
    /**
     * Method for build HLS Player with VideoJS
     * @private
     * @param {?} url
     * @return {?}
     */
    buildVjsMp4Player(url) {
        // console.log('this.buildMp4Player');
        this._videojs = videojs(this._videoTag);
        this._videojs.src(url);
    }
    /**
     * *****************************************************
     *
     *
     * iOS NATIVE HLS AREA
     *
     *
     * ********************************************************
     * @private
     * @param {?} url
     * @return {?}
     */
    loadNativeFor_iOS(url) {
        url = iOSTools.processStringForPlatform(url);
        this._videoTag.src = url;
        this._videoTag.load();
        this._akiryConsole.addLogs('Starting IOS');
    }
}
AkiryPlayerDirective.decorators = [
    { type: Directive, args: [{
                selector: '[libAkiryPlayer]',
            },] }
];
/** @nocollapse */
AkiryPlayerDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: AkiryPlayerConsoleService }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerDirective.prototype._videoTag;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerDirective.prototype._videojs;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerDirective.prototype._shaka;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerDirective.prototype._opts;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerDirective.prototype._playerOptions;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerDirective.prototype._internalListeners;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerDirective.prototype.currentPlaylist;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerDirective.prototype._renderer;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerDirective.prototype._akiryConsole;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWtpcnktcGxheWVyLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FraXJ5LXBsYXllci8iLCJzb3VyY2VzIjpbImxpYi9kaXJlY3RpdmVzL2FraXJ5LXBsYXllci9ha2lyeS1wbGF5ZXIuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUlBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRW5DLE9BQU8sRUFBRSxDQUFDLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNuQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDaEQsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sa0VBQWtFLENBQUM7OztNQUt2RyxPQUFPLEdBQUcsT0FBTyxDQUFDLDBDQUEwQyxDQUFDOztNQUc3RCx3QkFBd0IsR0FBRyxDQUFDOzs7Ozs7O0FBVWxDLENBQUMsbUJBQUEsTUFBTSxFQUFPLENBQUMsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDOzs7Ozs7TUFNNUIsS0FBSyxHQUFHLE9BQU8sQ0FBQywrQ0FBK0MsQ0FBQzs7O0FBRXRFLE9BQU8sQ0FBQyxxREFBcUQsQ0FBQyxDQUFDLENBQUMsc0NBQXNDOztBQUt0RyxNQUFNLE9BQU8sb0JBQW9COzs7Ozs7SUFXL0IsWUFDRSxFQUFjLEVBQ0csU0FBb0IsRUFDcEIsYUFBd0M7UUFEeEMsY0FBUyxHQUFULFNBQVMsQ0FBVztRQUNwQixrQkFBYSxHQUFiLGFBQWEsQ0FBMkI7UUFQMUMsdUJBQWtCLEdBQXNCLEVBQUUsQ0FBQztRQUVwRCxvQkFBZSxHQUFHLHdCQUF3QixDQUFDO1FBT2pELElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDLGFBQWEsQ0FBQztRQUVsQyx5Q0FBeUM7UUFDekMscUNBQXFDO0lBQ3ZDLENBQUM7Ozs7O0lBS00sV0FBVztRQUNoQixJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUFFO1lBQ2hDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7Z0JBQ3pCLElBQUksUUFBUSxDQUFDLEdBQUcsRUFBRSxFQUFFO29CQUNsQixRQUFRLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFO3dCQUNsQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTzs0QkFDcEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs0QkFDckIsTUFBTTt3QkFDUjs0QkFDRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDOzRCQUN0QixNQUFNO3FCQUNUO2lCQUNGO3FCQUFNO29CQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7aUJBQ3ZCO2FBQ0Y7aUJBQU07Z0JBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUFFO1NBQ25DO2FBQU07WUFDTCxPQUFPLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDakM7SUFDSCxDQUFDOzs7O0lBRU0sVUFBVTtRQUNmLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRTtZQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixFQUFFLENBQUM7U0FDcEM7YUFBTSxJQUFJLENBQUMsbUJBQUEsSUFBSSxDQUFDLFNBQVMsRUFBTyxDQUFDLENBQUMsb0JBQW9CLEVBQUU7WUFDdkQsQ0FBQyxtQkFBQSxJQUFJLENBQUMsU0FBUyxFQUFPLENBQUMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1NBQ2hEO2FBQU0sSUFBSSxDQUFDLG1CQUFBLElBQUksQ0FBQyxTQUFTLEVBQU8sQ0FBQyxDQUFDLHVCQUF1QixFQUFFO1lBQzFELENBQUMsbUJBQUEsSUFBSSxDQUFDLFNBQVMsRUFBTyxDQUFDLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztTQUNuRDtRQUVELElBQUksUUFBUSxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQ2xCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsRUFBRTtnQkFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO2FBQ3hDO1lBQ0QsSUFBSSxDQUFDLG1CQUFBLElBQUksQ0FBQyxTQUFTLEVBQU8sQ0FBQyxDQUFDLGVBQWUsRUFBRTtnQkFDM0MsQ0FBQyxtQkFBQSxJQUFJLENBQUMsU0FBUyxFQUFPLENBQUMsQ0FBQyxlQUFlLEVBQUUsQ0FBQzthQUMzQztTQUNGO0lBQ0gsQ0FBQzs7Ozs7SUFLWSxJQUFJOztZQUNmLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxTQUFTLEVBQUU7Z0JBQ2hDLElBQUksUUFBUSxDQUFDLEdBQUcsRUFBRSxFQUFFO29CQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRTt5QkFDbEIsSUFBSTs7O29CQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBQzt5QkFDZixLQUFLOzs7O29CQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7d0JBQ2YsT0FBTyxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsRUFBRSxLQUFLLENBQUMsQ0FBQzt3QkFDaEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO3dCQUM1QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO29CQUN4QixDQUFDLEVBQUMsQ0FBQztpQkFDTjtxQkFBTTtvQkFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDLDJDQUEyQzt5QkFDOUQsSUFBSTs7O29CQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBQzt5QkFDZixLQUFLOzs7O29CQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7d0JBQ2YsT0FBTyxDQUFDLEtBQUssQ0FBQyx5QkFBeUIsRUFBRSxLQUFLLENBQUMsQ0FBQzt3QkFDaEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO3dCQUM1QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO29CQUN4QixDQUFDLEVBQUMsQ0FBQztpQkFDTjthQUNGO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzthQUNqQztRQUNILENBQUM7S0FBQTs7Ozs7SUFLWSxLQUFLOztZQUNoQixJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssU0FBUyxFQUFFO2dCQUNoQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ3hCO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzthQUNqQztRQUNILENBQUM7S0FBQTs7Ozs7SUFLTSxPQUFPO1FBQ1osSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZCLFFBQVEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2xDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPO29CQUNwQixJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7d0JBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztxQkFBRTtvQkFDL0MsTUFBTTtnQkFDUjtvQkFDRSxPQUFPLENBQUMsS0FBSyxDQUFDLDZDQUE2QyxDQUFDLENBQUM7b0JBQzdELE1BQU07YUFDVDtTQUNGO0lBQ0gsQ0FBQzs7Ozs7OztJQU9NLElBQUksQ0FBQyxHQUFHLEVBQUUsYUFBYTtRQUM1QixJQUFJLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQztRQUNwQyxxREFBcUQ7UUFDckQsMkVBQTJFO1FBQzNFLElBQUk7UUFFSixJQUFJLFFBQVEsQ0FBQyxHQUFHLEVBQUUsRUFBRTtZQUNsQixJQUFJLENBQUMsaUJBQWlCLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDN0I7YUFBTTtZQUNMLFFBQVEsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2xDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLO29CQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNwQixNQUFNO2dCQUNSLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPO29CQUNwQixJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUN0QixNQUFNO2dCQUNSO29CQUNFLE9BQU8sQ0FBQyxLQUFLLENBQUMsNEZBQTRGLENBQUMsQ0FBQztvQkFDNUcsTUFBTTthQUNUO1NBQ0Y7UUFFRCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7Ozs7Ozs7Ozs7O0lBWU8sWUFBWTtRQUNsQixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFO1lBQzdCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDO1NBQ25EO1FBRUQsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDOzs7Y0FHaEIsVUFBVSxHQUFhLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTztRQUVsRCxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU87Ozs7UUFBRyxDQUFDLEtBQW9CLEVBQUUsRUFBRTtZQUMvQywwQkFBMEI7WUFDMUIsUUFBUSxLQUFLLENBQUMsR0FBRyxFQUFFO2dCQUNqQixLQUFLLEdBQUcsSUFBSSxVQUFVO29CQUNwQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ25CLE1BQU07Z0JBQ1IsS0FBSyxXQUFXO29CQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7b0JBQ2pHLE1BQU07Z0JBQ1IsS0FBSyxZQUFZO29CQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVzt3QkFDeEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLENBQUM7NEJBQ3hELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUM7NEJBQzVCLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztvQkFDbkMsTUFBTTtnQkFDUixLQUFLLFNBQVM7b0JBQ1osSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztvQkFDdEYsTUFBTTtnQkFDUixLQUFLLFdBQVc7b0JBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQztvQkFDdEYsTUFBTTtnQkFDUjtvQkFDRSxJQUFJLFVBQVUsRUFBRTt3QkFDZCxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQ25CO29CQUNELE1BQU07YUFDVDtRQUNILENBQUMsQ0FBQSxDQUFDO0lBQ0osQ0FBQzs7Ozs7SUFFTyxlQUFlO1FBRXJCOztXQUVHO1FBQ0gsMkVBQTJFO1FBQzNFLHNGQUFzRjtRQUN0RixvSUFBb0k7UUFDcEksd0JBQXdCO1FBQ3hCLDJIQUEySDtRQUMzSCxJQUFJO1FBRUo7O1dBRUc7SUFDTCxDQUFDOzs7OztJQUVPLGNBQWM7UUFDcEIsMENBQTBDO1FBQzFDLCtDQUErQztRQUMvQyw2Q0FBNkM7UUFDN0MsNERBQTREO1FBQzVELGlFQUFpRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Y0FjM0QsTUFBTSxHQUFHO1lBQ2IsT0FBTztZQUNQLFNBQVM7WUFDVCxnQkFBZ0I7WUFDaEIsZ0JBQWdCO1lBQ2hCLFNBQVM7WUFDVCxPQUFPO1lBQ1AsT0FBTztZQUNQLFlBQVk7WUFDWixnQkFBZ0I7WUFDaEIsV0FBVztZQUNYLE9BQU87WUFDUCxNQUFNO1lBQ04sU0FBUztZQUNULG1EQUFtRDtZQUNuRCxZQUFZO1lBQ1osUUFBUTtZQUNSLFNBQVM7WUFDVCxTQUFTO1lBQ1QsU0FBUztZQUNULHNFQUFzRTtZQUN0RSwyREFBMkQ7WUFDM0QsU0FBUztTQUNWO1FBRUQsS0FBSyxNQUFNLEtBQUssSUFBSSxNQUFNLEVBQUU7WUFDMUIsSUFBSSxRQUFRLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsS0FBSzs7O2dCQUFFLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRSxLQUFLLENBQUMsQ0FBQzthQUN6RztpQkFBTTtnQkFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEtBQUs7OztnQkFBRSxHQUFHLEVBQUUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUUsS0FBSyxDQUFDLENBQUM7YUFDekc7U0FDRjtRQUVELElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsVUFBVTs7O1FBQUUsR0FBRyxFQUFFO1lBQy9DLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN2QixDQUFDLEdBQUUsS0FBSyxDQUFDLENBQUM7UUFFVixJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLFNBQVM7OztRQUFFLEdBQUcsRUFBRTtZQUM5QyxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLEVBQUU7Z0JBQ3JDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUNyQztRQUNILENBQUMsRUFBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPOzs7UUFBRSxHQUFHLEVBQUU7WUFDNUMsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRTtnQkFDakcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDekM7UUFDSCxDQUFDLEVBQUMsQ0FBQztRQUVILHVDQUF1QztRQUN2QywwQ0FBMEM7UUFDMUMseURBQXlEO1FBQ3pELDhEQUE4RDtRQUU5RCwrQ0FBK0M7UUFDL0MsbUZBQW1GO1FBQ25GLDZFQUE2RTtRQUU3RSw0RUFBNEU7UUFDNUUseURBQXlEO1FBQ3pELG9DQUFvQztRQUNwQyw2QkFBNkI7UUFDN0IsNENBQTRDO1FBQzVDLCtCQUErQjtRQUMvQixlQUFlO1FBQ2Ysb0NBQW9DO1FBQ3BDLG9DQUFvQztRQUNwQyw0RkFBNEY7UUFDNUYsb0RBQW9EO1FBQ3BELDBIQUEwSDtRQUMxSCxrQ0FBa0M7UUFDbEMsMkNBQTJDO1FBQzNDLHFDQUFxQztRQUNyQyx1Q0FBdUM7UUFDdkMsK0NBQStDO1FBQy9DLFlBQVk7UUFDWixRQUFRO1FBQ1IsTUFBTTtRQUVOLHFEQUFxRDtRQUNyRCwrRkFBK0Y7UUFDL0Ysa0ZBQWtGO1FBQ2xGLE1BQU07UUFFTixzQ0FBc0M7UUFDdEMsd0NBQXdDO1FBRXhDLDJEQUEyRDtRQUMzRCxNQUFNO1FBQ04scUNBQXFDO1FBQ3JDLGlJQUFpSTtRQUNqSSxNQUFNO1FBRU4sNkNBQTZDO1FBQzdDLG9EQUFvRDtRQUNwRCx3Q0FBd0M7UUFFeEMsd0NBQXdDO1FBQ3hDLG1CQUFtQjtRQUNuQixNQUFNO1FBQ04sTUFBTTtRQUVOLG1DQUFtQztRQUNuQyx5Q0FBeUM7UUFDekMsa0NBQWtDO1FBQ2xDLE1BQU07UUFFTiwyQ0FBMkM7UUFDM0MsaURBQWlEO1FBQ2pELDBDQUEwQztRQUMxQyxNQUFNO1FBRU4seUNBQXlDO1FBQ3pDLHdDQUF3QztRQUN4QyxNQUFNO1FBRU4sMkNBQTJDO1FBQzNDLDBDQUEwQztRQUMxQyxNQUFNO1FBRU4sb0NBQW9DO1FBQ3BDLG1DQUFtQztRQUNuQyx5R0FBeUc7UUFDekcsK0NBQStDO1FBQy9DLE1BQU07UUFDTixNQUFNO1FBRU4sb0NBQW9DO1FBQ3BDLG1DQUFtQztRQUNuQyxNQUFNO1FBRU4sK0NBQStDO1FBQy9DLDhDQUE4QztRQUM5QyxNQUFNO1FBRU4sc0NBQXNDO1FBQ3RDLG9EQUFvRDtRQUNwRCxNQUFNO1FBRU4sc0NBQXNDO1FBQ3RDLDRDQUE0QztRQUM1QyxNQUFNO1FBRU4sc0NBQXNDO1FBQ3RDLDRDQUE0QztRQUM1QyxNQUFNO1FBRU4sc0NBQXNDO1FBQ3RDLDRDQUE0QztRQUM1QyxNQUFNO1FBRU4sb0NBQW9DO1FBQ3BDLG1DQUFtQztRQUNuQyxNQUFNO0lBQ1IsQ0FBQzs7Ozs7SUFFTSx3QkFBd0IsQ0FBQyxZQUFzQjtRQUNwRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztJQUN0RCxDQUFDOzs7OztJQUVNLG9CQUFvQixDQUFDLFNBQW1CO1FBQzdDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO0lBQ2hELENBQUM7Ozs7SUFFTSxhQUFhOztjQUNaLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVc7O2NBQ3hDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7O2NBQ2pILFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVE7O2NBQ2xDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVc7UUFFOUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLFlBQVksRUFBRSxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFFckYsSUFBSSxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFO1lBQ3hDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLFlBQVksQ0FBQyxDQUFDO1NBQ2pFO0lBQ0gsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7SUFnQk8sU0FBUyxDQUFDLEdBQUc7UUFDbkIsaUVBQWlFO1FBQ2pFLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLENBQUM7UUFFNUIsbUVBQW1FO1FBQ25FLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLGtCQUFrQixFQUFFLEVBQUU7WUFDdEMsOERBQThEO1lBQzlELE9BQU8sQ0FBQyxLQUFLLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUV4QyxPQUFPO1NBQ1I7UUFFRCx5QkFBeUI7UUFDekIsUUFBUSxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRTtZQUNoQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRztnQkFDYixJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzNCLE1BQU07WUFDUixLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSTtnQkFDZCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzNCLE1BQU07WUFDUixLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRztnQkFDYixJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQzNCLE1BQU07WUFDUjtnQkFDRSxNQUFNO1NBQ1Q7SUFDSCxDQUFDOzs7Ozs7SUFFTyxnQkFBZ0IsQ0FBQyxHQUFHOztjQUNwQixPQUFPOzs7O1FBQUcsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUN4QixpQkFBaUI7WUFDakIsT0FBTyxDQUFDLEtBQUssQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLElBQUksRUFBRSxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDM0QsQ0FBQyxDQUFBOztjQUVLLFlBQVk7Ozs7UUFBRyxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQzdCLHNEQUFzRDtZQUN0RCxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3hCLENBQUMsQ0FBQTtRQUVELDRCQUE0QjtRQUM1QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFL0MsMkVBQTJFO1FBQzNFLENBQUMsbUJBQUEsTUFBTSxFQUFPLENBQUMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUVyQywyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFFcEQsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO1lBQ3BCLFNBQVMsRUFBRTtnQkFDVCxhQUFhLEVBQUUsR0FBRztnQkFDbEIsZUFBZSxFQUFFLEVBQUU7Z0JBQ25CLGVBQWUsRUFBRTtvQkFDZixPQUFPLEVBQUUsQ0FBQzs7b0JBQ1YsV0FBVyxFQUFFLENBQUM7O29CQUNkLFNBQVMsRUFBRSxHQUFHOztvQkFDZCxhQUFhLEVBQUUsQ0FBQzs7b0JBQ2hCLFVBQVUsRUFBRSxHQUFHO2lCQUNoQjthQUNGO1NBQ0YsQ0FBQyxDQUFDO1FBRUgseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxxQkFBcUI7Ozs7O1FBQUMsQ0FBQyxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQUU7WUFDeEUsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRTtnQkFDN0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUU7b0JBQUUsT0FBTyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7aUJBQUU7Z0JBQzlDLE9BQU8sQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDO2FBQzFFO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFFSCwwQkFBMEI7UUFDMUIsbUNBQW1DO1FBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUk7OztRQUFDLEdBQUcsRUFBRTtZQUM5QixvREFBb0Q7WUFDcEQsT0FBTyxDQUFDLElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDLENBQUMsaUNBQWlDO1FBQ25GLENBQUMsRUFBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFFLHNEQUFzRDtJQUM1RSxDQUFDOzs7Ozs7Ozs7Ozs7OztJQXFCTyxXQUFXLENBQUMsR0FBRztRQUNyQixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUU1RCxRQUFRLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFO1lBQ2hDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHO2dCQUNiLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUIsTUFBTTtZQUNSLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHO2dCQUNiLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUIsTUFBTTtZQUNSO2dCQUNFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUIsTUFBTTtTQUNUO0lBRUgsQ0FBQzs7Ozs7OztJQU1PLGlCQUFpQixDQUFDLEdBQUc7O2NBQ3JCLGNBQWMsR0FBRyxJQUFJO1FBRTNCLElBQUksQ0FBQyxLQUFLLEdBQUc7WUFDWCxLQUFLLEVBQUU7Z0JBQ0wsR0FBRyxFQUFFO29CQUNILEtBQUssRUFBRSxJQUFJOztvQkFFWCxjQUFjLEVBQUUsY0FBYztpQkFDL0I7Z0JBQ0QsaUJBQWlCLEVBQUUsQ0FBQyxjQUFjO2dCQUNsQyxpQkFBaUIsRUFBRSxDQUFDLGNBQWM7Z0JBQ2xDLGdCQUFnQixFQUFFLENBQUMsY0FBYzthQUNsQztZQUNELGFBQWEsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhO1lBQ3pHLFFBQVEsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsSUFBSSxLQUFLO1lBQy9DLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLFlBQVksRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSTtTQUN4QyxDQUFDO1FBRUYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsT0FBTzs7OztRQUFFLENBQUMsUUFBUSxFQUFFLEVBQUUsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLHFCQUFxQixFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQUM7UUFHNUYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsYUFBYTs7OztRQUFHLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNwRSxJQUFJLFdBQVcsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7b0JBQ2hDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO2lCQUN0QjtnQkFDRCxPQUFPLENBQUMsT0FBTyxDQUFDLGFBQWEsR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3RFLE9BQU8sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQ3pCO1FBQ0gsQ0FBQyxDQUFBLENBQUM7UUFFRjs7V0FFRztRQUNILElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUM7WUFDaEIsR0FBRyxFQUFFLEdBQUc7WUFDUixJQUFJLEVBQUUsdUJBQXVCO1lBQzdCLGVBQWUsRUFBRSxLQUFLO1NBQ3ZCLENBQUMsQ0FBQztRQUVIOztXQUVHO1FBQ0gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLOzs7UUFBQyxHQUFHLEVBQUU7WUFDdkIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQzs7O2tCQUc3QyxjQUFjLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsK0NBQStDLENBQUMsQ0FBQztZQUM3RyxLQUFLLE1BQU0sYUFBYSxJQUFJLGNBQWMsRUFBRTtnQkFDMUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQzthQUMzRDs7O2tCQUdLLGdCQUFnQixHQUNwQixtQkFBQSxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxzQkFBc0IsQ0FBQyx5Q0FBeUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQXFCO1lBQ2hILDRCQUE0QjtZQUU1Qjs7ZUFFRztZQUNILElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUU7Z0JBQ25FLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxjQUFjOzs7Z0JBQUcsR0FBRyxFQUFFOzswQkFDdEMsR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUc7OzBCQUM3QixTQUFTLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsU0FBUzs7MEJBQzFDLE9BQU8sR0FBRyxTQUFTLENBQUMsTUFBTTs7MEJBQzFCLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxXQUFXOzswQkFDOUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxTQUFTO29CQUUvQixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDOzt3QkFFbkIsZ0JBQXdCO29CQUU1QixJQUFJLElBQUksQ0FBQyxlQUFlLEdBQUcsU0FBUyxDQUFDLE1BQU0sRUFBRTt3QkFDM0MsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztxQkFDekM7eUJBQU07d0JBQ0wsT0FBTyxDQUFDLEtBQUssQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO3dCQUNqRCxnQkFBZ0IsR0FBRyxDQUFDLENBQUM7cUJBQ3RCO29CQUVELHFGQUFxRjtvQkFFckYsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRTt3QkFDakMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUN2RCxXQUFXLEVBQ1gsQ0FBQyxFQUNELFNBQVMsRUFDVCxTQUFTLENBQ1YsQ0FBQztxQkFDSDtvQkFFRCxnQ0FBZ0M7b0JBQ2hDLGtDQUFrQztvQkFDbEMsd0VBQXdFO29CQUN4RSxPQUFPO29CQUVQLE9BQU8sU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQSxDQUFDO2FBQ0g7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsd0RBQXdELENBQUMsQ0FBQzthQUN0RjtZQUVELElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUV2QixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRTtnQkFDMUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDekM7WUFFRCx3QkFBd0I7WUFDeEIseUJBQXlCO1lBQ3pCLHdIQUF3SDtZQUN4SCxzQkFBc0I7WUFDdEIscUNBQXFDO1lBQ3JDLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDbEQsQ0FBQyxFQUFDLENBQUM7UUFFSCw0Q0FBNEM7SUFDOUMsQ0FBQzs7Ozs7OztJQU1PLGlCQUFpQixDQUFDLEdBQUc7UUFDM0Isc0NBQXNDO1FBRXRDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN6QixDQUFDOzs7Ozs7Ozs7Ozs7O0lBZU8saUJBQWlCLENBQUMsR0FBRztRQUMzQixHQUFHLEdBQUcsUUFBUSxDQUFDLHdCQUF3QixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTdDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUN6QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQzdDLENBQUM7OztZQTdyQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxrQkFBa0I7YUFDN0I7Ozs7WUFuQ21CLFVBQVU7WUFBRSxTQUFTO1lBS2hDLHlCQUF5Qjs7Ozs7OztJQWdDaEMseUNBQTZDOzs7OztJQUM3Qyx3Q0FBc0I7Ozs7O0lBQ3RCLHNDQUFvQjs7Ozs7SUFDcEIscUNBQW1COzs7OztJQUNuQiw4Q0FBMkM7Ozs7O0lBRTNDLGtEQUE0RDs7Ozs7SUFFNUQsK0NBQW1EOzs7OztJQUlqRCx5Q0FBcUM7Ozs7O0lBQ3JDLDZDQUF5RCIsInNvdXJjZXNDb250ZW50IjpbImRlY2xhcmUgY29uc3QgcmVxdWlyZTogYW55O1xuZGVjbGFyZSBjb25zdCB3aW5kb3c6IFdpbmRvdztcbmRlY2xhcmUgY29uc3QgZG9jdW1lbnQ6IERvY3VtZW50O1xuXG5pbXBvcnQgeyBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIFJlbmRlcmVyMiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgaXNVbmRlZmluZWQgfSBmcm9tICd1dGlsJztcblxuaW1wb3J0IHsgQyB9IGZyb20gJy4uLy4uL2NvbmZpZy9jJztcbmltcG9ydCB7IGlPU1Rvb2xzIH0gZnJvbSAnLi4vLi4vbGlicy91dGlscy9pb3MnO1xuaW1wb3J0IHsgQWtpcnlQbGF5ZXJDb25zb2xlU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FraXJ5LXBsYXllci1jb25zb2xlL2FraXJ5LXBsYXllci1jb25zb2xlLnNlcnZpY2UnO1xuaW1wb3J0IHsgQWtpcnlQbGF5ZXJPcHRpb25zLCBJbnRlcm5hbExpc3RlbmVycyB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5cbi8vIGltcG9ydCAqIGFzIHZpZGVvanMgZnJvbSAnLi4vLi4vbGlicy9taW4vdmlkZW9qcy9kaXN0L3ZpZGVvLm1pbi5qcyc7XG5cbmNvbnN0IHZpZGVvanMgPSByZXF1aXJlKCcuLi8uLi9saWJzL21pbi92aWRlb2pzL2Rpc3QvdmlkZW8ubWluLmpzJyk7XG5cblxuY29uc3QgREVGQVVMVF9JTklUSUFMX1BMQVlMSVNUID0gMTtcblxuLy8gVE9ETzogSW1wb3J0YXIgTVVYLkpTXG5cbi8qKlxuICogSW1wb3J0aW5nIFZpZGVvSlNcbiAqL1xuXG4vLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tcmVxdWlyZS1pbXBvcnRzXG4vLyBjb25zdCB2aWRlb2pzID0gcmVxdWlyZSgnLi4vLi4vbGlicy9taW4vdmlkZW9qcy9kaXN0L3ZpZGVvLm1pbi5qcycpOyAvLyB0c2xpbnQ6ZGlzYWJsZS1saW5lOm5vLXZhci1yZXF1aXJlc1xuKHdpbmRvdyBhcyBhbnkpLnZpZGVvanMgPSB2aWRlb2pzO1xuXG4vKipcbiAqIEltcG9ydGluZyBTaGFrYVBsYXllclxuICovXG4vLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tcmVxdWlyZS1pbXBvcnRzXG5jb25zdCBzaGFrYSA9IHJlcXVpcmUoJy4uLy4uL2xpYnMvbWluL3NoYWthL3NoYWthLXBsYXllci5jb21waWxlZC5qcycpOyAvLyB0c2xpbnQ6ZGlzYWJsZS1saW5lOm5vLXZhci1yZXF1aXJlc1xuLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm5vLXJlcXVpcmUtaW1wb3J0c1xucmVxdWlyZSgnLi4vLi4vbGlicy9taW4vc2hha2Evc2hha2EtcGxheWVyLmNvbXBpbGVkLmRlYnVnLmpzJyk7IC8vIHRzbGludDpkaXNhYmxlLWxpbmU6bm8tdmFyLXJlcXVpcmVzXG5cbkBEaXJlY3RpdmUoe1xuICBzZWxlY3RvcjogJ1tsaWJBa2lyeVBsYXllcl0nLFxufSlcbmV4cG9ydCBjbGFzcyBBa2lyeVBsYXllckRpcmVjdGl2ZSB7XG4gIHByaXZhdGUgcmVhZG9ubHkgX3ZpZGVvVGFnOiBIVE1MVmlkZW9FbGVtZW50O1xuICBwcml2YXRlIF92aWRlb2pzOiBhbnk7XG4gIHByaXZhdGUgX3NoYWthOiBhbnk7XG4gIHByaXZhdGUgX29wdHM6IGFueTtcbiAgcHJpdmF0ZSBfcGxheWVyT3B0aW9uczogQWtpcnlQbGF5ZXJPcHRpb25zO1xuXG4gIHByaXZhdGUgcmVhZG9ubHkgX2ludGVybmFsTGlzdGVuZXJzOiBJbnRlcm5hbExpc3RlbmVycyA9IHt9O1xuXG4gIHByaXZhdGUgY3VycmVudFBsYXlsaXN0ID0gREVGQVVMVF9JTklUSUFMX1BMQVlMSVNUO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIGVsOiBFbGVtZW50UmVmLFxuICAgIHByaXZhdGUgcmVhZG9ubHkgX3JlbmRlcmVyOiBSZW5kZXJlcjIsXG4gICAgcHJpdmF0ZSByZWFkb25seSBfYWtpcnlDb25zb2xlOiBBa2lyeVBsYXllckNvbnNvbGVTZXJ2aWNlLFxuICApIHtcbiAgICB0aGlzLl92aWRlb1RhZyA9IGVsLm5hdGl2ZUVsZW1lbnQ7XG5cbiAgICAvLyBjb25zb2xlLmxvZygnVmlkZW9KUyBsaWI6ICcsIHZpZGVvanMpO1xuICAgIC8vIGNvbnNvbGUubG9nKCdTaGFrYSBsaWI6ICcsIHNoYWthKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBQbGF5LCBpZiB0aGUgcGxheWVyIGlzIHBhdXNlZCwgYW5kIHBhdXNlLCBpZiBwbGF5aW5nXG4gICAqL1xuICBwdWJsaWMgcGxheU9yUGF1c2UoKSB7XG4gICAgaWYgKHRoaXMuX3ZpZGVvVGFnICE9PSB1bmRlZmluZWQpIHtcbiAgICAgIGlmICh0aGlzLl92aWRlb1RhZy5wYXVzZWQpIHtcbiAgICAgICAgaWYgKGlPU1Rvb2xzLmlPUygpKSB7XG4gICAgICAgICAgc3dpdGNoICh0aGlzLl9wbGF5ZXJPcHRpb25zLnBsYXllcikge1xuICAgICAgICAgICAgY2FzZSBDLlBMQVlFUlMuVklERU9KUzpcbiAgICAgICAgICAgICAgdGhpcy5fdmlkZW9qcy5wbGF5KCk7XG4gICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgdGhpcy5fdmlkZW9UYWcucGxheSgpO1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5fdmlkZW9UYWcucGxheSgpO1xuICAgICAgICB9XG4gICAgICB9IGVsc2UgeyB0aGlzLl92aWRlb1RhZy5wYXVzZSgpOyB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ1BsYXllciBpcyBudWxsJyk7XG4gICAgfVxuICB9XG5cbiAgcHVibGljIGZ1bGxzY3JlZW4oKSB7XG4gICAgaWYgKHRoaXMuX3ZpZGVvVGFnLnJlcXVlc3RGdWxsc2NyZWVuKSB7XG4gICAgICB0aGlzLl92aWRlb1RhZy5yZXF1ZXN0RnVsbHNjcmVlbigpO1xuICAgIH0gZWxzZSBpZiAoKHRoaXMuX3ZpZGVvVGFnIGFzIGFueSkubW96UmVxdWVzdEZ1bGxTY3JlZW4pIHtcbiAgICAgICh0aGlzLl92aWRlb1RhZyBhcyBhbnkpLm1velJlcXVlc3RGdWxsU2NyZWVuKCk7XG4gICAgfSBlbHNlIGlmICgodGhpcy5fdmlkZW9UYWcgYXMgYW55KS53ZWJraXRSZXF1ZXN0RnVsbHNjcmVlbikge1xuICAgICAgKHRoaXMuX3ZpZGVvVGFnIGFzIGFueSkud2Via2l0UmVxdWVzdEZ1bGxzY3JlZW4oKTtcbiAgICB9XG5cbiAgICBpZiAoaU9TVG9vbHMuaU9TKCkpIHtcbiAgICAgIGlmICh0aGlzLl92aWRlb1RhZy53ZWJraXRFbnRlckZ1bGxzY3JlZW4pIHtcbiAgICAgICAgdGhpcy5fdmlkZW9UYWcud2Via2l0RW50ZXJGdWxsc2NyZWVuKCk7XG4gICAgICB9XG4gICAgICBpZiAoKHRoaXMuX3ZpZGVvVGFnIGFzIGFueSkuZW50ZXJGdWxsc2NyZWVuKSB7XG4gICAgICAgICh0aGlzLl92aWRlb1RhZyBhcyBhbnkpLmVudGVyRnVsbHNjcmVlbigpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBQbGF5IG9ubHlcbiAgICovXG4gIHB1YmxpYyBhc3luYyBwbGF5KCkge1xuICAgIGlmICh0aGlzLl92aWRlb1RhZyAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICBpZiAoaU9TVG9vbHMuaU9TKCkpIHtcbiAgICAgICAgdGhpcy5fdmlkZW9UYWcucGxheSgpXG4gICAgICAgICAgLnRoZW4oKCkgPT4geyB9KVxuICAgICAgICAgIC5jYXRjaCgoZXJyb3IpID0+IHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0Vycm9yIG9uIGF1dG9wbGF5IHZpZGVvJywgZXJyb3IpO1xuICAgICAgICAgICAgdGhpcy5fdmlkZW9UYWcubXV0ZWQgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy5fdmlkZW9UYWcucGxheSgpO1xuICAgICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5fdmlkZW9UYWcucGxheSgpIC8vIEZJWE1FOiBub3Qgd29ya2luZyB3aGVuIHJlZnJlc2ggdGhlIHBhZ2VcbiAgICAgICAgICAudGhlbigoKSA9PiB7IH0pXG4gICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge1xuICAgICAgICAgICAgY29uc29sZS5lcnJvcignRXJyb3Igb24gYXV0b3BsYXkgdmlkZW8nLCBlcnJvcik7XG4gICAgICAgICAgICB0aGlzLl92aWRlb1RhZy5tdXRlZCA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLl92aWRlb1RhZy5wbGF5KCk7XG4gICAgICAgICAgfSk7XG4gICAgICB9XG4gICAgfSBlbHNlIHtcbiAgICAgIGNvbnNvbGUuZXJyb3IoJ1BsYXllciBpcyBudWxsJyk7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIFBhdXNlIG9ubHlcbiAgICovXG4gIHB1YmxpYyBhc3luYyBwYXVzZSgpIHtcbiAgICBpZiAodGhpcy5fdmlkZW9UYWcgIT09IHVuZGVmaW5lZCkge1xuICAgICAgdGhpcy5fdmlkZW9UYWcucGF1c2UoKTtcbiAgICB9IGVsc2Uge1xuICAgICAgY29uc29sZS5lcnJvcignUGxheWVyIGlzIG51bGwnKTtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogTWV0aG9kIHRvIHVuYnVpbGQgcGxheWVyLCBmb3IgcmVsZWFzZSByZXNvdXJjZXNcbiAgICovXG4gIHB1YmxpYyByZWxlYXNlKCkge1xuICAgIGlmICh0aGlzLl9wbGF5ZXJPcHRpb25zKSB7XG4gICAgICBzd2l0Y2ggKHRoaXMuX3BsYXllck9wdGlvbnMucGxheWVyKSB7XG4gICAgICAgIGNhc2UgQy5QTEFZRVJTLlZJREVPSlM6XG4gICAgICAgICAgaWYgKHRoaXMuX3ZpZGVvanMpIHsgdGhpcy5fdmlkZW9qcy5kaXNwb3NlKCk7IH1cbiAgICAgICAgICBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICBjb25zb2xlLmVycm9yKCdOb3RoaW5nIHRvZG8uIFRoZXJlIGlzIG5vdCBwbGF5ZXIgc2VsZWN0ZWQuJyk7XG4gICAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIE1ldGhvZCBmb3IgYnVpbGQgcGxheWVyIGFuZCBwcmVwYXJlIHRvIHN0YXJ0LlxuICAgKiBAcGFyYW0gdmlkZW8gVmlkZW8gb2JqZWN0IHdpdGggc291cmNlIGxpbmsgKHNyYykgYW5kIHR5cGUgKGhscywgZGFzaCwgbXA0KVxuICAgKiBAcGFyYW0gb3B0IE9wdGlvbnMgZm9yIHBsYXkgKHBsYXllciwgaGFzIG1vZGUsIGFkYXB0aXZlIGFsZ29yaXRobSwgZXRjKVxuICAgKi9cbiAgcHVibGljIGxvYWQodXJsLCBwbGF5ZXJPcHRpb25zKSB7XG4gICAgdGhpcy5fcGxheWVyT3B0aW9ucyA9IHBsYXllck9wdGlvbnM7XG4gICAgLy8gaWYgKHRoaXMuX3BsYXllck9wdGlvbnMuZXZhbHVhdG9yID09PSB1bmRlZmluZWQpIHtcbiAgICAvLyAgIHRoaXMuX3BsYXllck9wdGlvbnMuZXZhbHVhdG9yID0gbmV3IEZpeEZvcm1hdCgwLCAoKSA9PiB0aGlzLl92aWRlb2pzKTtcbiAgICAvLyB9XG5cbiAgICBpZiAoaU9TVG9vbHMuaU9TKCkpIHtcbiAgICAgIHRoaXMubG9hZE5hdGl2ZUZvcl9pT1ModXJsKTtcbiAgICB9IGVsc2Uge1xuICAgICAgc3dpdGNoICh0aGlzLl9wbGF5ZXJPcHRpb25zLnBsYXllcikge1xuICAgICAgICBjYXNlIEMuUExBWUVSUy5TSEFLQTpcbiAgICAgICAgICB0aGlzLmxvYWRTaGFrYSh1cmwpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIEMuUExBWUVSUy5WSURFT0pTOlxuICAgICAgICAgIHRoaXMubG9hZFZpZGVvSnModXJsKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICBjb25zb2xlLmVycm9yKCdUaGVyZSBpcyBub3QgcGxheWVyIHNlbGVjdGVkLiBTZWxlY3Qgb25lIHdpdGggYG9wdC5wbGF5ZXIgPSBDLlBMQVlFUlMuJHtTRUxFQ1RFRF9QTEFZRVJ9YC4nKTtcbiAgICAgICAgICBicmVhaztcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLmdlbmVyYWxCdWlsZCgpO1xuICB9XG5cblxuXG5cbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAqXG4gICAqXG4gICAqIEdFTkVSQUwgQVJFQVxuICAgKlxuICAgKlxuICAgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbiAgcHJpdmF0ZSBnZW5lcmFsQnVpbGQoKSB7XG4gICAgaWYgKHRoaXMuX3BsYXllck9wdGlvbnMudGh1bWIpIHtcbiAgICAgIHRoaXMuX3ZpZGVvVGFnLnBvc3RlciA9IHRoaXMuX3BsYXllck9wdGlvbnMudGh1bWI7XG4gICAgfVxuXG4gICAgdGhpcy5idWlsZExpc3RlbmVycygpO1xuXG4gICAgLy8gc2V0IGtleSAnc3BhY2UnIHRvIHBsYXkvcGF1c2VcbiAgICBjb25zdCBvbGRPbktleVVwOiBGdW5jdGlvbiA9IGRvY3VtZW50LmJvZHkub25rZXl1cDtcblxuICAgIGRvY3VtZW50LmJvZHkub25rZXl1cCA9IChldmVudDogS2V5Ym9hcmRFdmVudCkgPT4ge1xuICAgICAgLy8gY29uc29sZS5sb2coZXZlbnQua2V5KTtcbiAgICAgIHN3aXRjaCAoZXZlbnQua2V5KSB7XG4gICAgICAgIGNhc2UgJyAnIHx8ICdTcGFjZWJhcic6XG4gICAgICAgICAgdGhpcy5wbGF5T3JQYXVzZSgpO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlICdBcnJvd0xlZnQnOlxuICAgICAgICAgIHRoaXMuX3ZpZGVvVGFnLmN1cnJlbnRUaW1lID0gdGhpcy5fdmlkZW9UYWcuY3VycmVudFRpbWUgPCA1ID8gMCA6IHRoaXMuX3ZpZGVvVGFnLmN1cnJlbnRUaW1lIC0gNTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnQXJyb3dSaWdodCc6XG4gICAgICAgICAgdGhpcy5fdmlkZW9UYWcuY3VycmVudFRpbWUgPVxuICAgICAgICAgICAgdGhpcy5fdmlkZW9UYWcuY3VycmVudFRpbWUgPiB0aGlzLl92aWRlb1RhZy5kdXJhdGlvbiAtIDUgP1xuICAgICAgICAgICAgICB0aGlzLl92aWRlb1RhZy5jdXJyZW50VGltZSA6XG4gICAgICAgICAgICAgIHRoaXMuX3ZpZGVvVGFnLmN1cnJlbnRUaW1lICsgNTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnQXJyb3dVcCc6XG4gICAgICAgICAgdGhpcy5fdmlkZW9UYWcudm9sdW1lID0gdGhpcy5fdmlkZW9UYWcudm9sdW1lID4gMC45ID8gMSA6IHRoaXMuX3ZpZGVvVGFnLnZvbHVtZSArIDAuMTtcbiAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSAnQXJyb3dEb3duJzpcbiAgICAgICAgICB0aGlzLl92aWRlb1RhZy52b2x1bWUgPSB0aGlzLl92aWRlb1RhZy52b2x1bWUgPCAwLjEgPyAwIDogdGhpcy5fdmlkZW9UYWcudm9sdW1lIC0gMC4xO1xuICAgICAgICAgIGJyZWFrO1xuICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgIGlmIChvbGRPbktleVVwKSB7XG4gICAgICAgICAgICBvbGRPbktleVVwKGV2ZW50KTtcbiAgICAgICAgICB9XG4gICAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfTtcbiAgfVxuXG4gIHByaXZhdGUgYnVpbGRQbGF5ZXJTa2luKCkge1xuXG4gICAgLyoqXG4gICAgICogSW5zZXJ0IHBlcnNvbmFsIGNvbXBhbnkgbG9hZGVyXG4gICAgICovXG4gICAgLy8gY29uc3Qgc3Bpbm5lcnMgPSBkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCd2anMtbG9hZGluZy1zcGlubmVyJyk7XG4gICAgLy8gY29uc3Qgc3Bpbm5lcjogRWxlbWVudCA9IHNwaW5uZXJzICYmIHNwaW5uZXJzLmxlbmd0aCA+IDAgPyBzcGlubmVyc1swXSA6IHVuZGVmaW5lZDtcbiAgICAvLyBpZiAoc3Bpbm5lciAmJiB0aGlzLl9wbGF5ZXJPcHRpb25zICYmIHRoaXMuX3BsYXllck9wdGlvbnMuY29udHJvbHNJbWdQYXRoICYmIHRoaXMuX3BsYXllck9wdGlvbnMuY29udHJvbHNJbWdQYXRoLmxvYWRpbmdCdXR0b24pIHtcbiAgICAvLyAgIHNwaW5uZXIuaW5uZXJIVE1MID1cbiAgICAvLyAgICAgYDxpbWcgc3JjPVwiJHt0aGlzLl9wbGF5ZXJPcHRpb25zLmNvbnRyb2xzSW1nUGF0aC5sb2FkaW5nQnV0dG9ufVwiIGNsYXNzPVwidmpzLWxvYWRpbmctc3Bpbm5lci1pbWdcIiBhbHQ9XCJMb2FkaW5nLi4uXCI+YDtcbiAgICAvLyB9XG5cbiAgICAvKipcbiAgICAgKiBPdGhlcnMgY29udHJvbHMgcG93ZXJlZCBieSBBa2lyeVxuICAgICAqL1xuICB9XG5cbiAgcHJpdmF0ZSBidWlsZExpc3RlbmVycygpIHtcbiAgICAvLyB0aGlzLl92aWRlb1RhZy5vbigndGltZXVwZGF0ZScsICgpID0+IHtcbiAgICAvLyAgIHRoaXMuX2FraXJ5Q29uc29sZS5hZGRMb2dzKCd0aW1ldXBkYXRlZCcpO1xuICAgIC8vICAgLy8gdGhpcy5wcmV2aW91c1RpbWUgPSB0aGlzLmN1cnJlbnRUaW1lO1xuICAgIC8vICAgLy8gdGhpcy5jdXJyZW50VGltZSA9IHRoaXMuX3ZpZGVvanMuY2FjaGVfLmN1cnJlbnRUaW1lO1xuICAgIC8vICAgLy8gLy8gICAgICAgIGNvbnNvbGUubG9nKCd0aW1lVXBkYXRlOiAnICsgdGhpcy5jdXJyZW50VGltZSk7XG5cbiAgICAvLyAgIC8vIGNvbnN0IGJ1ZmZlcmVkID0gdGhpcy5fdmlkZW9qcy5idWZmZXJlZCgpO1xuICAgIC8vICAgLy8gY29uc3QgYnVmZmVyU3RhcnQgPSBidWZmZXJlZC5zdGFydCA/IGJ1ZmZlcmVkLnN0YXJ0KGJ1ZmZlcmVkLmxlbmd0aCAtIDEpIDogLTE7XG4gICAgLy8gICAvLyBjb25zdCBidWZmZXJFbmQgPSBidWZmZXJlZC5lbmQgPyBidWZmZXJlZC5lbmQoYnVmZmVyZWQubGVuZ3RoIC0gMSkgOiAtMTtcbiAgICAvLyAgIC8vIHRoaXMub25UaW1lVXBkYXRlZCh0aGlzLmN1cnJlbnRUaW1lLCBidWZmZXJFbmQpO1xuICAgIC8vICAgLy8gLy8gY29uc3QgYnVmZmVyU3RhcnRQcm90ID0gYnVmZmVyZWQuX19wcm90X18uZW5kID8gYnVmZmVyZWQuX19wcm90X18uZW5kKDApIDogJ27Do28gcm9sb3UnO1xuICAgIC8vICAgLy8gLy8gICAgICAgIGNvbnNvbGUubG9nKCdidWZmZXJlZDogJywgYnVmZmVyU3RhcnQsIGJ1ZmZlckVuZCwgYnVmZmVyZWQubGVuZ3RoKTtcbiAgICAvLyB9KTtcblxuICAgIC8vIHRoaXMuX3ZpZGVvVGFnLm9uKCdwcm9ncmVzcycsICgpID0+IHtcbiAgICAvLyAgIHRoaXMuX2FraXJ5Q29uc29sZS5hZGRMb2dzKCdwcm9ncmVzcycpO1xuICAgIC8vIH0pO1xuXG4gICAgY29uc3QgZXZlbnRzID0gW1xuICAgICAgJ2Fib3J0JyxcbiAgICAgICdjYW5wbGF5JywgICAgICAgICAgLy8gV29ya3Mgb24gaU9TIC0gN1xuICAgICAgJ2NhbnBsYXl0aHJvdWdoJywgICAvLyBXb3JrcyBvbiBpT1MgLSA4XG4gICAgICAnZHVyYXRpb25jaGFuZ2UnLCAgIC8vIFdvcmtzIG9uIGlPUyAtIDMgYW5kIExpdmVcbiAgICAgICdlbXB0aWVkJywgICAgICAgICAgLy8gV29ya3Mgb24gaU9TIC0gMVxuICAgICAgJ2VuZGVkJywgICAgICAgICAgICAvLyBXb3JrcyBvbiBpT1MgLSB3aGVuIGVuZGVkXG4gICAgICAnZXJyb3InLFxuICAgICAgJ2xvYWRlZGRhdGEnLCAgICAgICAvLyBXb3JrcyBvbiBpT1MgLSA2XG4gICAgICAnbG9hZGVkbWV0YWRhdGEnLCAgIC8vIFdvcmtzIG9uIGlPUyAtIDRcbiAgICAgICdsb2Fkc3RhcnQnLCAgICAgICAgLy8gV29ya3Mgb24gaU9TIC0gMlxuICAgICAgJ3BhdXNlJywgICAgICAgICAgICAvLyBXb3JrcyBvbiBpT1MgLSBvbnBhdXNlXG4gICAgICAncGxheScsICAgICAgICAgICAgIC8vIFdvcmtzIG9uIGlPUyAtIG9ucGxheSAxXG4gICAgICAncGxheWluZycsICAgICAgICAgIC8vIFdvcmtzIG9uIGlPUyAtIG9ucGxheSAyXG4gICAgICAvLyAncHJvZ3Jlc3MnLCAgICAgICAgIC8vIFdvcmtzIG9uIGlPUyAtIDUgYW5kIGV2ZXJcbiAgICAgICdyYXRlY2hhbmdlJyxcbiAgICAgICdzZWVrZWQnLCAgICAgICAgICAgLy8gV29ya3Mgb24gaU9TIC0gb24gZW5kZWQgc2Vla1xuICAgICAgJ3NlZWtpbmcnLCAgICAgICAgICAvLyBXb3JrcyBvbiBpT1MgLSBkdXJpbmcgc2Vla2luZ1xuICAgICAgJ3N0YWxsZWQnLCAgICAgICAgICAvLyBXb3JrcyBvbiBpT1MgLSBpcyBub3QgJ3JlYnVmZmVyaXphdGlvbidcbiAgICAgICdzdXNwZW5kJyxcbiAgICAgIC8vICd0aW1ldXBkYXRlJywgICAgICAgLy8gV29ya3Mgb24gaU9TIC0gb25wbGF5IDMgYW5kIHdoaWxlIGlzIHBsYXlpbmdcbiAgICAgIC8vICd2b2x1bWVjaGFuZ2UnLCAgICAgLy8gV29ya3Mgb24gaU9TIC0gd2hlbiB2b2x1bWUgY2hhbmdlXG4gICAgICAnd2FpdGluZycsICAgICAgICAgIC8vIFdvcmtzIG9uIGlPUyAtIHdoZW4gaGF2ZW4ndCBidWZmZXJcbiAgICBdO1xuXG4gICAgZm9yIChjb25zdCBldmVudCBvZiBldmVudHMpIHtcbiAgICAgIGlmIChpT1NUb29scy5pT1MoKSkge1xuICAgICAgICB0aGlzLl92aWRlb1RhZy5hZGRFdmVudExpc3RlbmVyKGV2ZW50LCAoKSA9PiB7IHRoaXMuX2FraXJ5Q29uc29sZS5hZGRMb2dzKCdFdmVudDogJyArIGV2ZW50KTsgfSwgZmFsc2UpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5fdmlkZW9UYWcuYWRkRXZlbnRMaXN0ZW5lcihldmVudCwgKCkgPT4geyB0aGlzLl9ha2lyeUNvbnNvbGUuYWRkTG9ncygnRXZlbnQ6ICcgKyBldmVudCk7IH0sIGZhbHNlKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLl92aWRlb1RhZy5hZGRFdmVudExpc3RlbmVyKCdwcm9ncmVzcycsICgpID0+IHtcbiAgICAgIHRoaXMub25UaW1lVXBkYXRlZCgpO1xuICAgIH0sIGZhbHNlKTtcblxuICAgIHRoaXMuX3ZpZGVvVGFnLmFkZEV2ZW50TGlzdGVuZXIoJ3dhaXRpbmcnLCAoKSA9PiB7XG4gICAgICBpZiAodGhpcy5faW50ZXJuYWxMaXN0ZW5lcnMub25XYWl0aW5nKSB7XG4gICAgICAgIHRoaXMuX2ludGVybmFsTGlzdGVuZXJzLm9uV2FpdGluZygpO1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgdGhpcy5fdmlkZW9UYWcuYWRkRXZlbnRMaXN0ZW5lcignZW5kZWQnLCAoKSA9PiB7XG4gICAgICBpZiAodGhpcy5fcGxheWVyT3B0aW9ucyAmJiB0aGlzLl9wbGF5ZXJPcHRpb25zLmNhbGxiYWNrcyAmJiB0aGlzLl9wbGF5ZXJPcHRpb25zLmNhbGxiYWNrcy5vbkVuZGVkKSB7XG4gICAgICAgIHRoaXMuX3BsYXllck9wdGlvbnMuY2FsbGJhY2tzLm9uRW5kZWQoKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIC8vIHRoaXMuX3ZpZGVvanMub24oJ3Byb2dyZXNzJywgKCkgPT4ge1xuICAgIC8vICAgdGhpcy5wcmV2aW91c1RpbWUgPSB0aGlzLmN1cnJlbnRUaW1lO1xuICAgIC8vICAgdGhpcy5jdXJyZW50VGltZSA9IHRoaXMuX3ZpZGVvanMuY2FjaGVfLmN1cnJlbnRUaW1lO1xuICAgIC8vICAgLy8gICAgICAgIGNvbnNvbGUubG9nKCd0aW1lVXBkYXRlOiAnICsgdGhpcy5jdXJyZW50VGltZSk7XG5cbiAgICAvLyAgIGNvbnN0IGJ1ZmZlcmVkID0gdGhpcy5fdmlkZW9qcy5idWZmZXJlZCgpO1xuICAgIC8vICAgY29uc3QgYnVmZmVyU3RhcnQgPSBidWZmZXJlZC5zdGFydCA/IGJ1ZmZlcmVkLnN0YXJ0KGJ1ZmZlcmVkLmxlbmd0aCAtIDEpIDogLTE7XG4gICAgLy8gICBjb25zdCBidWZmZXJFbmQgPSBidWZmZXJlZC5lbmQgPyBidWZmZXJlZC5lbmQoYnVmZmVyZWQubGVuZ3RoIC0gMSkgOiAtMTtcblxuICAgIC8vICAgY29uc29sZS5sb2coJy0tJywgIXRoaXMuX2lzTm90SW5pdGlhbEJ1ZmZlcmluZywgJy0tfC0tJywgYnVmZmVyRW5kID4gMClcbiAgICAvLyAgIGlmICghdGhpcy5faXNOb3RJbml0aWFsQnVmZmVyaW5nICYmIGJ1ZmZlckVuZCA+IDApIHtcbiAgICAvLyAgICAgaWYgKCF0aGlzLl92aWRlb1RhZy5wYXVzZWQpIHtcbiAgICAvLyAgICAgICBjb25zb2xlLmxvZygncGF1c2UnKVxuICAgIC8vICAgICAgIHRoaXMuX2lzTm90SW5pdGlhbEJ1ZmZlcmluZyA9IHRydWU7XG4gICAgLy8gICAgICAgdGhpcy5fdmlkZW9qcy5wYXVzZSgpO1xuICAgIC8vICAgICB9IGVsc2Uge1xuICAgIC8vICAgICAgIGNvbnNvbGUubG9nKCdkZWxheWVkIHBsYXknKVxuICAgIC8vICAgICAgIHRoaXMuX2RlbGF5ZWRQbGF5ZWQgPSB0cnVlO1xuICAgIC8vICAgICAgIC8vIG1ldGhvZCB0byBzdGFydCBpbml0aWFsIGJ1ZmZlcmluZyBvbiBpT1MgYnV0IGl0IGFsc28gYXV0b3N0YXJ0IG9uIG90aGVycyBkZXZpY2VzXG4gICAgLy8gICAgICAgY29uc3QgcGxheUJ1dHRvbkJvdHRvbTogSFRNTEJ1dHRvbkVsZW1lbnQgPVxuICAgIC8vICAgICAgICAgQXJyYXkuZnJvbShkb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKCd2anMtcGxheS1jb250cm9sIHZqcy1jb250cm9sIHZqcy1idXR0b24nKSlbMF0gYXMgSFRNTEJ1dHRvbkVsZW1lbnQ7XG4gICAgLy8gICAgICAgcGxheUJ1dHRvbkJvdHRvbS5jbGljaygpO1xuICAgIC8vICAgICAgIHRoaXMuX3ZpZGVvanMucGxheSgpLmNhdGNoKCgpID0+IHtcbiAgICAvLyAgICAgICAgIHRoaXMuX3ZpZGVvVGFnLnZvbHVtZSA9IDA7XG4gICAgLy8gICAgICAgICB0aGlzLl9kZWxheWVkUGxheWVkID0gZmFsc2U7XG4gICAgLy8gICAgICAgICB0aGlzLl9pc05vdEluaXRpYWxCdWZmZXJpbmcgPSBmYWxzZTtcbiAgICAvLyAgICAgICB9KTtcbiAgICAvLyAgICAgfVxuICAgIC8vICAgfVxuXG4gICAgLy8gICB0aGlzLm9uVGltZVVwZGF0ZWQodGhpcy5jdXJyZW50VGltZSwgYnVmZmVyRW5kKTtcbiAgICAvLyAgIC8vIGNvbnN0IGJ1ZmZlclN0YXJ0UHJvdCA9IGJ1ZmZlcmVkLl9fcHJvdF9fLmVuZCA/IGJ1ZmZlcmVkLl9fcHJvdF9fLmVuZCgwKSA6ICduw6NvIHJvbG91JztcbiAgICAvLyAgIC8vICAgICAgICBjb25zb2xlLmxvZygnYnVmZmVyZWQ6ICcsIGJ1ZmZlclN0YXJ0LCBidWZmZXJFbmQsIGJ1ZmZlcmVkLmxlbmd0aCk7XG4gICAgLy8gfSk7XG5cbiAgICAvLyB0aGlzLl92aWRlb2pzLm9uKCdzZWVraW5nJywgKCkgPT4ge1xuICAgIC8vICAgdGhpcy5zZWVrU3RhcnQgPSB0aGlzLnByZXZpb3VzVGltZTtcblxuICAgIC8vICAgLy8gICAgICAgIGNvbnNvbGUubG9nKCdzZWVraW5nOiAnICsgdGhpcy5jdXJyZW50VGltZSk7XG4gICAgLy8gfSk7XG4gICAgLy8gdGhpcy5fdmlkZW9qcy5vbignc2Vla2VkJywgKCkgPT4ge1xuICAgIC8vICAgLy8gICAgICAgIGNvbnNvbGUubG9nKCdzZWVrZWQgZnJvbScsIHRoaXMuc2Vla1N0YXJ0LCAndG8nLCB0aGlzLmN1cnJlbnRUaW1lLCAnOyBkZWx0YTonLCB0aGlzLmN1cnJlbnRUaW1lIC0gdGhpcy5zZWVrU3RhcnQpO1xuICAgIC8vIH0pO1xuXG4gICAgLy8gdGhpcy5fdmlkZW9qcy5vbignbG9hZGVkbWV0YWRhdGEnLCAoKSA9PiB7XG4gICAgLy8gICB0aGlzLl9ha2lyeUNvbnNvbGUuYWRkTG9ncygnTG9hZGVkIE1ldGFkYXRhLicpO1xuICAgIC8vICAgLy8gIGNvbnNvbGUubG9nKCdsb2FkZWRtZXRhZGF0YS4nKTtcblxuICAgIC8vICAgaWYgKHRoaXMuX3BsYXllck9wdGlvbnMuYXV0b3BsYXkpIHtcbiAgICAvLyAgICAgdGhpcy5wbGF5KCk7XG4gICAgLy8gICB9XG4gICAgLy8gfSk7XG5cbiAgICAvLyB0aGlzLl92aWRlb2pzLm9uKCdwbGF5JywgKCkgPT4ge1xuICAgIC8vICAgdGhpcy5fYWtpcnlDb25zb2xlLmFkZExvZ3MoJ3BsYXkuJyk7XG4gICAgLy8gICAvLyAgICAgIGNvbnNvbGUubG9nKCdwbGF5LicpO1xuICAgIC8vIH0pO1xuXG4gICAgLy8gdGhpcy5fdmlkZW9qcy5vbigndm9sdW1lY2hhbmdlJywgKCkgPT4ge1xuICAgIC8vICAgdGhpcy5fYWtpcnlDb25zb2xlLmFkZExvZ3MoJ3ZvbHVtZWNoYW5nZS4nKTtcbiAgICAvLyAgIC8vICAgICAgY29uc29sZS5sb2coJ3ZvbHVtZWNoYW5nZS4nKTtcbiAgICAvLyB9KTtcblxuICAgIC8vIHRoaXMuX3ZpZGVvanMub24oJ3VzZXJhY3RpdmUnLCAoKSA9PiB7XG4gICAgLy8gICAvLyAgICAgIGNvbnNvbGUubG9nKCd1c2VyYWN0aXZlLicpO1xuICAgIC8vIH0pO1xuXG4gICAgLy8gdGhpcy5fdmlkZW9qcy5vbigndXNlcmluYWN0aXZlJywgKCkgPT4ge1xuICAgIC8vICAgLy8gICAgICBjb25zb2xlLmxvZygndXNlcmluYWN0aXZlLicpO1xuICAgIC8vIH0pO1xuXG4gICAgLy8gdGhpcy5fdmlkZW9qcy5vbignZXJyb3InLCAoKSA9PiB7XG4gICAgLy8gICAvLyAgICAgIGNvbnNvbGUubG9nKCdlcnJvci4nKTtcbiAgICAvLyAgIGlmICh0aGlzLl9wbGF5ZXJPcHRpb25zICYmIHRoaXMuX3BsYXllck9wdGlvbnMuY2FsbGJhY2tzICYmIHRoaXMuX3BsYXllck9wdGlvbnMuY2FsbGJhY2tzLm9uRXJyb3IpIHtcbiAgICAvLyAgICAgdGhpcy5fcGxheWVyT3B0aW9ucy5jYWxsYmFja3Mub25FcnJvcigpO1xuICAgIC8vICAgfVxuICAgIC8vIH0pO1xuXG4gICAgLy8gdGhpcy5fdmlkZW9qcy5vbigncGF1c2UnLCAoKSA9PiB7XG4gICAgLy8gICAvLyAgICAgIGNvbnNvbGUubG9nKCdwYXVzZS4nKTtcbiAgICAvLyB9KTtcblxuICAgIC8vIHRoaXMuX3ZpZGVvanMub24oJ2Z1bGxzY3JlZW5jaGFuZ2UnLCAoKSA9PiB7XG4gICAgLy8gICAvLyAgICAgIGNvbnNvbGUubG9nKCdmdWxsc2NyZWVuY2hhbmdlLicpO1xuICAgIC8vIH0pO1xuXG4gICAgLy8gdGhpcy5fdmlkZW9qcy5vbignd2FpdGluZycsICgpID0+IHtcbiAgICAvLyAgIC8vICAgICAgY29uc29sZS5sb2coJ3dhaXRpbmcuJywgdGhpcy5fdmlkZW9qcyk7XG4gICAgLy8gfSk7XG5cbiAgICAvLyB0aGlzLl92aWRlb2pzLm9uKCdwbGF5aW5nJywgKCkgPT4ge1xuICAgIC8vICAgY29uc29sZS5sb2coJ3BsYXlpbmcuJywgdGhpcy5fdmlkZW9qcyk7XG4gICAgLy8gfSk7XG5cbiAgICAvLyB0aGlzLl92aWRlb2pzLm9uKCdzdXNwZW5kJywgKCkgPT4ge1xuICAgIC8vICAgY29uc29sZS5sb2coJ3N1c3BlbmQuJywgdGhpcy5fdmlkZW9qcyk7XG4gICAgLy8gfSk7XG5cbiAgICAvLyB0aGlzLl92aWRlb2pzLm9uKCdzdGFsbGVkJywgKCkgPT4ge1xuICAgIC8vICAgY29uc29sZS5sb2coJ3N0YWxsZWQuJywgdGhpcy5fdmlkZW9qcyk7XG4gICAgLy8gfSk7XG5cbiAgICAvLyB0aGlzLl92aWRlb2pzLm9uKCdyZWFkeScsICgpID0+IHtcbiAgICAvLyAgIC8vICAgICAgY29uc29sZS5sb2coJ3JlYWR5LicpO1xuICAgIC8vIH0pO1xuICB9XG5cbiAgcHVibGljIHNldE9uVGltZVVwZGF0ZWRMaXN0ZW5lcihvblRpbWVVcGRhdGU6IEZ1bmN0aW9uKSB7XG4gICAgdGhpcy5faW50ZXJuYWxMaXN0ZW5lcnMub25UaW1lVXBkYXRlID0gb25UaW1lVXBkYXRlO1xuICB9XG5cbiAgcHVibGljIHNldE9uV2FpdGluZ0xpc3RlbmVyKG9uV2FpdGluZzogRnVuY3Rpb24pIHtcbiAgICB0aGlzLl9pbnRlcm5hbExpc3RlbmVycy5vbldhaXRpbmcgPSBvbldhaXRpbmc7XG4gIH1cblxuICBwdWJsaWMgb25UaW1lVXBkYXRlZCgpIHtcbiAgICBjb25zdCBjdXJyZW50VGltZSA9IHRoaXMuX3ZpZGVvVGFnLmN1cnJlbnRUaW1lO1xuICAgIGNvbnN0IGJ1ZmZlcmVkVGltZSA9IHRoaXMuX3ZpZGVvVGFnLmJ1ZmZlcmVkICYmIHRoaXMuX3ZpZGVvVGFnLmJ1ZmZlcmVkLmxlbmd0aCA+IDAgPyB0aGlzLl92aWRlb1RhZy5idWZmZXJlZC5lbmQoMCkgOiAwO1xuICAgIGNvbnN0IGR1cmF0aW9uID0gdGhpcy5fdmlkZW9UYWcuZHVyYXRpb247XG4gICAgY29uc3QgdmlkZW9IZWlnaHQgPSB0aGlzLl92aWRlb1RhZy52aWRlb0hlaWdodDtcblxuICAgIHRoaXMuX2FraXJ5Q29uc29sZS5zZXRQbGF5ZXJTdGF0dXMoY3VycmVudFRpbWUsIGJ1ZmZlcmVkVGltZSwgZHVyYXRpb24sIHZpZGVvSGVpZ2h0KTtcblxuICAgIGlmICh0aGlzLl9pbnRlcm5hbExpc3RlbmVycy5vblRpbWVVcGRhdGUpIHtcbiAgICAgIHRoaXMuX2ludGVybmFsTGlzdGVuZXJzLm9uVGltZVVwZGF0ZShjdXJyZW50VGltZSwgYnVmZmVyZWRUaW1lKTtcbiAgICB9XG4gIH1cblxuXG5cbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAqXG4gICAqXG4gICAqIFNIQUtBIEFSRUFcbiAgICpcbiAgICpcbiAgICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG5cbiAgLyoqXG4gICAqIFByaXZhdGUgbWV0aG9kIGZvciBidWlsZCBTaGFrYSBwbGF5ZXIgYW5kIHByZXBhcmUgdG8gc3RhcnQuXG4gICAqIEBwYXJhbSB2aWRlbyBWaWRlbyBvYmplY3Qgd2l0aCBzb3VyY2UgbGluayAoc3JjKSBhbmQgdHlwZSAoaGxzLCBkYXNoLCBtcDQpXG4gICAqL1xuICBwcml2YXRlIGxvYWRTaGFrYSh1cmwpIHtcbiAgICAvLyBJbnN0YWxsIGJ1aWx0LWluIHBvbHlmaWxscyB0byBwYXRjaCBicm93c2VyIGluY29tcGF0aWJpbGl0aWVzLlxuICAgIHNoYWthLnBvbHlmaWxsLmluc3RhbGxBbGwoKTtcblxuICAgIC8vIENoZWNrIHRvIHNlZSBpZiB0aGUgYnJvd3NlciBzdXBwb3J0cyB0aGUgYmFzaWMgQVBJcyBTaGFrYSBuZWVkcy5cbiAgICBpZiAoIXNoYWthLlBsYXllci5pc0Jyb3dzZXJTdXBwb3J0ZWQoKSkge1xuICAgICAgLy8gVGhpcyBicm93c2VyIGRvZXMgbm90IGhhdmUgdGhlIG1pbmltdW0gc2V0IG9mIEFQSXMgd2UgbmVlZC5cbiAgICAgIGNvbnNvbGUuZXJyb3IoJ0Jyb3dzZXIgbm90IHN1cHBvcnRlZCEnKTtcblxuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vIEV2ZXJ5dGhpbmcgbG9va3MgZ29vZCFcbiAgICBzd2l0Y2ggKHRoaXMuX3BsYXllck9wdGlvbnMubW9kZSkge1xuICAgICAgY2FzZSBDLlRZUEUuSExTOlxuICAgICAgICB0aGlzLmJ1aWxkU2hha2FQbGF5ZXIodXJsKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIEMuVFlQRS5EQVNIOlxuICAgICAgICB0aGlzLmJ1aWxkU2hha2FQbGF5ZXIodXJsKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIEMuVFlQRS5NUDQ6XG4gICAgICAgIHRoaXMuYnVpbGRTaGFrYVBsYXllcih1cmwpO1xuICAgICAgICBicmVhaztcbiAgICAgIGRlZmF1bHQ6XG4gICAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgYnVpbGRTaGFrYVBsYXllcih1cmwpIHtcbiAgICBjb25zdCBvbkVycm9yID0gKGVycm9yKSA9PiB7XG4gICAgICAvLyBMb2cgdGhlIGVycm9yLlxuICAgICAgY29uc29sZS5lcnJvcignRXJyb3IgY29kZScsIGVycm9yLmNvZGUsICdvYmplY3QnLCBlcnJvcik7XG4gICAgfTtcblxuICAgIGNvbnN0IG9uRXJyb3JFdmVudCA9IChldmVudCkgPT4ge1xuICAgICAgLy8gRXh0cmFjdCB0aGUgc2hha2EudXRpbC5FcnJvciBvYmplY3QgZnJvbSB0aGUgZXZlbnQuXG4gICAgICBvbkVycm9yKGV2ZW50LmRldGFpbCk7XG4gICAgfTtcblxuICAgIC8vIENyZWF0ZSBhIFBsYXllciBpbnN0YW5jZS5cbiAgICB0aGlzLl9zaGFrYSA9IG5ldyBzaGFrYS5QbGF5ZXIodGhpcy5fdmlkZW9UYWcpO1xuXG4gICAgLy8gQXR0YWNoIHBsYXllciB0byB0aGUgd2luZG93IHRvIG1ha2UgaXQgZWFzeSB0byBhY2Nlc3MgaW4gdGhlIEpTIGNvbnNvbGUuXG4gICAgKHdpbmRvdyBhcyBhbnkpLnBsYXllciA9IHRoaXMuX3NoYWthO1xuXG4gICAgLy8gTGlzdGVuIGZvciBlcnJvciBldmVudHMuXG4gICAgdGhpcy5fc2hha2EuYWRkRXZlbnRMaXN0ZW5lcignZXJyb3InLCBvbkVycm9yRXZlbnQpO1xuXG4gICAgLy8gQ29uZmlndXJlIGJ1ZmZlciBsZW5ndGhcbiAgICB0aGlzLl9zaGFrYS5jb25maWd1cmUoe1xuICAgICAgc3RyZWFtaW5nOiB7XG4gICAgICAgIGJ1ZmZlcmluZ0dvYWw6IDEyMCxcbiAgICAgICAgcmVidWZmZXJpbmdHb2FsOiAxNSxcbiAgICAgICAgcmV0cnlQYXJhbWV0ZXJzOiB7XG4gICAgICAgICAgdGltZW91dDogMCwgICAgICAgLy8gdGltZW91dCBpbiBtcywgYWZ0ZXIgd2hpY2ggd2UgYWJvcnQ7IDAgbWVhbnMgbmV2ZXJcbiAgICAgICAgICBtYXhBdHRlbXB0czogNCwgICAvLyB0aGUgbWF4aW11bSBudW1iZXIgb2YgcmVxdWVzdHMgYmVmb3JlIHdlIGZhaWxcbiAgICAgICAgICBiYXNlRGVsYXk6IDUwMCwgIC8vIHRoZSBiYXNlIGRlbGF5IGluIG1zIGJldHdlZW4gcmV0cmllc1xuICAgICAgICAgIGJhY2tvZmZGYWN0b3I6IDIsIC8vIHRoZSBtdWx0aXBsaWNhdGl2ZSBiYWNrb2ZmIGZhY3RvciBiZXR3ZWVuIHJldHJpZXNcbiAgICAgICAgICBmdXp6RmFjdG9yOiAwLjUsICAvLyB0aGUgZnV6eiBmYWN0b3IgdG8gYXBwbHkgdG8gZWFjaCByZXRyeSBkZWxheVxuICAgICAgICB9LFxuICAgICAgfSxcbiAgICB9KTtcblxuICAgIC8vIENvbmZpZ3VyZSBhbGwgcmVxdWVzdHNcbiAgICB0aGlzLl9zaGFrYS5nZXROZXR3b3JraW5nRW5naW5lKCkucmVnaXN0ZXJSZXF1ZXN0RmlsdGVyKCh0eXBlLCByZXF1ZXN0KSA9PiB7XG4gICAgICBpZiAodGhpcy5fcGxheWVyT3B0aW9ucy50b2tlbikge1xuICAgICAgICBpZiAoIXJlcXVlc3QuaGVhZGVycykgeyByZXF1ZXN0LmhlYWRlciA9IHt9OyB9XG4gICAgICAgIHJlcXVlc3QuaGVhZGVyc1snQXV0aG9yaXphdGlvbiddID0gJ0JlYXJlciAnICsgdGhpcy5fcGxheWVyT3B0aW9ucy50b2tlbjtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIC8vIFRyeSB0byBsb2FkIGEgbWFuaWZlc3QuXG4gICAgLy8gVGhpcyBpcyBhbiBhc3luY2hyb25vdXMgcHJvY2Vzcy5cbiAgICB0aGlzLl9zaGFrYS5sb2FkKHVybCkudGhlbigoKSA9PiB7XG4gICAgICAvLyBUaGlzIHJ1bnMgaWYgdGhlIGFzeW5jaHJvbm91cyBsb2FkIGlzIHN1Y2Nlc3NmdWwuXG4gICAgICBjb25zb2xlLmluZm8oJ1RoZSB2aWRlbyBoYXMgbm93IGJlZW4gbG9hZGVkIScpOyAvLyB0c2xpbnQ6ZGlzYWJsZS1saW5lOm5vLWNvbnNvbGVcbiAgICB9KS5jYXRjaChvbkVycm9yKTsgIC8vIG9uRXJyb3IgaXMgZXhlY3V0ZWQgaWYgdGhlIGFzeW5jaHJvbm91cyBsb2FkIGZhaWxzLlxuICB9XG5cblxuXG5cblxuXG5cblxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcbiAgICpcbiAgICpcbiAgICogVklERU9KUyBBUkVBXG4gICAqXG4gICAqXG4gICAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xuXG4gIC8qKlxuICAgKiBQcml2YXRlIG1ldGhvZCBmb3IgYnVpbGQgVmlkZW9KcyBwbGF5ZXIgYW5kIHByZXBhcmUgdG8gc3RhcnQuXG4gICAqIEBwYXJhbSB2aWRlbyBWaWRlbyBvYmplY3Qgd2l0aCBzb3VyY2UgbGluayAoc3JjKSBhbmQgdHlwZSAoaGxzLCBkYXNoLCBtcDQpXG4gICAqL1xuICBwcml2YXRlIGxvYWRWaWRlb0pzKHVybCkge1xuICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMuX3ZpZGVvVGFnLCAndmlkZW8tanMnKTtcbiAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLl92aWRlb1RhZywgJ3Zqcy1kZWZhdWx0LXNraW4nKTtcblxuICAgIHN3aXRjaCAodGhpcy5fcGxheWVyT3B0aW9ucy5tb2RlKSB7XG4gICAgICBjYXNlIEMuVFlQRS5ITFM6XG4gICAgICAgIHRoaXMuYnVpbGRWanNIbHNQbGF5ZXIodXJsKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBjYXNlIEMuVFlQRS5NUDQ6XG4gICAgICAgIHRoaXMuYnVpbGRWanNNcDRQbGF5ZXIodXJsKTtcbiAgICAgICAgYnJlYWs7XG4gICAgICBkZWZhdWx0OlxuICAgICAgICB0aGlzLmJ1aWxkVmpzTXA0UGxheWVyKHVybCk7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cblxuICB9XG5cbiAgLyoqXG4gICAqIE1ldGhvZCBmb3IgYnVpbGQgSExTIFBsYXllciB3aXRoIFZpZGVvSlNcbiAgICogQHBhcmFtIHZpZGVvXG4gICAqL1xuICBwcml2YXRlIGJ1aWxkVmpzSGxzUGxheWVyKHVybCkge1xuICAgIGNvbnN0IG92ZXJyaWRlTmF0aXZlID0gdHJ1ZTsgIC8vIFRoaXMgb3B0aW9uIGlzIHJlcXVpcmVkIGZvciBBbmRyb2lkXG5cbiAgICB0aGlzLl9vcHRzID0ge1xuICAgICAgaHRtbDU6IHtcbiAgICAgICAgaGxzOiB7XG4gICAgICAgICAgZGVidWc6IHRydWUsXG4gICAgICAgICAgLy8gYmFuZHdpZHRoOiA1MDAwMDAsIC8vIGluaXRpYWwgYmFuZHdpZHRoXG4gICAgICAgICAgb3ZlcnJpZGVOYXRpdmU6IG92ZXJyaWRlTmF0aXZlLCAgLy8gVGhpcyBvcHRpb24gaXMgcmVxdWlyZWQgZm9yIEFuZHJvaWRcbiAgICAgICAgfSxcbiAgICAgICAgbmF0aXZlVmlkZW9UcmFja3M6ICFvdmVycmlkZU5hdGl2ZSxcbiAgICAgICAgbmF0aXZlQXVkaW9UcmFja3M6ICFvdmVycmlkZU5hdGl2ZSxcbiAgICAgICAgbmF0aXZlVGV4dFRyYWNrczogIW92ZXJyaWRlTmF0aXZlLFxuICAgICAgfSxcbiAgICAgIGJpZ1BsYXlCdXR0b246IHRoaXMuX3BsYXllck9wdGlvbnMuYmlnUGxheUJ1dHRvbiA9PT0gdW5kZWZpbmVkID8gdHJ1ZSA6IHRoaXMuX3BsYXllck9wdGlvbnMuYmlnUGxheUJ1dHRvbixcbiAgICAgIGF1dG9wbGF5OiB0aGlzLl9wbGF5ZXJPcHRpb25zLmF1dG9wbGF5IHx8IGZhbHNlLFxuICAgICAgcGxheXNpbmxpbmU6IHRydWUsXG4gICAgICBlcnJvckRpc3BsYXk6ICF0aGlzLl9wbGF5ZXJPcHRpb25zLnByb2QsXG4gICAgfTtcblxuICAgIHZpZGVvanMuSGxzLnhocignZXJyb3InLCAoYW55dGhpbmcpID0+IHsgY29uc29sZS5lcnJvcignRXJybyBYSFIgY2FwdHVyYWRvLicsIGFueXRoaW5nKTsgfSk7XG5cblxuICAgIHZpZGVvanMuSGxzLnhoci5iZWZvcmVSZXF1ZXN0ID0gKG9wdGlvbnMpID0+IHtcbiAgICAgIGlmICghaXNVbmRlZmluZWQodGhpcy5fcGxheWVyT3B0aW9ucy50b2tlbikgJiYgIWlzVW5kZWZpbmVkKG9wdGlvbnMpKSB7XG4gICAgICAgIGlmIChpc1VuZGVmaW5lZChvcHRpb25zLmhlYWRlcnMpKSB7XG4gICAgICAgICAgb3B0aW9ucy5oZWFkZXJzID0ge307XG4gICAgICAgIH1cbiAgICAgICAgb3B0aW9ucy5oZWFkZXJzLkF1dGhvcml6YXRpb24gPSAnQmVhcmVyICcgKyB0aGlzLl9wbGF5ZXJPcHRpb25zLnRva2VuO1xuICAgICAgICBvcHRpb25zLnRpbWVvdXQgPSA0NTAwMDtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgLyoqXG4gICAgICogSW5pdGlhbGl6ZSBwbGF5ZXJcbiAgICAgKi9cbiAgICB0aGlzLl92aWRlb2pzID0gdmlkZW9qcygnYWtpcnlQbGF5ZXJJZCcsIHRoaXMuX29wdHMpO1xuXG4gICAgdGhpcy5fdmlkZW9qcy5zcmMoe1xuICAgICAgc3JjOiB1cmwsXG4gICAgICB0eXBlOiAnYXBwbGljYXRpb24veC1tcGVnVVJMJyxcbiAgICAgIHdpdGhDcmVkZW50aWFsczogZmFsc2UsXG4gICAgfSk7XG5cbiAgICAvKipcbiAgICAgKiBPblJlYWR5IC0+IEJlZm9yZSB0aGUgZG93bmxvYWQgb2YgZmlyc3QgY2h1bmtcbiAgICAgKi9cbiAgICB0aGlzLl92aWRlb2pzLnJlYWR5KCgpID0+IHtcbiAgICAgIHRoaXMuX2FraXJ5Q29uc29sZS5hZGRMb2dzKCdFeGVjdXRpbmcgb25SZWFkeS4uLicpO1xuXG4gICAgICAvLyBmb3JjZSB0byBzaG93IGJvdHRvbSBjb250cm9sc1xuICAgICAgY29uc3QgdmpzQ29udHJvbEJhcnMgPSBBcnJheS5mcm9tKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy52anMtZGVmYXVsdC1za2luLnZqcy1wYXVzZWQgLnZqcy1jb250cm9sLWJhcicpKTtcbiAgICAgIGZvciAoY29uc3QgdmpzQ29udHJvbEJhciBvZiB2anNDb250cm9sQmFycykge1xuICAgICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZSh2anNDb250cm9sQmFyLCAnZGlzcGxheScsICdmbGV4Jyk7XG4gICAgICB9XG5cbiAgICAgIC8vIG1ldGhvZCB0byBzdGFydCBpbml0aWFsIGJ1ZmZlcmluZyBvbiBpT1MgYnV0IGl0IGFsc28gYXV0b3N0YXJ0IG9uIG90aGVycyBkZXZpY2VzXG4gICAgICBjb25zdCBwbGF5QnV0dG9uQm90dG9tOiBIVE1MQnV0dG9uRWxlbWVudCA9XG4gICAgICAgIEFycmF5LmZyb20oZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgndmpzLXBsYXktY29udHJvbCB2anMtY29udHJvbCB2anMtYnV0dG9uJykpWzBdIGFzIEhUTUxCdXR0b25FbGVtZW50O1xuICAgICAgLy8gcGxheUJ1dHRvbkJvdHRvbS5jbGljaygpO1xuXG4gICAgICAvKipcbiAgICAgICAqIFNldHVwIGFkYXB0YXRpb24gc3RyYXRlZ3lcbiAgICAgICAqL1xuICAgICAgaWYgKHRoaXMuX3ZpZGVvanMgJiYgdGhpcy5fdmlkZW9qcy50ZWNoXyAmJiB0aGlzLl92aWRlb2pzLnRlY2hfLmhscykge1xuICAgICAgICB0aGlzLl92aWRlb2pzLnRlY2hfLmhscy5zZWxlY3RQbGF5bGlzdCA9ICgpID0+IHtcbiAgICAgICAgICBjb25zdCBobHMgPSB0aGlzLl92aWRlb2pzLnRlY2hfLmhscztcbiAgICAgICAgICBjb25zdCBwbGF5bGlzdHMgPSBobHMucGxheWxpc3RzLm1hc3Rlci5wbGF5bGlzdHM7XG4gICAgICAgICAgY29uc3QgZm9ybWF0cyA9IHBsYXlsaXN0cy5sZW5ndGg7XG4gICAgICAgICAgY29uc3QgY3VycmVudFRpbWUgPSB0aGlzLl92aWRlb2pzLmNhY2hlXy5jdXJyZW50VGltZTtcbiAgICAgICAgICBjb25zdCBiYW5kd2lkdGggPSBobHMuYmFuZHdpZHRoO1xuXG4gICAgICAgICAgY29uc29sZS5sb2cocGxheWxpc3RzKTtcblxuICAgICAgICAgIGxldCBzZWxlY3RlZFBsYXlsaXN0OiBudW1iZXI7XG5cbiAgICAgICAgICBpZiAodGhpcy5jdXJyZW50UGxheWxpc3QgPCBwbGF5bGlzdHMubGVuZ3RoKSB7XG4gICAgICAgICAgICBzZWxlY3RlZFBsYXlsaXN0ID0gdGhpcy5jdXJyZW50UGxheWxpc3Q7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0N1cnJlbnQgcGxheWxpc3QgZG9lc25cXCd0IGV4aXN0Jyk7XG4gICAgICAgICAgICBzZWxlY3RlZFBsYXlsaXN0ID0gMDtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyAgIHRoaXMuX2FraXJ5Q29uc29sZS5hZGRMb2dzKEpTT04uc3RyaW5naWZ5KHsgY3VycmVudFRpbWUsIGZvcm1hdHMsIGJhbmR3aWR0aCB9KSk7XG5cbiAgICAgICAgICBpZiAodGhpcy5fcGxheWVyT3B0aW9ucy5ldmFsdWF0b3IpIHtcbiAgICAgICAgICAgIHNlbGVjdGVkUGxheWxpc3QgPSB0aGlzLl9wbGF5ZXJPcHRpb25zLmV2YWx1YXRvci5ldmFsdWF0ZShcbiAgICAgICAgICAgICAgY3VycmVudFRpbWUsXG4gICAgICAgICAgICAgIDAsXG4gICAgICAgICAgICAgIHBsYXlsaXN0cyxcbiAgICAgICAgICAgICAgYmFuZHdpZHRoLFxuICAgICAgICAgICAgKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICAvLyAgIHRoaXMuX2FraXJ5Q29uc29sZS5hZGRMb2dzKFxuICAgICAgICAgIC8vICAgICAnQmFuZHdpZHRoOiAnICsgYmFuZHdpZHRoICtcbiAgICAgICAgICAvLyAgICAgJ1xcblNlbGVjdGVkOiAnICsgKHNlbGVjdGVkUGxheWxpc3QgKyAxKSArICcvJyArIHBsYXlsaXN0cy5sZW5ndGgsXG4gICAgICAgICAgLy8gICApO1xuXG4gICAgICAgICAgcmV0dXJuIHBsYXlsaXN0c1tzZWxlY3RlZFBsYXlsaXN0XTtcbiAgICAgICAgfTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMuX2FraXJ5Q29uc29sZS5hZGRMb2dzKCdBa2lyeSBBZGFwdGF0aW9uIGlzIGRpc2FibGVkLiBOYXRpdmUgcGxheWVyIGV4ZWN1dGluZy4nKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5idWlsZFBsYXllclNraW4oKTtcblxuICAgICAgaWYgKHRoaXMuX3BsYXllck9wdGlvbnMuY2FsbGJhY2tzICYmIHRoaXMuX3BsYXllck9wdGlvbnMuY2FsbGJhY2tzLm9uUmVhZHkpIHtcbiAgICAgICAgdGhpcy5fcGxheWVyT3B0aW9ucy5jYWxsYmFja3Mub25SZWFkeSgpO1xuICAgICAgfVxuXG4gICAgICAvLyB0aGlzLl92aWRlb2pzLnBsYXkoKTtcbiAgICAgIC8vIHRoaXMuX3ZpZGVvVGFnLnBsYXkoKTtcbiAgICAgIC8vIGNvbnN0IHBsYXlCdXR0b246IEhUTUxCdXR0b25FbGVtZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgndmpzLWJpZy1wbGF5LWJ1dHRvbicpWzBdIGFzIEhUTUxCdXR0b25FbGVtZW50O1xuICAgICAgLy8gcGxheUJ1dHRvbi5jbGljaygpO1xuICAgICAgLy8gcGxheUJ1dHRvbi5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICAgICAgdGhpcy5fYWtpcnlDb25zb2xlLmFkZExvZ3MoJ29uUmVhZHkgZXhlY3V0ZWQuJyk7XG4gICAgfSk7XG5cbiAgICAvLyB0aGlzLl92aWRlb1RhZy5sb2FkKCk7IC8vIE7Do28gb2JyaWdhdMOzcmlvXG4gIH1cblxuICAvKipcbiAgICogTWV0aG9kIGZvciBidWlsZCBITFMgUGxheWVyIHdpdGggVmlkZW9KU1xuICAgKiBAcGFyYW0gdmlkZW9cbiAgICovXG4gIHByaXZhdGUgYnVpbGRWanNNcDRQbGF5ZXIodXJsKSB7XG4gICAgLy8gY29uc29sZS5sb2coJ3RoaXMuYnVpbGRNcDRQbGF5ZXInKTtcblxuICAgIHRoaXMuX3ZpZGVvanMgPSB2aWRlb2pzKHRoaXMuX3ZpZGVvVGFnKTtcbiAgICB0aGlzLl92aWRlb2pzLnNyYyh1cmwpO1xuICB9XG5cblxuXG5cblxuXG5cbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAqXG4gICAqXG4gICAqIGlPUyBOQVRJVkUgSExTIEFSRUFcbiAgICpcbiAgICpcbiAgICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4gIHByaXZhdGUgbG9hZE5hdGl2ZUZvcl9pT1ModXJsKSB7XG4gICAgdXJsID0gaU9TVG9vbHMucHJvY2Vzc1N0cmluZ0ZvclBsYXRmb3JtKHVybCk7XG5cbiAgICB0aGlzLl92aWRlb1RhZy5zcmMgPSB1cmw7XG4gICAgdGhpcy5fdmlkZW9UYWcubG9hZCgpO1xuICAgIHRoaXMuX2FraXJ5Q29uc29sZS5hZGRMb2dzKCdTdGFydGluZyBJT1MnKTtcbiAgfVxufVxuIl19