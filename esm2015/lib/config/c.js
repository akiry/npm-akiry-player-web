/**
 * @fileoverview added by tsickle
 * Generated from: lib/config/c.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const C = {
    PLAYERS: {
        SHAKA: { name: 'shaka' },
        VIDEOJS: { name: 'videojs' },
    },
    TYPE: {
        HLS: { name: 'hls' },
        DASH: { name: 'dash' },
        MP4: { name: 'mp4' },
    },
    teste: {
        loginType: '',
    },
};
export { C };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FraXJ5LXBsYXllci8iLCJzb3VyY2VzIjpbImxpYi9jb25maWcvYy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7TUFBTSxDQUFDLEdBQUc7SUFDTixPQUFPLEVBQUU7UUFDTCxLQUFLLEVBQUUsRUFBQyxJQUFJLEVBQUUsT0FBTyxFQUFDO1FBQ3RCLE9BQU8sRUFBRSxFQUFDLElBQUksRUFBRSxTQUFTLEVBQUM7S0FDN0I7SUFDRCxJQUFJLEVBQUU7UUFDRixHQUFHLEVBQUUsRUFBQyxJQUFJLEVBQUUsS0FBSyxFQUFDO1FBQ2xCLElBQUksRUFBRSxFQUFDLElBQUksRUFBRSxNQUFNLEVBQUM7UUFDcEIsR0FBRyxFQUFFLEVBQUMsSUFBSSxFQUFFLEtBQUssRUFBQztLQUNyQjtJQUNELEtBQUssRUFBRTtRQUNILFNBQVMsRUFBRSxFQUFFO0tBQ2hCO0NBQ0o7QUFFRCxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBDID0ge1xuICAgIFBMQVlFUlM6IHtcbiAgICAgICAgU0hBS0E6IHtuYW1lOiAnc2hha2EnfSxcbiAgICAgICAgVklERU9KUzoge25hbWU6ICd2aWRlb2pzJ30sXG4gICAgfSxcbiAgICBUWVBFOiB7XG4gICAgICAgIEhMUzoge25hbWU6ICdobHMnfSxcbiAgICAgICAgREFTSDoge25hbWU6ICdkYXNoJ30sXG4gICAgICAgIE1QNDoge25hbWU6ICdtcDQnfSxcbiAgICB9LFxuICAgIHRlc3RlOiB7XG4gICAgICAgIGxvZ2luVHlwZTogJycsXG4gICAgfSxcbn07XG5cbmV4cG9ydCB7IEMgfTtcbiJdfQ==