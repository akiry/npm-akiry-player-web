export * from './lib/akiry-player.module';
export * from './lib/config/c';
export * from './lib/libs/utils/ios';
export * from './lib/browser/console-logs-manager';
export * from './lib/services/akiry-player/akiry-player.service';
export * from './lib/components/akiry-player/akiry-player.component';
