import { ElementRef, Renderer2 } from '@angular/core';
import { AkiryPlayerConsoleService } from '../../services/akiry-player-console/akiry-player-console.service';
export declare class AkiryPlayerDirective {
    private readonly _renderer;
    private readonly _akiryConsole;
    private readonly _videoTag;
    private _videojs;
    private _shaka;
    private _opts;
    private _playerOptions;
    private readonly _internalListeners;
    private currentPlaylist;
    constructor(el: ElementRef, _renderer: Renderer2, _akiryConsole: AkiryPlayerConsoleService);
    /**
     * Play, if the player is paused, and pause, if playing
     */
    playOrPause(): void;
    fullscreen(): void;
    /**
     * Play only
     */
    play(): Promise<void>;
    /**
     * Pause only
     */
    pause(): Promise<void>;
    /**
     * Method to unbuild player, for release resources
     */
    release(): void;
    /**
     * Method for build player and prepare to start.
     * @param video Video object with source link (src) and type (hls, dash, mp4)
     * @param opt Options for play (player, has mode, adaptive algorithm, etc)
     */
    load(url: any, playerOptions: any): void;
    /********************************************************
     *
     *
     * GENERAL AREA
     *
     *
     **********************************************************/
    private generalBuild;
    private buildPlayerSkin;
    private buildListeners;
    setOnTimeUpdatedListener(onTimeUpdate: Function): void;
    setOnWaitingListener(onWaiting: Function): void;
    onTimeUpdated(): void;
    /********************************************************
     *
     *
     * SHAKA AREA
     *
     *
     **********************************************************/
    /**
     * Private method for build Shaka player and prepare to start.
     * @param video Video object with source link (src) and type (hls, dash, mp4)
     */
    private loadShaka;
    private buildShakaPlayer;
    /********************************************************
     *
     *
     * VIDEOJS AREA
     *
     *
     **********************************************************/
    /**
     * Private method for build VideoJs player and prepare to start.
     * @param video Video object with source link (src) and type (hls, dash, mp4)
     */
    private loadVideoJs;
    /**
     * Method for build HLS Player with VideoJS
     * @param video
     */
    private buildVjsHlsPlayer;
    /**
     * Method for build HLS Player with VideoJS
     * @param video
     */
    private buildVjsMp4Player;
    /********************************************************
     *
     *
     * iOS NATIVE HLS AREA
     *
     *
     **********************************************************/
    private loadNativeFor_iOS;
}
