export interface Evaluator {
    evaluate: (currentTime: number, bufferTime: number, tracks: any, bandwidth: any) => number;
}
export interface AkiryPlayerOptions {
    player: any;
    mode: any;
    evaluator?: Evaluator;
    thumb?: string;
    token?: string;
    bigPlayButton?: boolean;
    autoplay?: boolean;
    callbacks?: {
        onReady?: Function;
        onError?: Function;
        onEnded?: Function;
    };
    controlsImgPath?: {
        playButton?: string;
        pauseButton?: string;
        loadingButton?: string;
    };
    prod?: boolean;
}
export interface InternalListeners {
    onTimeUpdate?: any;
    onWaiting?: any;
}
