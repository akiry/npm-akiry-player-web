export declare class AkiryPlayerConsoleService {
    private _isVisible;
    private _logs;
    private _lastLog;
    private _debug;
    private _playerStatusInfos;
    constructor();
    isVisible: boolean;
    addLogs(logs: string): void;
    setPlayerStatus(currentTime: number, bufferedTime: number, duration: number, currentBitrate?: number, currentBandwidth?: number): void;
    readonly logs: string[];
    readonly playerStatusInfos: string;
}
