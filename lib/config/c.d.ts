declare const C: {
    PLAYERS: {
        SHAKA: {
            name: string;
        };
        VIDEOJS: {
            name: string;
        };
    };
    TYPE: {
        HLS: {
            name: string;
        };
        DASH: {
            name: string;
        };
        MP4: {
            name: string;
        };
    };
    teste: {
        loginType: string;
    };
};
export { C };
