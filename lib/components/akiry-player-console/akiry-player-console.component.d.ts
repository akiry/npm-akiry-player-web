import { OnInit } from '@angular/core';
import { AkiryPlayerConsoleService } from '../../services/akiry-player-console/akiry-player-console.service';
export declare class AkiryPlayerConsoleComponent implements OnInit {
    akiryConsole: AkiryPlayerConsoleService;
    constructor(akiryConsole: AkiryPlayerConsoleService);
    ngOnInit(): void;
}
