import { OnInit, Renderer2 } from '@angular/core';
import { AkiryPlayerConsoleService } from '../../services/akiry-player-console/akiry-player-console.service';
import { AkiryPlayerOptions } from '../../directives/akiry-player/interfaces';
export declare const DEFAULT_TIME_TO_HIDE_CONTROLS = 3000;
export interface ControlsListener {
    showControls?: (show: boolean) => void;
    timeToHide?: number;
}
export declare class AkiryPlayerComponent implements OnInit {
    akiryConsole: AkiryPlayerConsoleService;
    private readonly _renderer;
    private readonly _akiryPlayer;
    private readonly topControls;
    private readonly bottomControls;
    private readonly section;
    private readonly libLoading;
    private readonly divLoading;
    private readonly controlsListener;
    private _consoleCallerCount;
    private _lastClickInit;
    private _timeToHideControls;
    private _isShowing;
    constructor(akiryConsole: AkiryPlayerConsoleService, _renderer: Renderer2);
    ngOnInit(): void;
    play(): void;
    load(url: string, opts: AkiryPlayerOptions): void;
    release(): void;
    private setConsoleCaller;
    private setEvents;
    showControls(): void;
    hideControls(): void;
    private controlTimerWatch;
    clickPlayButton(): boolean;
    clickFullscreenButton(): void;
    private setPlayerInternalCallbacks;
    private startLoading;
}
