/**
 * @fileoverview added by tsickle
 * Generated from: lib/akiry-player.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AkiryPlayerComponent } from './components/akiry-player/akiry-player.component';
import { AkiryPlayerDirective } from './directives/akiry-player/akiry-player.directive';
import { AkiryPlayerConsoleComponent } from './components/akiry-player-console/akiry-player-console.component';
import { AkiryPlayerConsoleService } from './services/akiry-player-console/akiry-player-console.service';
import { LoadingCircleComponent } from './components/loading-circle/loading-circle.component';
var AkiryPlayerModule = /** @class */ (function () {
    function AkiryPlayerModule() {
    }
    AkiryPlayerModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        BrowserModule,
                    ],
                    declarations: [
                        AkiryPlayerComponent,
                        AkiryPlayerConsoleComponent,
                        AkiryPlayerDirective,
                        LoadingCircleComponent,
                    ],
                    providers: [
                        AkiryPlayerConsoleService,
                    ],
                    exports: [
                        AkiryPlayerComponent,
                    ],
                },] }
    ];
    return AkiryPlayerModule;
}());
export { AkiryPlayerModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWtpcnktcGxheWVyLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FraXJ5LXBsYXllci8iLCJzb3VyY2VzIjpbImxpYi9ha2lyeS1wbGF5ZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDMUQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDeEYsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDeEYsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sa0VBQWtFLENBQUM7QUFDL0csT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sOERBQThELENBQUM7QUFDekcsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFHOUY7SUFBQTtJQWlCaUMsQ0FBQzs7Z0JBakJqQyxRQUFRLFNBQUM7b0JBQ1IsT0FBTyxFQUFFO3dCQUNQLGFBQWE7cUJBQ2Q7b0JBQ0QsWUFBWSxFQUFFO3dCQUNaLG9CQUFvQjt3QkFDcEIsMkJBQTJCO3dCQUMzQixvQkFBb0I7d0JBQ3BCLHNCQUFzQjtxQkFDdkI7b0JBQ0QsU0FBUyxFQUFFO3dCQUNULHlCQUF5QjtxQkFDMUI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLG9CQUFvQjtxQkFDckI7aUJBQ0Y7O0lBQ2dDLHdCQUFDO0NBQUEsQUFqQmxDLElBaUJrQztTQUFyQixpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQnJvd3Nlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xuaW1wb3J0IHsgQWtpcnlQbGF5ZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvYWtpcnktcGxheWVyL2FraXJ5LXBsYXllci5jb21wb25lbnQnO1xuaW1wb3J0IHsgQWtpcnlQbGF5ZXJEaXJlY3RpdmUgfSBmcm9tICcuL2RpcmVjdGl2ZXMvYWtpcnktcGxheWVyL2FraXJ5LXBsYXllci5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgQWtpcnlQbGF5ZXJDb25zb2xlQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2FraXJ5LXBsYXllci1jb25zb2xlL2FraXJ5LXBsYXllci1jb25zb2xlLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBBa2lyeVBsYXllckNvbnNvbGVTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9ha2lyeS1wbGF5ZXItY29uc29sZS9ha2lyeS1wbGF5ZXItY29uc29sZS5zZXJ2aWNlJztcbmltcG9ydCB7IExvYWRpbmdDaXJjbGVDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbG9hZGluZy1jaXJjbGUvbG9hZGluZy1jaXJjbGUuY29tcG9uZW50JztcblxuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgQnJvd3Nlck1vZHVsZSxcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgQWtpcnlQbGF5ZXJDb21wb25lbnQsXG4gICAgQWtpcnlQbGF5ZXJDb25zb2xlQ29tcG9uZW50LFxuICAgIEFraXJ5UGxheWVyRGlyZWN0aXZlLFxuICAgIExvYWRpbmdDaXJjbGVDb21wb25lbnQsXG4gIF0sXG4gIHByb3ZpZGVyczogW1xuICAgIEFraXJ5UGxheWVyQ29uc29sZVNlcnZpY2UsXG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBBa2lyeVBsYXllckNvbXBvbmVudCxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgQWtpcnlQbGF5ZXJNb2R1bGUgeyB9XG4iXX0=