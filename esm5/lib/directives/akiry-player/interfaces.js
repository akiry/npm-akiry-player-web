/**
 * @fileoverview added by tsickle
 * Generated from: lib/directives/akiry-player/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function Evaluator() { }
if (false) {
    /** @type {?} */
    Evaluator.prototype.evaluate;
}
/**
 * @record
 */
export function AkiryPlayerOptions() { }
if (false) {
    /** @type {?} */
    AkiryPlayerOptions.prototype.player;
    /** @type {?} */
    AkiryPlayerOptions.prototype.mode;
    /** @type {?|undefined} */
    AkiryPlayerOptions.prototype.evaluator;
    /** @type {?|undefined} */
    AkiryPlayerOptions.prototype.thumb;
    /** @type {?|undefined} */
    AkiryPlayerOptions.prototype.token;
    /** @type {?|undefined} */
    AkiryPlayerOptions.prototype.bigPlayButton;
    /** @type {?|undefined} */
    AkiryPlayerOptions.prototype.autoplay;
    /** @type {?|undefined} */
    AkiryPlayerOptions.prototype.callbacks;
    /** @type {?|undefined} */
    AkiryPlayerOptions.prototype.controlsImgPath;
    /** @type {?|undefined} */
    AkiryPlayerOptions.prototype.prod;
}
/**
 * @record
 */
export function InternalListeners() { }
if (false) {
    /** @type {?|undefined} */
    InternalListeners.prototype.onTimeUpdate;
    /** @type {?|undefined} */
    InternalListeners.prototype.onWaiting;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FraXJ5LXBsYXllci8iLCJzb3VyY2VzIjpbImxpYi9kaXJlY3RpdmVzL2FraXJ5LXBsYXllci9pbnRlcmZhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsK0JBT0M7OztJQU5HLDZCQUtZOzs7OztBQUdoQix3Q0FtQkM7OztJQWxCRyxvQ0FBWTs7SUFDWixrQ0FBVTs7SUFDVix1Q0FBc0I7O0lBQ3RCLG1DQUFlOztJQUNmLG1DQUFlOztJQUNmLDJDQUF3Qjs7SUFDeEIsc0NBQW1COztJQUNuQix1Q0FJRTs7SUFDRiw2Q0FJRTs7SUFDRixrQ0FBZTs7Ozs7QUFHbkIsdUNBR0M7OztJQUZHLHlDQUFtQjs7SUFDbkIsc0NBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBFdmFsdWF0b3Ige1xuICAgIGV2YWx1YXRlOiAoXG4gICAgICAgIGN1cnJlbnRUaW1lOiBudW1iZXIsXG4gICAgICAgIGJ1ZmZlclRpbWU6IG51bWJlcixcbiAgICAgICAgdHJhY2tzLFxuICAgICAgICBiYW5kd2lkdGgsXG4gICAgKSA9PiBudW1iZXI7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgQWtpcnlQbGF5ZXJPcHRpb25zIHtcbiAgICBwbGF5ZXI6IGFueTtcbiAgICBtb2RlOiBhbnk7XG4gICAgZXZhbHVhdG9yPzogRXZhbHVhdG9yO1xuICAgIHRodW1iPzogc3RyaW5nO1xuICAgIHRva2VuPzogc3RyaW5nO1xuICAgIGJpZ1BsYXlCdXR0b24/OiBib29sZWFuO1xuICAgIGF1dG9wbGF5PzogYm9vbGVhbjtcbiAgICBjYWxsYmFja3M/OiB7XG4gICAgICAgIG9uUmVhZHk/OiBGdW5jdGlvbjtcbiAgICAgICAgb25FcnJvcj86IEZ1bmN0aW9uO1xuICAgICAgICBvbkVuZGVkPzogRnVuY3Rpb247XG4gICAgfTtcbiAgICBjb250cm9sc0ltZ1BhdGg/OiB7XG4gICAgICAgIHBsYXlCdXR0b24/OiBzdHJpbmc7XG4gICAgICAgIHBhdXNlQnV0dG9uPzogc3RyaW5nO1xuICAgICAgICBsb2FkaW5nQnV0dG9uPzogc3RyaW5nO1xuICAgIH07XG4gICAgcHJvZD86IGJvb2xlYW47XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSW50ZXJuYWxMaXN0ZW5lcnMge1xuICAgIG9uVGltZVVwZGF0ZT86IGFueTtcbiAgICBvbldhaXRpbmc/OiBhbnk7XG59XG4iXX0=