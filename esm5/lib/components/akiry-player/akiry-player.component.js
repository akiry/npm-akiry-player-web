/**
 * @fileoverview added by tsickle
 * Generated from: lib/components/akiry-player/akiry-player.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ViewChild, Renderer2, Input, ElementRef } from '@angular/core';
import { AkiryPlayerDirective } from '../../directives/akiry-player/akiry-player.directive';
import { AkiryPlayerConsoleService } from '../../services/akiry-player-console/akiry-player-console.service';
import { LoadingCircleComponent } from '../loading-circle/loading-circle.component';
/** @type {?} */
export var DEFAULT_TIME_TO_HIDE_CONTROLS = 3000;
/**
 * @record
 */
export function ControlsListener() { }
if (false) {
    /** @type {?|undefined} */
    ControlsListener.prototype.showControls;
    /** @type {?|undefined} */
    ControlsListener.prototype.timeToHide;
}
var AkiryPlayerComponent = /** @class */ (function () {
    function AkiryPlayerComponent(akiryConsole, _renderer) {
        this.akiryConsole = akiryConsole;
        this._renderer = _renderer;
        this._consoleCallerCount = 0;
        this._timeToHideControls = 0;
        this._isShowing = false;
        this.setConsoleCaller();
    }
    /**
     * @return {?}
     */
    AkiryPlayerComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.setEvents();
        this.setPlayerInternalCallbacks();
    };
    /**
     * @return {?}
     */
    AkiryPlayerComponent.prototype.play = /**
     * @return {?}
     */
    function () {
        this._akiryPlayer.playOrPause();
    };
    /**
     * @param {?} url
     * @param {?} opts
     * @return {?}
     */
    AkiryPlayerComponent.prototype.load = /**
     * @param {?} url
     * @param {?} opts
     * @return {?}
     */
    function (url, opts) {
        this._akiryPlayer.load(url, opts);
    };
    /**
     * @return {?}
     */
    AkiryPlayerComponent.prototype.release = /**
     * @return {?}
     */
    function () {
        this._akiryPlayer.release();
    };
    /**
     * @private
     * @return {?}
     */
    AkiryPlayerComponent.prototype.setConsoleCaller = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        document.addEventListener('touchstart', (/**
         * @param {?} zEvent
         * @return {?}
         */
        function (zEvent) {
            if (_this._consoleCallerCount > 1) {
                _this.akiryConsole.isVisible = !_this.akiryConsole.isVisible;
                _this._consoleCallerCount = 0;
            }
            _this._lastClickInit = new Date();
        }));
        document.addEventListener('touchend', (/**
         * @param {?} zEvent
         * @return {?}
         */
        function (zEvent) {
            if (_this._lastClickInit && Date.now() - _this._lastClickInit.valueOf() > 4000) {
                _this._consoleCallerCount++;
            }
            _this._lastClickInit = undefined;
        }));
        document.addEventListener('keydown', (/**
         * @param {?} zEvent
         * @return {?}
         */
        function (zEvent) {
            // console.log('keydown - SHOW')
            _this.showControls();
            if (zEvent.ctrlKey && zEvent.altKey && zEvent.code === 'KeyD') {
                _this.akiryConsole.isVisible = !_this.akiryConsole.isVisible;
            }
        }));
    };
    /**
     * @private
     * @return {?}
     */
    AkiryPlayerComponent.prototype.setEvents = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var section = this.section.nativeElement;
        section.addEventListener('mouseover', (/**
         * @param {?} zEvent
         * @return {?}
         */
        function (zEvent) {
            // console.log('over - SHOW')
            _this.showControls();
        }));
        section.addEventListener('mousemove', (/**
         * @param {?} zEvent
         * @return {?}
         */
        function (zEvent) {
            // console.log('move - SHOW')
            _this.showControls();
        }));
        // section.addEventListener('mouseleave', (zEvent) => {
        //   // console.log('leave - HIDE')
        //   this.hideControls();
        // });
        section.addEventListener('click', (/**
         * @param {?} zEvent
         * @return {?}
         */
        function (zEvent) {
            // this._akiryPlayer.playOrPause();
            _this.showControls();
        }));
    };
    /**
     * @return {?}
     */
    AkiryPlayerComponent.prototype.showControls = /**
     * @return {?}
     */
    function () {
        this._timeToHideControls =
            Date.now() + (this.controlsListener && this.controlsListener.timeToHide ?
                this.controlsListener.timeToHide : DEFAULT_TIME_TO_HIDE_CONTROLS);
        if (!this._isShowing) {
            this._renderer.setStyle(this.topControls.nativeElement, 'opacity', 1);
            this._renderer.setStyle(this.bottomControls.nativeElement, 'opacity', 1);
            if (this.controlsListener && this.controlsListener.showControls) {
                this.controlsListener.showControls(true);
            }
            this._isShowing = true;
            this.controlTimerWatch();
        }
    };
    /**
     * @return {?}
     */
    AkiryPlayerComponent.prototype.hideControls = /**
     * @return {?}
     */
    function () {
        this._renderer.setStyle(this.topControls.nativeElement, 'opacity', 0);
        this._renderer.setStyle(this.bottomControls.nativeElement, 'opacity', 0);
        if (this.controlsListener && this.controlsListener.showControls) {
            this.controlsListener.showControls(false);
        }
        this._isShowing = false;
    };
    /**
     * @private
     * @return {?}
     */
    AkiryPlayerComponent.prototype.controlTimerWatch = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        if (Date.now() > this._timeToHideControls) {
            this.hideControls();
        }
        else {
            setTimeout((/**
             * @return {?}
             */
            function () {
                _this.controlTimerWatch();
            }), 500);
        }
    };
    /**
     * @return {?}
     */
    AkiryPlayerComponent.prototype.clickPlayButton = /**
     * @return {?}
     */
    function () {
        this._akiryPlayer.playOrPause();
        return false;
    };
    /**
     * @return {?}
     */
    AkiryPlayerComponent.prototype.clickFullscreenButton = /**
     * @return {?}
     */
    function () {
        this._akiryPlayer.fullscreen();
    };
    /**
     * @private
     * @return {?}
     */
    AkiryPlayerComponent.prototype.setPlayerInternalCallbacks = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this._akiryPlayer.setOnTimeUpdatedListener((/**
         * @param {?} currentTime
         * @param {?} bufferedTime
         * @return {?}
         */
        function (currentTime, bufferedTime) {
            /** @type {?} */
            var timeInBuffer = bufferedTime < currentTime ? 0 : bufferedTime - currentTime;
            _this.libLoading.percentage = timeInBuffer / 15 < 1 ? Math.ceil(((5 + timeInBuffer) / 20) * 100) : 100;
            if (_this.libLoading.percentage >= 100 && ((/** @type {?} */ (_this.divLoading.nativeElement))).style.display !== 'none') {
                _this._renderer.setStyle(_this.divLoading.nativeElement, 'display', 'none');
                _this._akiryPlayer.play();
            }
        }));
        this._akiryPlayer.setOnWaitingListener((/**
         * @return {?}
         */
        function () {
            _this.startLoading();
        }));
    };
    /**
     * @private
     * @return {?}
     */
    AkiryPlayerComponent.prototype.startLoading = /**
     * @private
     * @return {?}
     */
    function () {
        this.libLoading.percentage = 0;
        this._renderer.setStyle(this.divLoading.nativeElement, 'display', 'flex');
        this._akiryPlayer.pause();
    };
    AkiryPlayerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'lib-akiry-player',
                    template: "<section #sectionControl>\n  <video libAkiryPlayer #akiryPlayer id='akiryPlayerId' preload playsinline controls>\n    <!-- <p class=\"vjs-no-js\">To view this video, please enable JavaScript, and consider upgrading to a web browser that <a href=\"http://videojs.com/html5-video-support/\">supports HTML5 video</a></p> -->\n    <!-- <source\n      src=\"http://10.42.0.1/videos/live3/live3.m3u8\"\n      type=\"application/x-mpegURL\"> -->\n  </video>\n\n  <div class='akiry-player-warns' #divLoading>\n    <div class='put-center'>\n      <lib-loading-circle #libLoading></lib-loading-circle>\n    </div>\n  </div>\n\n  <div class='akiry-player-controls'>\n    <div class='top' #topControls>\n\n    </div>\n    <div class='put-center' #centerControls>\n      \n    </div>\n    <div class='put-bottom'>\n      <div class='bottom' #bottomControls>\n        <button (click)='clickPlayButton()'>PLAY</button>\n        <button (click)='clickFullscreenButton()'>FULLSCREEN</button>\n      </div>\n      <div class='transparent-body'></div>\n    </div>\n  </div>\n\n  <!-- <lib-akiry-player-console *ngIf=\"akiryConsole.isVisible\"></lib-akiry-player-console> -->\n</section>",
                    styles: ["section{background-color:#000;display:-webkit-box;display:flex;-webkit-box-pack:center;justify-content:center;width:100%;height:100vh}video{width:100%;height:100vh}.video-js{position:relative!important;width:100%!important;height:auto!important}.akiry-player-warns{position:absolute;width:100%;height:100vh;background-color:rgba(255,0,0,0);padding:3% 5% 0;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center}.akiry-player-warns .put-center{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center}.akiry-player-controls{position:absolute;width:100%;height:100vh;background-color:rgba(255,0,0,0);padding:3% 5% 0;display:none;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:justify;justify-content:space-between}.akiry-player-controls .top{background-color:rgba(0,255,0,.5);width:100%;height:100px;-webkit-transition:.2s linear;transition:.2s linear;opacity:0}.akiry-player-controls .put-center{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center}.akiry-player-controls .put-bottom{width:100%;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:reverse;flex-direction:column-reverse}.akiry-player-controls .put-bottom .transparent-body{width:100%;height:100%}.akiry-player-controls .put-bottom .bottom{background-color:rgba(0,0,255,.5);width:100%;height:70px;-webkit-transition:.2s linear;transition:.2s linear;opacity:0}"]
                }] }
    ];
    /** @nocollapse */
    AkiryPlayerComponent.ctorParameters = function () { return [
        { type: AkiryPlayerConsoleService },
        { type: Renderer2 }
    ]; };
    AkiryPlayerComponent.propDecorators = {
        _akiryPlayer: [{ type: ViewChild, args: [AkiryPlayerDirective, { static: true },] }],
        topControls: [{ type: ViewChild, args: ['topControls', { static: true },] }],
        bottomControls: [{ type: ViewChild, args: ['bottomControls', { static: true },] }],
        section: [{ type: ViewChild, args: ['sectionControl', { static: true },] }],
        libLoading: [{ type: ViewChild, args: ['libLoading', { static: true },] }],
        divLoading: [{ type: ViewChild, args: ['divLoading', { static: true },] }],
        controlsListener: [{ type: Input }]
    };
    return AkiryPlayerComponent;
}());
export { AkiryPlayerComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._akiryPlayer;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.topControls;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.bottomControls;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.section;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.libLoading;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.divLoading;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype.controlsListener;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._consoleCallerCount;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._lastClickInit;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._timeToHideControls;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._isShowing;
    /** @type {?} */
    AkiryPlayerComponent.prototype.akiryConsole;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerComponent.prototype._renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWtpcnktcGxheWVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FraXJ5LXBsYXllci8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2FraXJ5LXBsYXllci9ha2lyeS1wbGF5ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxTQUFTLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0YsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDNUYsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sa0VBQWtFLENBQUM7QUFDN0csT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNENBQTRDLENBQUM7O0FBS3BGLE1BQU0sS0FBTyw2QkFBNkIsR0FBRyxJQUFJOzs7O0FBRWpELHNDQUdDOzs7SUFGQyx3Q0FBdUM7O0lBQ3ZDLHNDQUFvQjs7QUFHdEI7SUEyQkUsOEJBQ1MsWUFBdUMsRUFDN0IsU0FBb0I7UUFEOUIsaUJBQVksR0FBWixZQUFZLENBQTJCO1FBQzdCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFSL0Isd0JBQW1CLEdBQUcsQ0FBQyxDQUFDO1FBR3hCLHdCQUFtQixHQUFHLENBQUMsQ0FBQztRQUN4QixlQUFVLEdBQUcsS0FBSyxDQUFDO1FBTXpCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzFCLENBQUM7Ozs7SUFFTSx1Q0FBUTs7O0lBQWY7UUFDRSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDakIsSUFBSSxDQUFDLDBCQUEwQixFQUFFLENBQUM7SUFDcEMsQ0FBQzs7OztJQUVNLG1DQUFJOzs7SUFBWDtRQUNFLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDbEMsQ0FBQzs7Ozs7O0lBRU0sbUNBQUk7Ozs7O0lBQVgsVUFBWSxHQUFXLEVBQUUsSUFBd0I7UUFDL0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3BDLENBQUM7Ozs7SUFFTSxzQ0FBTzs7O0lBQWQ7UUFDRSxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQzlCLENBQUM7Ozs7O0lBRU8sK0NBQWdCOzs7O0lBQXhCO1FBQUEsaUJBdUJDO1FBdEJDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZOzs7O1FBQUUsVUFBQyxNQUFNO1lBQzdDLElBQUksS0FBSSxDQUFDLG1CQUFtQixHQUFHLENBQUMsRUFBRTtnQkFDaEMsS0FBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQztnQkFDM0QsS0FBSSxDQUFDLG1CQUFtQixHQUFHLENBQUMsQ0FBQzthQUM5QjtZQUNELEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUNuQyxDQUFDLEVBQUMsQ0FBQztRQUVILFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVOzs7O1FBQUUsVUFBQyxNQUFNO1lBQzNDLElBQUksS0FBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsR0FBRyxFQUFFLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUUsR0FBRyxJQUFJLEVBQUU7Z0JBQzVFLEtBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO2FBQzVCO1lBQ0QsS0FBSSxDQUFDLGNBQWMsR0FBRyxTQUFTLENBQUM7UUFDbEMsQ0FBQyxFQUFDLENBQUM7UUFFSCxRQUFRLENBQUMsZ0JBQWdCLENBQUMsU0FBUzs7OztRQUFFLFVBQUMsTUFBTTtZQUMxQyxnQ0FBZ0M7WUFDaEMsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ3BCLElBQUksTUFBTSxDQUFDLE9BQU8sSUFBSSxNQUFNLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssTUFBTSxFQUFFO2dCQUM3RCxLQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsR0FBRyxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO2FBQzVEO1FBQ0gsQ0FBQyxFQUFDLENBQUM7SUFDTCxDQUFDOzs7OztJQUVPLHdDQUFTOzs7O0lBQWpCO1FBQUEsaUJBc0JDOztZQXJCTyxPQUFPLEdBQWdCLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYTtRQUV2RCxPQUFPLENBQUMsZ0JBQWdCLENBQUMsV0FBVzs7OztRQUFFLFVBQUMsTUFBTTtZQUMzQyw2QkFBNkI7WUFDN0IsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3RCLENBQUMsRUFBQyxDQUFDO1FBRUgsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFdBQVc7Ozs7UUFBRSxVQUFDLE1BQU07WUFDM0MsNkJBQTZCO1lBQzdCLEtBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUN0QixDQUFDLEVBQUMsQ0FBQztRQUVILHVEQUF1RDtRQUN2RCxtQ0FBbUM7UUFDbkMseUJBQXlCO1FBQ3pCLE1BQU07UUFFTixPQUFPLENBQUMsZ0JBQWdCLENBQUMsT0FBTzs7OztRQUFFLFVBQUMsTUFBTTtZQUN2QyxtQ0FBbUM7WUFDbkMsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3RCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7OztJQUVNLDJDQUFZOzs7SUFBbkI7UUFDRSxJQUFJLENBQUMsbUJBQW1CO1lBQ3RCLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQ3ZFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLDZCQUE2QixDQUFDLENBQUM7UUFFdEUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3RFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUN6RSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFO2dCQUMvRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzFDO1lBQ0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7U0FDMUI7SUFDSCxDQUFDOzs7O0lBRU0sMkNBQVk7OztJQUFuQjtRQUNFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxTQUFTLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDekUsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRTtZQUMvRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzNDO1FBQ0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFFTyxnREFBaUI7Ozs7SUFBekI7UUFBQSxpQkFRQztRQVBDLElBQUksSUFBSSxDQUFDLEdBQUcsRUFBRSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUN6QyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7U0FDckI7YUFBTTtZQUNMLFVBQVU7OztZQUFDO2dCQUNULEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1lBQzNCLENBQUMsR0FBRSxHQUFHLENBQUMsQ0FBQztTQUNUO0lBQ0gsQ0FBQzs7OztJQUVNLDhDQUFlOzs7SUFBdEI7UUFDRSxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRWhDLE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7OztJQUVNLG9EQUFxQjs7O0lBQTVCO1FBQ0UsSUFBSSxDQUFDLFlBQVksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUNqQyxDQUFDOzs7OztJQUVPLHlEQUEwQjs7OztJQUFsQztRQUFBLGlCQWFDO1FBWkMsSUFBSSxDQUFDLFlBQVksQ0FBQyx3QkFBd0I7Ozs7O1FBQUMsVUFBQyxXQUFXLEVBQUUsWUFBWTs7Z0JBQzdELFlBQVksR0FBRyxZQUFZLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksR0FBRyxXQUFXO1lBQ2hGLEtBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxHQUFHLFlBQVksR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsWUFBWSxDQUFDLEdBQUcsRUFBRSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztZQUN0RyxJQUFJLEtBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxJQUFJLEdBQUcsSUFBSSxDQUFDLG1CQUFBLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFlLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxLQUFLLE1BQU0sRUFBRTtnQkFDaEgsS0FBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsU0FBUyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUMxRSxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO2FBQzFCO1FBQ0gsQ0FBQyxFQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsWUFBWSxDQUFDLG9CQUFvQjs7O1FBQUM7WUFDckMsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3RCLENBQUMsRUFBQyxDQUFDO0lBQ0wsQ0FBQzs7Ozs7SUFFTywyQ0FBWTs7OztJQUFwQjtRQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQztRQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUM1QixDQUFDOztnQkFwS0YsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLDJwQ0FBNEM7O2lCQUk3Qzs7OztnQkFuQlEseUJBQXlCO2dCQUZLLFNBQVM7OzsrQkF1QjdDLFNBQVMsU0FBQyxvQkFBb0IsRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7OEJBRWhELFNBQVMsU0FBQyxhQUFhLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFO2lDQUN6QyxTQUFTLFNBQUMsZ0JBQWdCLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFOzBCQUU1QyxTQUFTLFNBQUMsZ0JBQWdCLEVBQUUsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFOzZCQUU1QyxTQUFTLFNBQUMsWUFBWSxFQUFFLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRTs2QkFDeEMsU0FBUyxTQUFDLFlBQVksRUFBRSxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7bUNBR3hDLEtBQUs7O0lBa0pSLDJCQUFDO0NBQUEsQUFyS0QsSUFxS0M7U0E5Slksb0JBQW9COzs7Ozs7SUFDL0IsNENBQXVHOzs7OztJQUV2RywyQ0FBeUU7Ozs7O0lBQ3pFLDhDQUErRTs7Ozs7SUFFL0UsdUNBQXdFOzs7OztJQUV4RSwwQ0FBK0Y7Ozs7O0lBQy9GLDBDQUFtRjs7Ozs7SUFHbkYsZ0RBQTZEOzs7OztJQUU3RCxtREFBZ0M7Ozs7O0lBQ2hDLDhDQUE2Qjs7Ozs7SUFFN0IsbURBQWdDOzs7OztJQUNoQywwQ0FBMkI7O0lBR3pCLDRDQUE4Qzs7Ozs7SUFDOUMseUNBQXFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdDaGlsZCwgUmVuZGVyZXIyLCBJbnB1dCwgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQWtpcnlQbGF5ZXJEaXJlY3RpdmUgfSBmcm9tICcuLi8uLi9kaXJlY3RpdmVzL2FraXJ5LXBsYXllci9ha2lyeS1wbGF5ZXIuZGlyZWN0aXZlJztcbmltcG9ydCB7IEFraXJ5UGxheWVyQ29uc29sZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9ha2lyeS1wbGF5ZXItY29uc29sZS9ha2lyeS1wbGF5ZXItY29uc29sZS5zZXJ2aWNlJztcbmltcG9ydCB7IExvYWRpbmdDaXJjbGVDb21wb25lbnQgfSBmcm9tICcuLi9sb2FkaW5nLWNpcmNsZS9sb2FkaW5nLWNpcmNsZS5jb21wb25lbnQnO1xuaW1wb3J0IHsgQWtpcnlQbGF5ZXJPcHRpb25zIH0gZnJvbSAnLi4vLi4vZGlyZWN0aXZlcy9ha2lyeS1wbGF5ZXIvaW50ZXJmYWNlcyc7XG5cbmRlY2xhcmUgY29uc3QgZG9jdW1lbnQ6IGFueTtcblxuZXhwb3J0IGNvbnN0IERFRkFVTFRfVElNRV9UT19ISURFX0NPTlRST0xTID0gMzAwMDtcblxuZXhwb3J0IGludGVyZmFjZSBDb250cm9sc0xpc3RlbmVyIHtcbiAgc2hvd0NvbnRyb2xzPzogKHNob3c6IGJvb2xlYW4pID0+IHZvaWQ7XG4gIHRpbWVUb0hpZGU/OiBudW1iZXI7XG59XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2xpYi1ha2lyeS1wbGF5ZXInLFxuICB0ZW1wbGF0ZVVybDogJy4vYWtpcnktcGxheWVyLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbXG4gICAgJy4vYWtpcnktcGxheWVyLmNvbXBvbmVudC5zYXNzJyxcbiAgXSxcbn0pXG5leHBvcnQgY2xhc3MgQWtpcnlQbGF5ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBAVmlld0NoaWxkKEFraXJ5UGxheWVyRGlyZWN0aXZlLCB7IHN0YXRpYzogdHJ1ZSB9KSBwcml2YXRlIHJlYWRvbmx5IF9ha2lyeVBsYXllcjogQWtpcnlQbGF5ZXJEaXJlY3RpdmU7XG5cbiAgQFZpZXdDaGlsZCgndG9wQ29udHJvbHMnLCB7IHN0YXRpYzogdHJ1ZSB9KSBwcml2YXRlIHJlYWRvbmx5IHRvcENvbnRyb2xzO1xuICBAVmlld0NoaWxkKCdib3R0b21Db250cm9scycsIHsgc3RhdGljOiB0cnVlIH0pIHByaXZhdGUgcmVhZG9ubHkgYm90dG9tQ29udHJvbHM7XG5cbiAgQFZpZXdDaGlsZCgnc2VjdGlvbkNvbnRyb2wnLCB7IHN0YXRpYzogdHJ1ZSB9KSBwcml2YXRlIHJlYWRvbmx5IHNlY3Rpb247XG5cbiAgQFZpZXdDaGlsZCgnbGliTG9hZGluZycsIHsgc3RhdGljOiB0cnVlIH0pIHByaXZhdGUgcmVhZG9ubHkgbGliTG9hZGluZzogTG9hZGluZ0NpcmNsZUNvbXBvbmVudDtcbiAgQFZpZXdDaGlsZCgnZGl2TG9hZGluZycsIHsgc3RhdGljOiB0cnVlIH0pIHByaXZhdGUgcmVhZG9ubHkgZGl2TG9hZGluZzogRWxlbWVudFJlZjtcblxuXG4gIEBJbnB1dCgpIHByaXZhdGUgcmVhZG9ubHkgY29udHJvbHNMaXN0ZW5lcjogQ29udHJvbHNMaXN0ZW5lcjtcblxuICBwcml2YXRlIF9jb25zb2xlQ2FsbGVyQ291bnQgPSAwO1xuICBwcml2YXRlIF9sYXN0Q2xpY2tJbml0OiBEYXRlO1xuXG4gIHByaXZhdGUgX3RpbWVUb0hpZGVDb250cm9scyA9IDA7XG4gIHByaXZhdGUgX2lzU2hvd2luZyA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKFxuICAgIHB1YmxpYyBha2lyeUNvbnNvbGU6IEFraXJ5UGxheWVyQ29uc29sZVNlcnZpY2UsXG4gICAgcHJpdmF0ZSByZWFkb25seSBfcmVuZGVyZXI6IFJlbmRlcmVyMixcbiAgKSB7XG4gICAgdGhpcy5zZXRDb25zb2xlQ2FsbGVyKCk7XG4gIH1cblxuICBwdWJsaWMgbmdPbkluaXQoKSB7XG4gICAgdGhpcy5zZXRFdmVudHMoKTtcbiAgICB0aGlzLnNldFBsYXllckludGVybmFsQ2FsbGJhY2tzKCk7XG4gIH1cblxuICBwdWJsaWMgcGxheSgpIHtcbiAgICB0aGlzLl9ha2lyeVBsYXllci5wbGF5T3JQYXVzZSgpO1xuICB9XG5cbiAgcHVibGljIGxvYWQodXJsOiBzdHJpbmcsIG9wdHM6IEFraXJ5UGxheWVyT3B0aW9ucykge1xuICAgIHRoaXMuX2FraXJ5UGxheWVyLmxvYWQodXJsLCBvcHRzKTtcbiAgfVxuXG4gIHB1YmxpYyByZWxlYXNlKCkge1xuICAgIHRoaXMuX2FraXJ5UGxheWVyLnJlbGVhc2UoKTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0Q29uc29sZUNhbGxlcigpIHtcbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCd0b3VjaHN0YXJ0JywgKHpFdmVudCkgPT4ge1xuICAgICAgaWYgKHRoaXMuX2NvbnNvbGVDYWxsZXJDb3VudCA+IDEpIHtcbiAgICAgICAgdGhpcy5ha2lyeUNvbnNvbGUuaXNWaXNpYmxlID0gIXRoaXMuYWtpcnlDb25zb2xlLmlzVmlzaWJsZTtcbiAgICAgICAgdGhpcy5fY29uc29sZUNhbGxlckNvdW50ID0gMDtcbiAgICAgIH1cbiAgICAgIHRoaXMuX2xhc3RDbGlja0luaXQgPSBuZXcgRGF0ZSgpO1xuICAgIH0pO1xuXG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcigndG91Y2hlbmQnLCAoekV2ZW50KSA9PiB7XG4gICAgICBpZiAodGhpcy5fbGFzdENsaWNrSW5pdCAmJiBEYXRlLm5vdygpIC0gdGhpcy5fbGFzdENsaWNrSW5pdC52YWx1ZU9mKCkgPiA0MDAwKSB7XG4gICAgICAgIHRoaXMuX2NvbnNvbGVDYWxsZXJDb3VudCsrO1xuICAgICAgfVxuICAgICAgdGhpcy5fbGFzdENsaWNrSW5pdCA9IHVuZGVmaW5lZDtcbiAgICB9KTtcblxuICAgIGRvY3VtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2tleWRvd24nLCAoekV2ZW50KSA9PiB7XG4gICAgICAvLyBjb25zb2xlLmxvZygna2V5ZG93biAtIFNIT1cnKVxuICAgICAgdGhpcy5zaG93Q29udHJvbHMoKTtcbiAgICAgIGlmICh6RXZlbnQuY3RybEtleSAmJiB6RXZlbnQuYWx0S2V5ICYmIHpFdmVudC5jb2RlID09PSAnS2V5RCcpIHtcbiAgICAgICAgdGhpcy5ha2lyeUNvbnNvbGUuaXNWaXNpYmxlID0gIXRoaXMuYWtpcnlDb25zb2xlLmlzVmlzaWJsZTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHByaXZhdGUgc2V0RXZlbnRzKCkge1xuICAgIGNvbnN0IHNlY3Rpb246IEhUTUxFbGVtZW50ID0gdGhpcy5zZWN0aW9uLm5hdGl2ZUVsZW1lbnQ7XG5cbiAgICBzZWN0aW9uLmFkZEV2ZW50TGlzdGVuZXIoJ21vdXNlb3ZlcicsICh6RXZlbnQpID0+IHtcbiAgICAgIC8vIGNvbnNvbGUubG9nKCdvdmVyIC0gU0hPVycpXG4gICAgICB0aGlzLnNob3dDb250cm9scygpO1xuICAgIH0pO1xuXG4gICAgc2VjdGlvbi5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLCAoekV2ZW50KSA9PiB7XG4gICAgICAvLyBjb25zb2xlLmxvZygnbW92ZSAtIFNIT1cnKVxuICAgICAgdGhpcy5zaG93Q29udHJvbHMoKTtcbiAgICB9KTtcblxuICAgIC8vIHNlY3Rpb24uYWRkRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsICh6RXZlbnQpID0+IHtcbiAgICAvLyAgIC8vIGNvbnNvbGUubG9nKCdsZWF2ZSAtIEhJREUnKVxuICAgIC8vICAgdGhpcy5oaWRlQ29udHJvbHMoKTtcbiAgICAvLyB9KTtcblxuICAgIHNlY3Rpb24uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoekV2ZW50KSA9PiB7XG4gICAgICAvLyB0aGlzLl9ha2lyeVBsYXllci5wbGF5T3JQYXVzZSgpO1xuICAgICAgdGhpcy5zaG93Q29udHJvbHMoKTtcbiAgICB9KTtcbiAgfVxuXG4gIHB1YmxpYyBzaG93Q29udHJvbHMoKSB7XG4gICAgdGhpcy5fdGltZVRvSGlkZUNvbnRyb2xzID1cbiAgICAgIERhdGUubm93KCkgKyAodGhpcy5jb250cm9sc0xpc3RlbmVyICYmIHRoaXMuY29udHJvbHNMaXN0ZW5lci50aW1lVG9IaWRlID9cbiAgICAgICAgdGhpcy5jb250cm9sc0xpc3RlbmVyLnRpbWVUb0hpZGUgOiBERUZBVUxUX1RJTUVfVE9fSElERV9DT05UUk9MUyk7XG5cbiAgICBpZiAoIXRoaXMuX2lzU2hvd2luZykge1xuICAgICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUodGhpcy50b3BDb250cm9scy5uYXRpdmVFbGVtZW50LCAnb3BhY2l0eScsIDEpO1xuICAgICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUodGhpcy5ib3R0b21Db250cm9scy5uYXRpdmVFbGVtZW50LCAnb3BhY2l0eScsIDEpO1xuICAgICAgaWYgKHRoaXMuY29udHJvbHNMaXN0ZW5lciAmJiB0aGlzLmNvbnRyb2xzTGlzdGVuZXIuc2hvd0NvbnRyb2xzKSB7XG4gICAgICAgIHRoaXMuY29udHJvbHNMaXN0ZW5lci5zaG93Q29udHJvbHModHJ1ZSk7XG4gICAgICB9XG4gICAgICB0aGlzLl9pc1Nob3dpbmcgPSB0cnVlO1xuICAgICAgdGhpcy5jb250cm9sVGltZXJXYXRjaCgpO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBoaWRlQ29udHJvbHMoKSB7XG4gICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUodGhpcy50b3BDb250cm9scy5uYXRpdmVFbGVtZW50LCAnb3BhY2l0eScsIDApO1xuICAgIHRoaXMuX3JlbmRlcmVyLnNldFN0eWxlKHRoaXMuYm90dG9tQ29udHJvbHMubmF0aXZlRWxlbWVudCwgJ29wYWNpdHknLCAwKTtcbiAgICBpZiAodGhpcy5jb250cm9sc0xpc3RlbmVyICYmIHRoaXMuY29udHJvbHNMaXN0ZW5lci5zaG93Q29udHJvbHMpIHtcbiAgICAgIHRoaXMuY29udHJvbHNMaXN0ZW5lci5zaG93Q29udHJvbHMoZmFsc2UpO1xuICAgIH1cbiAgICB0aGlzLl9pc1Nob3dpbmcgPSBmYWxzZTtcbiAgfVxuXG4gIHByaXZhdGUgY29udHJvbFRpbWVyV2F0Y2goKSB7XG4gICAgaWYgKERhdGUubm93KCkgPiB0aGlzLl90aW1lVG9IaWRlQ29udHJvbHMpIHtcbiAgICAgIHRoaXMuaGlkZUNvbnRyb2xzKCk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICB0aGlzLmNvbnRyb2xUaW1lcldhdGNoKCk7XG4gICAgICB9LCA1MDApO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBjbGlja1BsYXlCdXR0b24oKSB7XG4gICAgdGhpcy5fYWtpcnlQbGF5ZXIucGxheU9yUGF1c2UoKTtcblxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIHB1YmxpYyBjbGlja0Z1bGxzY3JlZW5CdXR0b24oKSB7XG4gICAgdGhpcy5fYWtpcnlQbGF5ZXIuZnVsbHNjcmVlbigpO1xuICB9XG5cbiAgcHJpdmF0ZSBzZXRQbGF5ZXJJbnRlcm5hbENhbGxiYWNrcygpIHtcbiAgICB0aGlzLl9ha2lyeVBsYXllci5zZXRPblRpbWVVcGRhdGVkTGlzdGVuZXIoKGN1cnJlbnRUaW1lLCBidWZmZXJlZFRpbWUpID0+IHtcbiAgICAgIGNvbnN0IHRpbWVJbkJ1ZmZlciA9IGJ1ZmZlcmVkVGltZSA8IGN1cnJlbnRUaW1lID8gMCA6IGJ1ZmZlcmVkVGltZSAtIGN1cnJlbnRUaW1lO1xuICAgICAgdGhpcy5saWJMb2FkaW5nLnBlcmNlbnRhZ2UgPSB0aW1lSW5CdWZmZXIgLyAxNSA8IDEgPyBNYXRoLmNlaWwoKCg1ICsgdGltZUluQnVmZmVyKSAvIDIwKSAqIDEwMCkgOiAxMDA7XG4gICAgICBpZiAodGhpcy5saWJMb2FkaW5nLnBlcmNlbnRhZ2UgPj0gMTAwICYmICh0aGlzLmRpdkxvYWRpbmcubmF0aXZlRWxlbWVudCBhcyBIVE1MRWxlbWVudCkuc3R5bGUuZGlzcGxheSAhPT0gJ25vbmUnKSB7XG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLnNldFN0eWxlKHRoaXMuZGl2TG9hZGluZy5uYXRpdmVFbGVtZW50LCAnZGlzcGxheScsICdub25lJyk7XG4gICAgICAgIHRoaXMuX2FraXJ5UGxheWVyLnBsYXkoKTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHRoaXMuX2FraXJ5UGxheWVyLnNldE9uV2FpdGluZ0xpc3RlbmVyKCgpID0+IHtcbiAgICAgIHRoaXMuc3RhcnRMb2FkaW5nKCk7XG4gICAgfSk7XG4gIH1cblxuICBwcml2YXRlIHN0YXJ0TG9hZGluZygpIHtcbiAgICB0aGlzLmxpYkxvYWRpbmcucGVyY2VudGFnZSA9IDA7XG4gICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUodGhpcy5kaXZMb2FkaW5nLm5hdGl2ZUVsZW1lbnQsICdkaXNwbGF5JywgJ2ZsZXgnKTtcbiAgICB0aGlzLl9ha2lyeVBsYXllci5wYXVzZSgpO1xuICB9XG59XG4iXX0=