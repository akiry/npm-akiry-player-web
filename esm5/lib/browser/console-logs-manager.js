/**
 * @fileoverview added by tsickle
 * Generated from: lib/browser/console-logs-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var printConsoleLogs = (/**
 * @param {?=} logElementId
 * @param {?=} breakline
 * @return {?}
 */
function (logElementId, breakline) {
    // Reference to an output container, use 'pre' styling for JSON output
    /** @type {?} */
    var output = logElementId ? document.getElementById(logElementId) : document.createElement('pre');
    /** @type {?} */
    var breakline = breakline ? breakline : '<br/>';
    if (!logElementId)
        document.body.appendChild(output);
    // Reference to native method(s)
    /** @type {?} */
    var oldLog = console.log;
    console.log = (/**
     * @param {...?} items
     * @return {?}
     */
    function () {
        var items = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            items[_i] = arguments[_i];
        }
        // Call native method first
        oldLog.apply(this, items);
        // Use JSON to transform objects, all others display normally
        items.forEach((/**
         * @param {?} item
         * @param {?} i
         * @return {?}
         */
        function (item, i) {
            items[i] = (typeof item === 'object' ? JSON.stringify(item, null, 4) : item);
        }));
        output.innerHTML += items.join(' ') + breakline + '-> ';
    });
    // You could even allow Javascript input...
    /**
     * @param {?} data
     * @return {?}
     */
    function consoleInput(data) {
        // Print it to console as typed
        console.log(data + breakline);
        try {
            console.log(eval(data));
        }
        catch (e) {
            console.log(e.stack);
        }
    }
});
var ɵ0 = printConsoleLogs;
export { printConsoleLogs };
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uc29sZS1sb2dzLW1hbmFnZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ha2lyeS1wbGF5ZXIvIiwic291cmNlcyI6WyJsaWIvYnJvd3Nlci9jb25zb2xlLWxvZ3MtbWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7SUFBTSxnQkFBZ0I7Ozs7O0FBQUcsVUFBQyxZQUFxQixFQUFFLFNBQWtCOzs7UUFFM0QsTUFBTSxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7O1FBQzdGLFNBQVMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsT0FBTztJQUUvQyxJQUFHLENBQUMsWUFBWTtRQUNoQixRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQzs7O1FBRzlCLE1BQU0sR0FBRyxPQUFPLENBQUMsR0FBRztJQUV4QixPQUFPLENBQUMsR0FBRzs7OztJQUFHO1FBQVUsZUFBUTthQUFSLFVBQVEsRUFBUixxQkFBUSxFQUFSLElBQVE7WUFBUiwwQkFBUTs7UUFFNUIsMkJBQTJCO1FBQzNCLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXpCLDZEQUE2RDtRQUM3RCxLQUFLLENBQUMsT0FBTzs7Ozs7UUFBRSxVQUFDLElBQUksRUFBQyxDQUFDO1lBQ2xCLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sSUFBSSxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvRSxDQUFDLEVBQUMsQ0FBQztRQUVILE1BQU0sQ0FBQyxTQUFTLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxTQUFTLEdBQUcsS0FBSyxDQUFDO0lBRTVELENBQUMsQ0FBQSxDQUFDOzs7Ozs7SUFHRixTQUFTLFlBQVksQ0FBRSxJQUFJO1FBQ3ZCLCtCQUErQjtRQUMvQixPQUFPLENBQUMsR0FBRyxDQUFFLElBQUksR0FBRyxTQUFTLENBQUUsQ0FBQztRQUNoQyxJQUFJO1lBQ0EsT0FBTyxDQUFDLEdBQUcsQ0FBRSxJQUFJLENBQUUsSUFBSSxDQUFFLENBQUUsQ0FBQztTQUMvQjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsT0FBTyxDQUFDLEdBQUcsQ0FBRSxDQUFDLENBQUMsS0FBSyxDQUFFLENBQUM7U0FDMUI7SUFDTCxDQUFDO0FBQ0wsQ0FBQyxDQUFBOztBQUVELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgcHJpbnRDb25zb2xlTG9ncyA9IChsb2dFbGVtZW50SWQ/OiBzdHJpbmcsIGJyZWFrbGluZT86IHN0cmluZykgPT4ge1xuICAgIC8vIFJlZmVyZW5jZSB0byBhbiBvdXRwdXQgY29udGFpbmVyLCB1c2UgJ3ByZScgc3R5bGluZyBmb3IgSlNPTiBvdXRwdXRcbiAgICB2YXIgb3V0cHV0ID0gbG9nRWxlbWVudElkID8gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQobG9nRWxlbWVudElkKSA6IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3ByZScpO1xuICAgIHZhciBicmVha2xpbmUgPSBicmVha2xpbmUgPyBicmVha2xpbmUgOiAnPGJyLz4nO1xuXG4gICAgaWYoIWxvZ0VsZW1lbnRJZClcbiAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKG91dHB1dCk7XG5cbiAgICAvLyBSZWZlcmVuY2UgdG8gbmF0aXZlIG1ldGhvZChzKVxuICAgIHZhciBvbGRMb2cgPSBjb25zb2xlLmxvZztcblxuICAgIGNvbnNvbGUubG9nID0gZnVuY3Rpb24oIC4uLml0ZW1zICkge1xuXG4gICAgICAgIC8vIENhbGwgbmF0aXZlIG1ldGhvZCBmaXJzdFxuICAgICAgICBvbGRMb2cuYXBwbHkodGhpcyxpdGVtcyk7XG5cbiAgICAgICAgLy8gVXNlIEpTT04gdG8gdHJhbnNmb3JtIG9iamVjdHMsIGFsbCBvdGhlcnMgZGlzcGxheSBub3JtYWxseVxuICAgICAgICBpdGVtcy5mb3JFYWNoKCAoaXRlbSxpKT0+e1xuICAgICAgICAgICAgaXRlbXNbaV0gPSAodHlwZW9mIGl0ZW0gPT09ICdvYmplY3QnID8gSlNPTi5zdHJpbmdpZnkoaXRlbSxudWxsLDQpIDogaXRlbSk7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIG91dHB1dC5pbm5lckhUTUwgKz0gaXRlbXMuam9pbignICcpICsgYnJlYWtsaW5lICsgJy0+ICc7XG5cbiAgICB9O1xuXG4gICAgLy8gWW91IGNvdWxkIGV2ZW4gYWxsb3cgSmF2YXNjcmlwdCBpbnB1dC4uLlxuICAgIGZ1bmN0aW9uIGNvbnNvbGVJbnB1dCggZGF0YSApIHtcbiAgICAgICAgLy8gUHJpbnQgaXQgdG8gY29uc29sZSBhcyB0eXBlZFxuICAgICAgICBjb25zb2xlLmxvZyggZGF0YSArIGJyZWFrbGluZSApO1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc29sZS5sb2coIGV2YWwoIGRhdGEgKSApO1xuICAgICAgICB9IGNhdGNoIChlKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyggZS5zdGFjayApO1xuICAgICAgICB9XG4gICAgfVxufTtcblxuZXhwb3J0IHsgcHJpbnRDb25zb2xlTG9ncyB9OyJdfQ==