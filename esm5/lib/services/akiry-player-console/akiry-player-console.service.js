/**
 * @fileoverview added by tsickle
 * Generated from: lib/services/akiry-player-console/akiry-player-console.service.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var AkiryPlayerConsoleService = /** @class */ (function () {
    function AkiryPlayerConsoleService() {
        this._isVisible = false;
        this._logs = [];
        this._debug = true;
    }
    Object.defineProperty(AkiryPlayerConsoleService.prototype, "isVisible", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isVisible;
        },
        set: /**
         * @param {?} isVisible
         * @return {?}
         */
        function (isVisible) {
            this._isVisible = isVisible;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} logs
     * @return {?}
     */
    AkiryPlayerConsoleService.prototype.addLogs = /**
     * @param {?} logs
     * @return {?}
     */
    function (logs) {
        /** @type {?} */
        var now = new Date();
        /** @type {?} */
        var nowString = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds() + "." + now.getMilliseconds();
        this._logs.push(nowString + ' | ' + logs);
        if (this._debug) {
            this.isVisible = true;
            this._lastLog = new Date();
            // setTimeout(() => { if (Date.now() - this._lastLog.valueOf() > 4000) { this.isVisible = false; } }, 4500);
        }
    };
    /**
     * @param {?} currentTime
     * @param {?} bufferedTime
     * @param {?} duration
     * @param {?=} currentBitrate
     * @param {?=} currentBandwidth
     * @return {?}
     */
    AkiryPlayerConsoleService.prototype.setPlayerStatus = /**
     * @param {?} currentTime
     * @param {?} bufferedTime
     * @param {?} duration
     * @param {?=} currentBitrate
     * @param {?=} currentBandwidth
     * @return {?}
     */
    function (currentTime, bufferedTime, duration, currentBitrate, currentBandwidth) {
        this._playerStatusInfos =
            'Current time: ' + currentTime + '/' + (duration || '?') +
                '\nBuffered time: ' + bufferedTime + '/' + (duration || '?') +
                '\ncurrentBitrate: ' + (currentBitrate || '?') +
                '\ncurrentBandwidth: ' + (currentBandwidth || '?');
    };
    Object.defineProperty(AkiryPlayerConsoleService.prototype, "logs", {
        get: /**
         * @return {?}
         */
        function () {
            return this._logs;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AkiryPlayerConsoleService.prototype, "playerStatusInfos", {
        get: /**
         * @return {?}
         */
        function () {
            return this._playerStatusInfos;
        },
        enumerable: true,
        configurable: true
    });
    AkiryPlayerConsoleService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root',
                },] }
    ];
    /** @nocollapse */
    AkiryPlayerConsoleService.ctorParameters = function () { return []; };
    /** @nocollapse */ AkiryPlayerConsoleService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AkiryPlayerConsoleService_Factory() { return new AkiryPlayerConsoleService(); }, token: AkiryPlayerConsoleService, providedIn: "root" });
    return AkiryPlayerConsoleService;
}());
export { AkiryPlayerConsoleService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerConsoleService.prototype._isVisible;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerConsoleService.prototype._logs;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerConsoleService.prototype._lastLog;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerConsoleService.prototype._debug;
    /**
     * @type {?}
     * @private
     */
    AkiryPlayerConsoleService.prototype._playerStatusInfos;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWtpcnktcGxheWVyLWNvbnNvbGUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FraXJ5LXBsYXllci8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9ha2lyeS1wbGF5ZXItY29uc29sZS9ha2lyeS1wbGF5ZXItY29uc29sZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFFM0M7SUFXRTtRQVBRLGVBQVUsR0FBRyxLQUFLLENBQUM7UUFDbkIsVUFBSyxHQUFhLEVBQUUsQ0FBQztRQUVyQixXQUFNLEdBQUcsSUFBSSxDQUFDO0lBSU4sQ0FBQztJQUVqQixzQkFBSSxnREFBUzs7OztRQUFiO1lBQ0UsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3pCLENBQUM7Ozs7O1FBRUQsVUFBYyxTQUFrQjtZQUM5QixJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztRQUM5QixDQUFDOzs7T0FKQTs7Ozs7SUFNTSwyQ0FBTzs7OztJQUFkLFVBQWUsSUFBWTs7WUFDbkIsR0FBRyxHQUFHLElBQUksSUFBSSxFQUFFOztZQUNoQixTQUFTLEdBQU0sR0FBRyxDQUFDLFFBQVEsRUFBRSxTQUFJLEdBQUcsQ0FBQyxVQUFVLEVBQUUsU0FBSSxHQUFHLENBQUMsVUFBVSxFQUFFLFNBQUksR0FBRyxDQUFDLGVBQWUsRUFBSTtRQUN0RyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxHQUFHLElBQUksQ0FBQyxDQUFDO1FBRTFDLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNmLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztZQUUzQiw0R0FBNEc7U0FDN0c7SUFDSCxDQUFDOzs7Ozs7Ozs7SUFFTSxtREFBZTs7Ozs7Ozs7SUFBdEIsVUFBdUIsV0FBbUIsRUFBRSxZQUFvQixFQUFFLFFBQWdCLEVBQUUsY0FBdUIsRUFBRSxnQkFBeUI7UUFDcEksSUFBSSxDQUFDLGtCQUFrQjtZQUNyQixnQkFBZ0IsR0FBRyxXQUFXLEdBQUcsR0FBRyxHQUFHLENBQUMsUUFBUSxJQUFJLEdBQUcsQ0FBQztnQkFDeEQsbUJBQW1CLEdBQUcsWUFBWSxHQUFHLEdBQUcsR0FBRyxDQUFDLFFBQVEsSUFBSSxHQUFHLENBQUM7Z0JBQzVELG9CQUFvQixHQUFHLENBQUMsY0FBYyxJQUFJLEdBQUcsQ0FBQztnQkFDOUMsc0JBQXNCLEdBQUcsQ0FBQyxnQkFBZ0IsSUFBSSxHQUFHLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUQsc0JBQUksMkNBQUk7Ozs7UUFBUjtZQUNFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNwQixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHdEQUFpQjs7OztRQUFyQjtZQUNFLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO1FBQ2pDLENBQUM7OztPQUFBOztnQkFoREYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7Ozs7b0NBSkQ7Q0FtREMsQUFqREQsSUFpREM7U0E5Q1kseUJBQXlCOzs7Ozs7SUFDcEMsK0NBQTJCOzs7OztJQUMzQiwwQ0FBNkI7Ozs7O0lBQzdCLDZDQUF1Qjs7Ozs7SUFDdkIsMkNBQXNCOzs7OztJQUV0Qix1REFBbUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBJbmplY3RhYmxlKHtcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnLFxufSlcbmV4cG9ydCBjbGFzcyBBa2lyeVBsYXllckNvbnNvbGVTZXJ2aWNlIHtcbiAgcHJpdmF0ZSBfaXNWaXNpYmxlID0gZmFsc2U7XG4gIHByaXZhdGUgX2xvZ3M6IHN0cmluZ1tdID0gW107XG4gIHByaXZhdGUgX2xhc3RMb2c6IERhdGU7XG4gIHByaXZhdGUgX2RlYnVnID0gdHJ1ZTtcblxuICBwcml2YXRlIF9wbGF5ZXJTdGF0dXNJbmZvczogc3RyaW5nO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgZ2V0IGlzVmlzaWJsZSgpOiBib29sZWFuIHtcbiAgICByZXR1cm4gdGhpcy5faXNWaXNpYmxlO1xuICB9XG5cbiAgc2V0IGlzVmlzaWJsZShpc1Zpc2libGU6IGJvb2xlYW4pIHtcbiAgICB0aGlzLl9pc1Zpc2libGUgPSBpc1Zpc2libGU7XG4gIH1cblxuICBwdWJsaWMgYWRkTG9ncyhsb2dzOiBzdHJpbmcpIHtcbiAgICBjb25zdCBub3cgPSBuZXcgRGF0ZSgpO1xuICAgIGNvbnN0IG5vd1N0cmluZyA9IGAke25vdy5nZXRIb3VycygpfToke25vdy5nZXRNaW51dGVzKCl9OiR7bm93LmdldFNlY29uZHMoKX0uJHtub3cuZ2V0TWlsbGlzZWNvbmRzKCl9YDtcbiAgICB0aGlzLl9sb2dzLnB1c2gobm93U3RyaW5nICsgJyB8ICcgKyBsb2dzKTtcblxuICAgIGlmICh0aGlzLl9kZWJ1Zykge1xuICAgICAgdGhpcy5pc1Zpc2libGUgPSB0cnVlO1xuICAgICAgdGhpcy5fbGFzdExvZyA9IG5ldyBEYXRlKCk7XG5cbiAgICAgIC8vIHNldFRpbWVvdXQoKCkgPT4geyBpZiAoRGF0ZS5ub3coKSAtIHRoaXMuX2xhc3RMb2cudmFsdWVPZigpID4gNDAwMCkgeyB0aGlzLmlzVmlzaWJsZSA9IGZhbHNlOyB9IH0sIDQ1MDApO1xuICAgIH1cbiAgfVxuXG4gIHB1YmxpYyBzZXRQbGF5ZXJTdGF0dXMoY3VycmVudFRpbWU6IG51bWJlciwgYnVmZmVyZWRUaW1lOiBudW1iZXIsIGR1cmF0aW9uOiBudW1iZXIsIGN1cnJlbnRCaXRyYXRlPzogbnVtYmVyLCBjdXJyZW50QmFuZHdpZHRoPzogbnVtYmVyKSB7XG4gICAgdGhpcy5fcGxheWVyU3RhdHVzSW5mb3MgPVxuICAgICAgJ0N1cnJlbnQgdGltZTogJyArIGN1cnJlbnRUaW1lICsgJy8nICsgKGR1cmF0aW9uIHx8ICc/JykgK1xuICAgICAgJ1xcbkJ1ZmZlcmVkIHRpbWU6ICcgKyBidWZmZXJlZFRpbWUgKyAnLycgKyAoZHVyYXRpb24gfHwgJz8nKSArXG4gICAgICAnXFxuY3VycmVudEJpdHJhdGU6ICcgKyAoY3VycmVudEJpdHJhdGUgfHwgJz8nKSArXG4gICAgICAnXFxuY3VycmVudEJhbmR3aWR0aDogJyArIChjdXJyZW50QmFuZHdpZHRoIHx8ICc/Jyk7XG4gIH1cblxuICBnZXQgbG9ncygpOiBzdHJpbmdbXSB7XG4gICAgcmV0dXJuIHRoaXMuX2xvZ3M7XG4gIH1cblxuICBnZXQgcGxheWVyU3RhdHVzSW5mb3MoKTogc3RyaW5nIHtcbiAgICByZXR1cm4gdGhpcy5fcGxheWVyU3RhdHVzSW5mb3M7XG4gIH1cbn1cbiJdfQ==