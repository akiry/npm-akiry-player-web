/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of akiry-player
 */
export { AkiryPlayerModule } from './lib/akiry-player.module';
export { C } from './lib/config/c';
export { iOSTools } from './lib/libs/utils/ios';
export { printConsoleLogs } from './lib/browser/console-logs-manager';
export { AkiryPlayerService } from './lib/services/akiry-player/akiry-player.service';
export { DEFAULT_TIME_TO_HIDE_CONTROLS, AkiryPlayerComponent } from './lib/components/akiry-player/akiry-player.component';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2FraXJ5LXBsYXllci8iLCJzb3VyY2VzIjpbInB1YmxpYy1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFJQSxrQ0FBYywyQkFBMkIsQ0FBQztBQUMxQyxrQkFBYyxnQkFBZ0IsQ0FBQztBQUMvQix5QkFBYyxzQkFBc0IsQ0FBQztBQUNyQyxpQ0FBYyxvQ0FBb0MsQ0FBQztBQUNuRCxtQ0FBYyxrREFBa0QsQ0FBQztBQUNqRSxvRUFBYyxzREFBc0QsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXG4gKiBQdWJsaWMgQVBJIFN1cmZhY2Ugb2YgYWtpcnktcGxheWVyXG4gKi9cblxuZXhwb3J0ICogZnJvbSAnLi9saWIvYWtpcnktcGxheWVyLm1vZHVsZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb25maWcvYyc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9saWJzL3V0aWxzL2lvcyc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9icm93c2VyL2NvbnNvbGUtbG9ncy1tYW5hZ2VyJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3NlcnZpY2VzL2FraXJ5LXBsYXllci9ha2lyeS1wbGF5ZXIuc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9jb21wb25lbnRzL2FraXJ5LXBsYXllci9ha2lyeS1wbGF5ZXIuY29tcG9uZW50JztcbiJdfQ==