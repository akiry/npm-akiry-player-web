(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/platform-browser'), require('util')) :
    typeof define === 'function' && define.amd ? define('akiry-player', ['exports', '@angular/core', '@angular/platform-browser', 'util'], factory) :
    (global = global || self, factory(global['akiry-player'] = {}, global.ng.core, global.ng.platformBrowser, global.util));
}(this, (function (exports, core, platformBrowser, util) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/config/c.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var C = {
        PLAYERS: {
            SHAKA: { name: 'shaka' },
            VIDEOJS: { name: 'videojs' },
        },
        TYPE: {
            HLS: { name: 'hls' },
            DASH: { name: 'dash' },
            MP4: { name: 'mp4' },
        },
        teste: {
            loginType: '',
        },
    };

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/libs/utils/ios.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function iOSToolsInterface() { }
    if (false) {
        /** @type {?|undefined} */
        iOSToolsInterface.prototype.processStringForPlatform;
        /** @type {?|undefined} */
        iOSToolsInterface.prototype.iOS;
    }
    /** @type {?} */
    var iOSTools = {};
    iOSTools.processStringForPlatform = (/**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        /** @type {?} */
        var names = url.split('/');
        /** @type {?} */
        var newUrl = [];
        names.forEach((/**
         * @param {?} name
         * @return {?}
         */
        function (name) {
            if (name === 'videos') {
                newUrl.push('ios');
            }
            newUrl.push(name);
        }));
        url = newUrl.join('/');
        return url;
    });
    iOSTools.iOS = (/**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var iDevices = [
            'iPad Simulator',
            'iPhone Simulator',
            'iPod Simulator',
            'iPad',
            'iPhone',
            'iPod'
        ];
        if (!!navigator.platform) {
            while (iDevices.length) {
                if (navigator.platform === iDevices.pop()) {
                    return true;
                }
            }
        }
        return false;
    });

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/services/akiry-player-console/akiry-player-console.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AkiryPlayerConsoleService = /** @class */ (function () {
        function AkiryPlayerConsoleService() {
            this._isVisible = false;
            this._logs = [];
            this._debug = true;
        }
        Object.defineProperty(AkiryPlayerConsoleService.prototype, "isVisible", {
            get: /**
             * @return {?}
             */
            function () {
                return this._isVisible;
            },
            set: /**
             * @param {?} isVisible
             * @return {?}
             */
            function (isVisible) {
                this._isVisible = isVisible;
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @param {?} logs
         * @return {?}
         */
        AkiryPlayerConsoleService.prototype.addLogs = /**
         * @param {?} logs
         * @return {?}
         */
        function (logs) {
            /** @type {?} */
            var now = new Date();
            /** @type {?} */
            var nowString = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds() + "." + now.getMilliseconds();
            this._logs.push(nowString + ' | ' + logs);
            if (this._debug) {
                this.isVisible = true;
                this._lastLog = new Date();
                // setTimeout(() => { if (Date.now() - this._lastLog.valueOf() > 4000) { this.isVisible = false; } }, 4500);
            }
        };
        /**
         * @param {?} currentTime
         * @param {?} bufferedTime
         * @param {?} duration
         * @param {?=} currentBitrate
         * @param {?=} currentBandwidth
         * @return {?}
         */
        AkiryPlayerConsoleService.prototype.setPlayerStatus = /**
         * @param {?} currentTime
         * @param {?} bufferedTime
         * @param {?} duration
         * @param {?=} currentBitrate
         * @param {?=} currentBandwidth
         * @return {?}
         */
        function (currentTime, bufferedTime, duration, currentBitrate, currentBandwidth) {
            this._playerStatusInfos =
                'Current time: ' + currentTime + '/' + (duration || '?') +
                    '\nBuffered time: ' + bufferedTime + '/' + (duration || '?') +
                    '\ncurrentBitrate: ' + (currentBitrate || '?') +
                    '\ncurrentBandwidth: ' + (currentBandwidth || '?');
        };
        Object.defineProperty(AkiryPlayerConsoleService.prototype, "logs", {
            get: /**
             * @return {?}
             */
            function () {
                return this._logs;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AkiryPlayerConsoleService.prototype, "playerStatusInfos", {
            get: /**
             * @return {?}
             */
            function () {
                return this._playerStatusInfos;
            },
            enumerable: true,
            configurable: true
        });
        AkiryPlayerConsoleService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root',
                    },] }
        ];
        /** @nocollapse */
        AkiryPlayerConsoleService.ctorParameters = function () { return []; };
        /** @nocollapse */ AkiryPlayerConsoleService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function AkiryPlayerConsoleService_Factory() { return new AkiryPlayerConsoleService(); }, token: AkiryPlayerConsoleService, providedIn: "root" });
        return AkiryPlayerConsoleService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerConsoleService.prototype._isVisible;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerConsoleService.prototype._logs;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerConsoleService.prototype._lastLog;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerConsoleService.prototype._debug;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerConsoleService.prototype._playerStatusInfos;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/directives/akiry-player/akiry-player.directive.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    // import * as videojs from '../../libs/min/videojs/dist/video.min.js';
    /** @type {?} */
    var videojs = require('../../libs/min/videojs/dist/video.min.js');
    /** @type {?} */
    var DEFAULT_INITIAL_PLAYLIST = 1;
    // TODO: Importar MUX.JS
    /**
     * Importing VideoJS
     */
    // tslint:disable-next-line:no-require-imports
    // const videojs = require('../../libs/min/videojs/dist/video.min.js'); // tslint:disable-line:no-var-requires
    ((/** @type {?} */ (window))).videojs = videojs;
    // tslint:disable-next-line:no-require-imports
    /**
     * Importing ShakaPlayer
     * @type {?}
     */
    var shaka = require('../../libs/min/shaka/shaka-player.compiled.js');
    // tslint:disable-line:no-var-requires
    // tslint:disable-next-line:no-require-imports
    require('../../libs/min/shaka/shaka-player.compiled.debug.js'); // tslint:disable-line:no-var-requires
    // tslint:disable-line:no-var-requires
    var AkiryPlayerDirective = /** @class */ (function () {
        function AkiryPlayerDirective(el, _renderer, _akiryConsole) {
            this._renderer = _renderer;
            this._akiryConsole = _akiryConsole;
            this._internalListeners = {};
            this.currentPlaylist = DEFAULT_INITIAL_PLAYLIST;
            this._videoTag = el.nativeElement;
            // console.log('VideoJS lib: ', videojs);
            // console.log('Shaka lib: ', shaka);
        }
        /**
         * Play, if the player is paused, and pause, if playing
         */
        /**
         * Play, if the player is paused, and pause, if playing
         * @return {?}
         */
        AkiryPlayerDirective.prototype.playOrPause = /**
         * Play, if the player is paused, and pause, if playing
         * @return {?}
         */
        function () {
            if (this._videoTag !== undefined) {
                if (this._videoTag.paused) {
                    if (iOSTools.iOS()) {
                        switch (this._playerOptions.player) {
                            case C.PLAYERS.VIDEOJS:
                                this._videojs.play();
                                break;
                            default:
                                this._videoTag.play();
                                break;
                        }
                    }
                    else {
                        this._videoTag.play();
                    }
                }
                else {
                    this._videoTag.pause();
                }
            }
            else {
                console.error('Player is null');
            }
        };
        /**
         * @return {?}
         */
        AkiryPlayerDirective.prototype.fullscreen = /**
         * @return {?}
         */
        function () {
            if (this._videoTag.requestFullscreen) {
                this._videoTag.requestFullscreen();
            }
            else if (((/** @type {?} */ (this._videoTag))).mozRequestFullScreen) {
                ((/** @type {?} */ (this._videoTag))).mozRequestFullScreen();
            }
            else if (((/** @type {?} */ (this._videoTag))).webkitRequestFullscreen) {
                ((/** @type {?} */ (this._videoTag))).webkitRequestFullscreen();
            }
            if (iOSTools.iOS()) {
                if (this._videoTag.webkitEnterFullscreen) {
                    this._videoTag.webkitEnterFullscreen();
                }
                if (((/** @type {?} */ (this._videoTag))).enterFullscreen) {
                    ((/** @type {?} */ (this._videoTag))).enterFullscreen();
                }
            }
        };
        /**
         * Play only
         */
        /**
         * Play only
         * @return {?}
         */
        AkiryPlayerDirective.prototype.play = /**
         * Play only
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    if (this._videoTag !== undefined) {
                        if (iOSTools.iOS()) {
                            this._videoTag.play()
                                .then((/**
                             * @return {?}
                             */
                            function () { }))
                                .catch((/**
                             * @param {?} error
                             * @return {?}
                             */
                            function (error) {
                                console.error('Error on autoplay video', error);
                                _this._videoTag.muted = true;
                                _this._videoTag.play();
                            }));
                        }
                        else {
                            this._videoTag.play() // FIXME: not working when refresh the page
                                .then((/**
                             * @return {?}
                             */
                            function () { }))
                                .catch((/**
                             * @param {?} error
                             * @return {?}
                             */
                            function (error) {
                                console.error('Error on autoplay video', error);
                                _this._videoTag.muted = true;
                                _this._videoTag.play();
                            }));
                        }
                    }
                    else {
                        console.error('Player is null');
                    }
                    return [2 /*return*/];
                });
            });
        };
        /**
         * Pause only
         */
        /**
         * Pause only
         * @return {?}
         */
        AkiryPlayerDirective.prototype.pause = /**
         * Pause only
         * @return {?}
         */
        function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    if (this._videoTag !== undefined) {
                        this._videoTag.pause();
                    }
                    else {
                        console.error('Player is null');
                    }
                    return [2 /*return*/];
                });
            });
        };
        /**
         * Method to unbuild player, for release resources
         */
        /**
         * Method to unbuild player, for release resources
         * @return {?}
         */
        AkiryPlayerDirective.prototype.release = /**
         * Method to unbuild player, for release resources
         * @return {?}
         */
        function () {
            if (this._playerOptions) {
                switch (this._playerOptions.player) {
                    case C.PLAYERS.VIDEOJS:
                        if (this._videojs) {
                            this._videojs.dispose();
                        }
                        break;
                    default:
                        console.error('Nothing todo. There is not player selected.');
                        break;
                }
            }
        };
        /**
         * Method for build player and prepare to start.
         * @param video Video object with source link (src) and type (hls, dash, mp4)
         * @param opt Options for play (player, has mode, adaptive algorithm, etc)
         */
        /**
         * Method for build player and prepare to start.
         * @param {?} url
         * @param {?} playerOptions
         * @return {?}
         */
        AkiryPlayerDirective.prototype.load = /**
         * Method for build player and prepare to start.
         * @param {?} url
         * @param {?} playerOptions
         * @return {?}
         */
        function (url, playerOptions) {
            this._playerOptions = playerOptions;
            // if (this._playerOptions.evaluator === undefined) {
            //   this._playerOptions.evaluator = new FixFormat(0, () => this._videojs);
            // }
            if (iOSTools.iOS()) {
                this.loadNativeFor_iOS(url);
            }
            else {
                switch (this._playerOptions.player) {
                    case C.PLAYERS.SHAKA:
                        this.loadShaka(url);
                        break;
                    case C.PLAYERS.VIDEOJS:
                        this.loadVideoJs(url);
                        break;
                    default:
                        console.error('There is not player selected. Select one with `opt.player = C.PLAYERS.${SELECTED_PLAYER}`.');
                        break;
                }
            }
            this.generalBuild();
        };
        /********************************************************
         *
         *
         * GENERAL AREA
         *
         *
         **********************************************************/
        /**
         * *****************************************************
         *
         *
         * GENERAL AREA
         *
         *
         * ********************************************************
         * @private
         * @return {?}
         */
        AkiryPlayerDirective.prototype.generalBuild = /**
         * *****************************************************
         *
         *
         * GENERAL AREA
         *
         *
         * ********************************************************
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            if (this._playerOptions.thumb) {
                this._videoTag.poster = this._playerOptions.thumb;
            }
            this.buildListeners();
            // set key 'space' to play/pause
            /** @type {?} */
            var oldOnKeyUp = document.body.onkeyup;
            document.body.onkeyup = (/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                // console.log(event.key);
                switch (event.key) {
                    case ' ' || 'Spacebar':
                        _this.playOrPause();
                        break;
                    case 'ArrowLeft':
                        _this._videoTag.currentTime = _this._videoTag.currentTime < 5 ? 0 : _this._videoTag.currentTime - 5;
                        break;
                    case 'ArrowRight':
                        _this._videoTag.currentTime =
                            _this._videoTag.currentTime > _this._videoTag.duration - 5 ?
                                _this._videoTag.currentTime :
                                _this._videoTag.currentTime + 5;
                        break;
                    case 'ArrowUp':
                        _this._videoTag.volume = _this._videoTag.volume > 0.9 ? 1 : _this._videoTag.volume + 0.1;
                        break;
                    case 'ArrowDown':
                        _this._videoTag.volume = _this._videoTag.volume < 0.1 ? 0 : _this._videoTag.volume - 0.1;
                        break;
                    default:
                        if (oldOnKeyUp) {
                            oldOnKeyUp(event);
                        }
                        break;
                }
            });
        };
        /**
         * @private
         * @return {?}
         */
        AkiryPlayerDirective.prototype.buildPlayerSkin = /**
         * @private
         * @return {?}
         */
        function () {
            /**
             * Insert personal company loader
             */
            // const spinners = document.getElementsByClassName('vjs-loading-spinner');
            // const spinner: Element = spinners && spinners.length > 0 ? spinners[0] : undefined;
            // if (spinner && this._playerOptions && this._playerOptions.controlsImgPath && this._playerOptions.controlsImgPath.loadingButton) {
            //   spinner.innerHTML =
            //     `<img src="${this._playerOptions.controlsImgPath.loadingButton}" class="vjs-loading-spinner-img" alt="Loading...">`;
            // }
            /**
             * Others controls powered by Akiry
             */
        };
        /**
         * @private
         * @return {?}
         */
        AkiryPlayerDirective.prototype.buildListeners = /**
         * @private
         * @return {?}
         */
        function () {
            // this._videoTag.on('timeupdate', () => {
            //   this._akiryConsole.addLogs('timeupdated');
            //   // this.previousTime = this.currentTime;
            //   // this.currentTime = this._videojs.cache_.currentTime;
            //   // //        console.log('timeUpdate: ' + this.currentTime);
            var e_1, _a;
            var _this = this;
            // this._videoTag.on('timeupdate', () => {
            //   this._akiryConsole.addLogs('timeupdated');
            //   // this.previousTime = this.currentTime;
            //   // this.currentTime = this._videojs.cache_.currentTime;
            //   // //        console.log('timeUpdate: ' + this.currentTime);
            //   // const buffered = this._videojs.buffered();
            //   // const bufferStart = buffered.start ? buffered.start(buffered.length - 1) : -1;
            //   // const bufferEnd = buffered.end ? buffered.end(buffered.length - 1) : -1;
            //   // this.onTimeUpdated(this.currentTime, bufferEnd);
            //   // // const bufferStartProt = buffered.__prot__.end ? buffered.__prot__.end(0) : 'não rolou';
            //   // //        console.log('buffered: ', bufferStart, bufferEnd, buffered.length);
            // });
            // this._videoTag.on('progress', () => {
            //   this._akiryConsole.addLogs('progress');
            // });
            /** @type {?} */
            var events = [
                'abort',
                'canplay',
                'canplaythrough',
                'durationchange',
                'emptied',
                'ended',
                'error',
                'loadeddata',
                'loadedmetadata',
                'loadstart',
                'pause',
                'play',
                'playing',
                // 'progress',         // Works on iOS - 5 and ever
                'ratechange',
                'seeked',
                'seeking',
                'stalled',
                'suspend',
                // 'timeupdate',       // Works on iOS - onplay 3 and while is playing
                // 'volumechange',     // Works on iOS - when volume change
                'waiting',
            ];
            var _loop_1 = function (event_1) {
                if (iOSTools.iOS()) {
                    this_1._videoTag.addEventListener(event_1, (/**
                     * @return {?}
                     */
                    function () { _this._akiryConsole.addLogs('Event: ' + event_1); }), false);
                }
                else {
                    this_1._videoTag.addEventListener(event_1, (/**
                     * @return {?}
                     */
                    function () { _this._akiryConsole.addLogs('Event: ' + event_1); }), false);
                }
            };
            var this_1 = this;
            try {
                for (var events_1 = __values(events), events_1_1 = events_1.next(); !events_1_1.done; events_1_1 = events_1.next()) {
                    var event_1 = events_1_1.value;
                    _loop_1(event_1);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (events_1_1 && !events_1_1.done && (_a = events_1.return)) _a.call(events_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
            this._videoTag.addEventListener('progress', (/**
             * @return {?}
             */
            function () {
                _this.onTimeUpdated();
            }), false);
            this._videoTag.addEventListener('waiting', (/**
             * @return {?}
             */
            function () {
                if (_this._internalListeners.onWaiting) {
                    _this._internalListeners.onWaiting();
                }
            }));
            this._videoTag.addEventListener('ended', (/**
             * @return {?}
             */
            function () {
                if (_this._playerOptions && _this._playerOptions.callbacks && _this._playerOptions.callbacks.onEnded) {
                    _this._playerOptions.callbacks.onEnded();
                }
            }));
            // this._videojs.on('progress', () => {
            //   this.previousTime = this.currentTime;
            //   this.currentTime = this._videojs.cache_.currentTime;
            //   //        console.log('timeUpdate: ' + this.currentTime);
            //   const buffered = this._videojs.buffered();
            //   const bufferStart = buffered.start ? buffered.start(buffered.length - 1) : -1;
            //   const bufferEnd = buffered.end ? buffered.end(buffered.length - 1) : -1;
            //   console.log('--', !this._isNotInitialBuffering, '--|--', bufferEnd > 0)
            //   if (!this._isNotInitialBuffering && bufferEnd > 0) {
            //     if (!this._videoTag.paused) {
            //       console.log('pause')
            //       this._isNotInitialBuffering = true;
            //       this._videojs.pause();
            //     } else {
            //       console.log('delayed play')
            //       this._delayedPlayed = true;
            //       // method to start initial buffering on iOS but it also autostart on others devices
            //       const playButtonBottom: HTMLButtonElement =
            //         Array.from(document.getElementsByClassName('vjs-play-control vjs-control vjs-button'))[0] as HTMLButtonElement;
            //       playButtonBottom.click();
            //       this._videojs.play().catch(() => {
            //         this._videoTag.volume = 0;
            //         this._delayedPlayed = false;
            //         this._isNotInitialBuffering = false;
            //       });
            //     }
            //   }
            //   this.onTimeUpdated(this.currentTime, bufferEnd);
            //   // const bufferStartProt = buffered.__prot__.end ? buffered.__prot__.end(0) : 'não rolou';
            //   //        console.log('buffered: ', bufferStart, bufferEnd, buffered.length);
            // });
            // this._videojs.on('seeking', () => {
            //   this.seekStart = this.previousTime;
            //   //        console.log('seeking: ' + this.currentTime);
            // });
            // this._videojs.on('seeked', () => {
            //   //        console.log('seeked from', this.seekStart, 'to', this.currentTime, '; delta:', this.currentTime - this.seekStart);
            // });
            // this._videojs.on('loadedmetadata', () => {
            //   this._akiryConsole.addLogs('Loaded Metadata.');
            //   //  console.log('loadedmetadata.');
            //   if (this._playerOptions.autoplay) {
            //     this.play();
            //   }
            // });
            // this._videojs.on('play', () => {
            //   this._akiryConsole.addLogs('play.');
            //   //      console.log('play.');
            // });
            // this._videojs.on('volumechange', () => {
            //   this._akiryConsole.addLogs('volumechange.');
            //   //      console.log('volumechange.');
            // });
            // this._videojs.on('useractive', () => {
            //   //      console.log('useractive.');
            // });
            // this._videojs.on('userinactive', () => {
            //   //      console.log('userinactive.');
            // });
            // this._videojs.on('error', () => {
            //   //      console.log('error.');
            //   if (this._playerOptions && this._playerOptions.callbacks && this._playerOptions.callbacks.onError) {
            //     this._playerOptions.callbacks.onError();
            //   }
            // });
            // this._videojs.on('pause', () => {
            //   //      console.log('pause.');
            // });
            // this._videojs.on('fullscreenchange', () => {
            //   //      console.log('fullscreenchange.');
            // });
            // this._videojs.on('waiting', () => {
            //   //      console.log('waiting.', this._videojs);
            // });
            // this._videojs.on('playing', () => {
            //   console.log('playing.', this._videojs);
            // });
            // this._videojs.on('suspend', () => {
            //   console.log('suspend.', this._videojs);
            // });
            // this._videojs.on('stalled', () => {
            //   console.log('stalled.', this._videojs);
            // });
            // this._videojs.on('ready', () => {
            //   //      console.log('ready.');
            // });
        };
        /**
         * @param {?} onTimeUpdate
         * @return {?}
         */
        AkiryPlayerDirective.prototype.setOnTimeUpdatedListener = /**
         * @param {?} onTimeUpdate
         * @return {?}
         */
        function (onTimeUpdate) {
            this._internalListeners.onTimeUpdate = onTimeUpdate;
        };
        /**
         * @param {?} onWaiting
         * @return {?}
         */
        AkiryPlayerDirective.prototype.setOnWaitingListener = /**
         * @param {?} onWaiting
         * @return {?}
         */
        function (onWaiting) {
            this._internalListeners.onWaiting = onWaiting;
        };
        /**
         * @return {?}
         */
        AkiryPlayerDirective.prototype.onTimeUpdated = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var currentTime = this._videoTag.currentTime;
            /** @type {?} */
            var bufferedTime = this._videoTag.buffered && this._videoTag.buffered.length > 0 ? this._videoTag.buffered.end(0) : 0;
            /** @type {?} */
            var duration = this._videoTag.duration;
            /** @type {?} */
            var videoHeight = this._videoTag.videoHeight;
            this._akiryConsole.setPlayerStatus(currentTime, bufferedTime, duration, videoHeight);
            if (this._internalListeners.onTimeUpdate) {
                this._internalListeners.onTimeUpdate(currentTime, bufferedTime);
            }
        };
        /********************************************************
         *
         *
         * SHAKA AREA
         *
         *
         **********************************************************/
        /**
         * Private method for build Shaka player and prepare to start.
         * @param video Video object with source link (src) and type (hls, dash, mp4)
         */
        /********************************************************
           *
           *
           * SHAKA AREA
           *
           *
           **********************************************************/
        /**
         * Private method for build Shaka player and prepare to start.
         * @private
         * @param {?} url
         * @return {?}
         */
        AkiryPlayerDirective.prototype.loadShaka = /********************************************************
           *
           *
           * SHAKA AREA
           *
           *
           **********************************************************/
        /**
         * Private method for build Shaka player and prepare to start.
         * @private
         * @param {?} url
         * @return {?}
         */
        function (url) {
            // Install built-in polyfills to patch browser incompatibilities.
            shaka.polyfill.installAll();
            // Check to see if the browser supports the basic APIs Shaka needs.
            if (!shaka.Player.isBrowserSupported()) {
                // This browser does not have the minimum set of APIs we need.
                console.error('Browser not supported!');
                return;
            }
            // Everything looks good!
            switch (this._playerOptions.mode) {
                case C.TYPE.HLS:
                    this.buildShakaPlayer(url);
                    break;
                case C.TYPE.DASH:
                    this.buildShakaPlayer(url);
                    break;
                case C.TYPE.MP4:
                    this.buildShakaPlayer(url);
                    break;
                default:
                    break;
            }
        };
        /**
         * @private
         * @param {?} url
         * @return {?}
         */
        AkiryPlayerDirective.prototype.buildShakaPlayer = /**
         * @private
         * @param {?} url
         * @return {?}
         */
        function (url) {
            var _this = this;
            /** @type {?} */
            var onError = (/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                // Log the error.
                console.error('Error code', error.code, 'object', error);
            });
            /** @type {?} */
            var onErrorEvent = (/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                // Extract the shaka.util.Error object from the event.
                onError(event.detail);
            });
            // Create a Player instance.
            this._shaka = new shaka.Player(this._videoTag);
            // Attach player to the window to make it easy to access in the JS console.
            ((/** @type {?} */ (window))).player = this._shaka;
            // Listen for error events.
            this._shaka.addEventListener('error', onErrorEvent);
            // Configure buffer length
            this._shaka.configure({
                streaming: {
                    bufferingGoal: 120,
                    rebufferingGoal: 15,
                    retryParameters: {
                        timeout: 0,
                        // timeout in ms, after which we abort; 0 means never
                        maxAttempts: 4,
                        // the maximum number of requests before we fail
                        baseDelay: 500,
                        // the base delay in ms between retries
                        backoffFactor: 2,
                        // the multiplicative backoff factor between retries
                        fuzzFactor: 0.5,
                    },
                },
            });
            // Configure all requests
            this._shaka.getNetworkingEngine().registerRequestFilter((/**
             * @param {?} type
             * @param {?} request
             * @return {?}
             */
            function (type, request) {
                if (_this._playerOptions.token) {
                    if (!request.headers) {
                        request.header = {};
                    }
                    request.headers['Authorization'] = 'Bearer ' + _this._playerOptions.token;
                }
            }));
            // Try to load a manifest.
            // This is an asynchronous process.
            this._shaka.load(url).then((/**
             * @return {?}
             */
            function () {
                // This runs if the asynchronous load is successful.
                console.info('The video has now been loaded!'); // tslint:disable-line:no-console
            })).catch(onError); // onError is executed if the asynchronous load fails.
        };
        /********************************************************
         *
         *
         * VIDEOJS AREA
         *
         *
         **********************************************************/
        /**
         * Private method for build VideoJs player and prepare to start.
         * @param video Video object with source link (src) and type (hls, dash, mp4)
         */
        /********************************************************
           *
           *
           * VIDEOJS AREA
           *
           *
           **********************************************************/
        /**
         * Private method for build VideoJs player and prepare to start.
         * @private
         * @param {?} url
         * @return {?}
         */
        AkiryPlayerDirective.prototype.loadVideoJs = /********************************************************
           *
           *
           * VIDEOJS AREA
           *
           *
           **********************************************************/
        /**
         * Private method for build VideoJs player and prepare to start.
         * @private
         * @param {?} url
         * @return {?}
         */
        function (url) {
            this._renderer.addClass(this._videoTag, 'video-js');
            this._renderer.addClass(this._videoTag, 'vjs-default-skin');
            switch (this._playerOptions.mode) {
                case C.TYPE.HLS:
                    this.buildVjsHlsPlayer(url);
                    break;
                case C.TYPE.MP4:
                    this.buildVjsMp4Player(url);
                    break;
                default:
                    this.buildVjsMp4Player(url);
                    break;
            }
        };
        /**
         * Method for build HLS Player with VideoJS
         * @param video
         */
        /**
         * Method for build HLS Player with VideoJS
         * @private
         * @param {?} url
         * @return {?}
         */
        AkiryPlayerDirective.prototype.buildVjsHlsPlayer = /**
         * Method for build HLS Player with VideoJS
         * @private
         * @param {?} url
         * @return {?}
         */
        function (url) {
            var _this = this;
            /** @type {?} */
            var overrideNative = true;
            this._opts = {
                html5: {
                    hls: {
                        debug: true,
                        // bandwidth: 500000, // initial bandwidth
                        overrideNative: overrideNative,
                    },
                    nativeVideoTracks: !overrideNative,
                    nativeAudioTracks: !overrideNative,
                    nativeTextTracks: !overrideNative,
                },
                bigPlayButton: this._playerOptions.bigPlayButton === undefined ? true : this._playerOptions.bigPlayButton,
                autoplay: this._playerOptions.autoplay || false,
                playsinline: true,
                errorDisplay: !this._playerOptions.prod,
            };
            videojs.Hls.xhr('error', (/**
             * @param {?} anything
             * @return {?}
             */
            function (anything) { console.error('Erro XHR capturado.', anything); }));
            videojs.Hls.xhr.beforeRequest = (/**
             * @param {?} options
             * @return {?}
             */
            function (options) {
                if (!util.isUndefined(_this._playerOptions.token) && !util.isUndefined(options)) {
                    if (util.isUndefined(options.headers)) {
                        options.headers = {};
                    }
                    options.headers.Authorization = 'Bearer ' + _this._playerOptions.token;
                    options.timeout = 45000;
                }
            });
            /**
             * Initialize player
             */
            this._videojs = videojs('akiryPlayerId', this._opts);
            this._videojs.src({
                src: url,
                type: 'application/x-mpegURL',
                withCredentials: false,
            });
            /**
             * OnReady -> Before the download of first chunk
             */
            this._videojs.ready((/**
             * @return {?}
             */
            function () {
                var e_2, _a;
                _this._akiryConsole.addLogs('Executing onReady...');
                // force to show bottom controls
                /** @type {?} */
                var vjsControlBars = Array.from(document.querySelectorAll('.vjs-default-skin.vjs-paused .vjs-control-bar'));
                try {
                    for (var vjsControlBars_1 = __values(vjsControlBars), vjsControlBars_1_1 = vjsControlBars_1.next(); !vjsControlBars_1_1.done; vjsControlBars_1_1 = vjsControlBars_1.next()) {
                        var vjsControlBar = vjsControlBars_1_1.value;
                        _this._renderer.setStyle(vjsControlBar, 'display', 'flex');
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (vjsControlBars_1_1 && !vjsControlBars_1_1.done && (_a = vjsControlBars_1.return)) _a.call(vjsControlBars_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
                // method to start initial buffering on iOS but it also autostart on others devices
                /** @type {?} */
                var playButtonBottom = (/** @type {?} */ (Array.from(document.getElementsByClassName('vjs-play-control vjs-control vjs-button'))[0]));
                // playButtonBottom.click();
                /**
                 * Setup adaptation strategy
                 */
                if (_this._videojs && _this._videojs.tech_ && _this._videojs.tech_.hls) {
                    _this._videojs.tech_.hls.selectPlaylist = (/**
                     * @return {?}
                     */
                    function () {
                        /** @type {?} */
                        var hls = _this._videojs.tech_.hls;
                        /** @type {?} */
                        var playlists = hls.playlists.master.playlists;
                        /** @type {?} */
                        var formats = playlists.length;
                        /** @type {?} */
                        var currentTime = _this._videojs.cache_.currentTime;
                        /** @type {?} */
                        var bandwidth = hls.bandwidth;
                        console.log(playlists);
                        /** @type {?} */
                        var selectedPlaylist;
                        if (_this.currentPlaylist < playlists.length) {
                            selectedPlaylist = _this.currentPlaylist;
                        }
                        else {
                            console.error('Current playlist doesn\'t exist');
                            selectedPlaylist = 0;
                        }
                        //   this._akiryConsole.addLogs(JSON.stringify({ currentTime, formats, bandwidth }));
                        if (_this._playerOptions.evaluator) {
                            selectedPlaylist = _this._playerOptions.evaluator.evaluate(currentTime, 0, playlists, bandwidth);
                        }
                        //   this._akiryConsole.addLogs(
                        //     'Bandwidth: ' + bandwidth +
                        //     '\nSelected: ' + (selectedPlaylist + 1) + '/' + playlists.length,
                        //   );
                        return playlists[selectedPlaylist];
                    });
                }
                else {
                    _this._akiryConsole.addLogs('Akiry Adaptation is disabled. Native player executing.');
                }
                _this.buildPlayerSkin();
                if (_this._playerOptions.callbacks && _this._playerOptions.callbacks.onReady) {
                    _this._playerOptions.callbacks.onReady();
                }
                // this._videojs.play();
                // this._videoTag.play();
                // const playButton: HTMLButtonElement = document.getElementsByClassName('vjs-big-play-button')[0] as HTMLButtonElement;
                // playButton.click();
                // playButton.style.display = 'none';
                _this._akiryConsole.addLogs('onReady executed.');
            }));
            // this._videoTag.load(); // Não obrigatório
        };
        /**
         * Method for build HLS Player with VideoJS
         * @param video
         */
        /**
         * Method for build HLS Player with VideoJS
         * @private
         * @param {?} url
         * @return {?}
         */
        AkiryPlayerDirective.prototype.buildVjsMp4Player = /**
         * Method for build HLS Player with VideoJS
         * @private
         * @param {?} url
         * @return {?}
         */
        function (url) {
            // console.log('this.buildMp4Player');
            this._videojs = videojs(this._videoTag);
            this._videojs.src(url);
        };
        /********************************************************
         *
         *
         * iOS NATIVE HLS AREA
         *
         *
         **********************************************************/
        /**
         * *****************************************************
         *
         *
         * iOS NATIVE HLS AREA
         *
         *
         * ********************************************************
         * @private
         * @param {?} url
         * @return {?}
         */
        AkiryPlayerDirective.prototype.loadNativeFor_iOS = /**
         * *****************************************************
         *
         *
         * iOS NATIVE HLS AREA
         *
         *
         * ********************************************************
         * @private
         * @param {?} url
         * @return {?}
         */
        function (url) {
            url = iOSTools.processStringForPlatform(url);
            this._videoTag.src = url;
            this._videoTag.load();
            this._akiryConsole.addLogs('Starting IOS');
        };
        AkiryPlayerDirective.decorators = [
            { type: core.Directive, args: [{
                        selector: '[libAkiryPlayer]',
                    },] }
        ];
        /** @nocollapse */
        AkiryPlayerDirective.ctorParameters = function () { return [
            { type: core.ElementRef },
            { type: core.Renderer2 },
            { type: AkiryPlayerConsoleService }
        ]; };
        return AkiryPlayerDirective;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerDirective.prototype._videoTag;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerDirective.prototype._videojs;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerDirective.prototype._shaka;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerDirective.prototype._opts;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerDirective.prototype._playerOptions;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerDirective.prototype._internalListeners;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerDirective.prototype.currentPlaylist;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerDirective.prototype._renderer;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerDirective.prototype._akiryConsole;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/components/loading-circle/loading-circle.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var LoadingCircleComponent = /** @class */ (function () {
        function LoadingCircleComponent() {
            this._percentage = 0;
            this.ngClassObj = {
                p0: true,
            };
        }
        /**
         * @return {?}
         */
        LoadingCircleComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () { };
        Object.defineProperty(LoadingCircleComponent.prototype, "percentage", {
            get: /**
             * @return {?}
             */
            function () {
                return this._percentage;
            },
            set: /**
             * @param {?} percentage
             * @return {?}
             */
            function (percentage) {
                this.ngClassObj = {};
                this.ngClassObj['p' + percentage] = true;
                this._percentage = percentage;
            },
            enumerable: true,
            configurable: true
        });
        LoadingCircleComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'lib-loading-circle',
                        template: "<div [ngClass]='ngClassObj' class=\"c100 blue\">\n  <span>{{percentage}}%</span>\n  <div class=\"slice\">\n    <div class=\"bar\"></div>\n    <div class=\"fill\"></div>\n  </div>\n</div>\n",
                        styles: [".c100.p100 .slice,.c100.p51 .slice,.c100.p52 .slice,.c100.p53 .slice,.c100.p54 .slice,.c100.p55 .slice,.c100.p56 .slice,.c100.p57 .slice,.c100.p58 .slice,.c100.p59 .slice,.c100.p60 .slice,.c100.p61 .slice,.c100.p62 .slice,.c100.p63 .slice,.c100.p64 .slice,.c100.p65 .slice,.c100.p66 .slice,.c100.p67 .slice,.c100.p68 .slice,.c100.p69 .slice,.c100.p70 .slice,.c100.p71 .slice,.c100.p72 .slice,.c100.p73 .slice,.c100.p74 .slice,.c100.p75 .slice,.c100.p76 .slice,.c100.p77 .slice,.c100.p78 .slice,.c100.p79 .slice,.c100.p80 .slice,.c100.p81 .slice,.c100.p82 .slice,.c100.p83 .slice,.c100.p84 .slice,.c100.p85 .slice,.c100.p86 .slice,.c100.p87 .slice,.c100.p88 .slice,.c100.p89 .slice,.c100.p90 .slice,.c100.p91 .slice,.c100.p92 .slice,.c100.p93 .slice,.c100.p94 .slice,.c100.p95 .slice,.c100.p96 .slice,.c100.p97 .slice,.c100.p98 .slice,.c100.p99 .slice,.rect-auto{clip:rect(auto,auto,auto,auto)}.c100 .bar,.c100.p100 .fill,.c100.p51 .fill,.c100.p52 .fill,.c100.p53 .fill,.c100.p54 .fill,.c100.p55 .fill,.c100.p56 .fill,.c100.p57 .fill,.c100.p58 .fill,.c100.p59 .fill,.c100.p60 .fill,.c100.p61 .fill,.c100.p62 .fill,.c100.p63 .fill,.c100.p64 .fill,.c100.p65 .fill,.c100.p66 .fill,.c100.p67 .fill,.c100.p68 .fill,.c100.p69 .fill,.c100.p70 .fill,.c100.p71 .fill,.c100.p72 .fill,.c100.p73 .fill,.c100.p74 .fill,.c100.p75 .fill,.c100.p76 .fill,.c100.p77 .fill,.c100.p78 .fill,.c100.p79 .fill,.c100.p80 .fill,.c100.p81 .fill,.c100.p82 .fill,.c100.p83 .fill,.c100.p84 .fill,.c100.p85 .fill,.c100.p86 .fill,.c100.p87 .fill,.c100.p88 .fill,.c100.p89 .fill,.c100.p90 .fill,.c100.p91 .fill,.c100.p92 .fill,.c100.p93 .fill,.c100.p94 .fill,.c100.p95 .fill,.c100.p96 .fill,.c100.p97 .fill,.c100.p98 .fill,.c100.p99 .fill,.pie{position:absolute;border:.09em solid #000;width:.82em;height:.82em;clip:rect(0,.5em,1em,0);border-radius:50%;transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);-webkit-transform:rotate(0)}.c100.p100 .bar:after,.c100.p100 .fill,.c100.p51 .bar:after,.c100.p51 .fill,.c100.p52 .bar:after,.c100.p52 .fill,.c100.p53 .bar:after,.c100.p53 .fill,.c100.p54 .bar:after,.c100.p54 .fill,.c100.p55 .bar:after,.c100.p55 .fill,.c100.p56 .bar:after,.c100.p56 .fill,.c100.p57 .bar:after,.c100.p57 .fill,.c100.p58 .bar:after,.c100.p58 .fill,.c100.p59 .bar:after,.c100.p59 .fill,.c100.p60 .bar:after,.c100.p60 .fill,.c100.p61 .bar:after,.c100.p61 .fill,.c100.p62 .bar:after,.c100.p62 .fill,.c100.p63 .bar:after,.c100.p63 .fill,.c100.p64 .bar:after,.c100.p64 .fill,.c100.p65 .bar:after,.c100.p65 .fill,.c100.p66 .bar:after,.c100.p66 .fill,.c100.p67 .bar:after,.c100.p67 .fill,.c100.p68 .bar:after,.c100.p68 .fill,.c100.p69 .bar:after,.c100.p69 .fill,.c100.p70 .bar:after,.c100.p70 .fill,.c100.p71 .bar:after,.c100.p71 .fill,.c100.p72 .bar:after,.c100.p72 .fill,.c100.p73 .bar:after,.c100.p73 .fill,.c100.p74 .bar:after,.c100.p74 .fill,.c100.p75 .bar:after,.c100.p75 .fill,.c100.p76 .bar:after,.c100.p76 .fill,.c100.p77 .bar:after,.c100.p77 .fill,.c100.p78 .bar:after,.c100.p78 .fill,.c100.p79 .bar:after,.c100.p79 .fill,.c100.p80 .bar:after,.c100.p80 .fill,.c100.p81 .bar:after,.c100.p81 .fill,.c100.p82 .bar:after,.c100.p82 .fill,.c100.p83 .bar:after,.c100.p83 .fill,.c100.p84 .bar:after,.c100.p84 .fill,.c100.p85 .bar:after,.c100.p85 .fill,.c100.p86 .bar:after,.c100.p86 .fill,.c100.p87 .bar:after,.c100.p87 .fill,.c100.p88 .bar:after,.c100.p88 .fill,.c100.p89 .bar:after,.c100.p89 .fill,.c100.p90 .bar:after,.c100.p90 .fill,.c100.p91 .bar:after,.c100.p91 .fill,.c100.p92 .bar:after,.c100.p92 .fill,.c100.p93 .bar:after,.c100.p93 .fill,.c100.p94 .bar:after,.c100.p94 .fill,.c100.p95 .bar:after,.c100.p95 .fill,.c100.p96 .bar:after,.c100.p96 .fill,.c100.p97 .bar:after,.c100.p97 .fill,.c100.p98 .bar:after,.c100.p98 .fill,.c100.p99 .bar:after,.c100.p99 .fill,.pie-fill{transform:rotate(180deg);-moz-transform:rotate(180deg);-ms-transform:rotate(180deg);-o-transform:rotate(180deg);-webkit-transform:rotate(180deg)}.wrapper{width:1200px;margin:0 auto}.c100{position:relative;font-size:160px;width:1em;height:1em;border-radius:50%;float:left;margin:.4em;background-color:#dfe8ed}.c100 *,.c100 :after,.c100 :before{box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box}.c100.center{float:none;margin:0 auto}.c100.small{font-size:80px}.c100>span{position:absolute;z-index:1;left:0;top:0;width:5em;line-height:5em;font-size:.2em;color:#dfe8ed;display:block;text-align:center;white-space:nowrap}.c100:after{position:absolute;top:.09em;left:.09em;display:block;content:\" \";border-radius:50%;background-color:#000;width:.82em;height:.82em}.c100 .slice{position:absolute;width:1em;height:1em;clip:rect(0,1em,1em,.5em)}.c100.p1 .bar{transform:rotate(3.6deg);-moz-transform:rotate(3.6deg);-ms-transform:rotate(3.6deg);-o-transform:rotate(3.6deg);-webkit-transform:rotate(3.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p2 .bar{transform:rotate(7.2deg);-moz-transform:rotate(7.2deg);-ms-transform:rotate(7.2deg);-o-transform:rotate(7.2deg);-webkit-transform:rotate(7.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p3 .bar{transform:rotate(10.8deg);-moz-transform:rotate(10.8deg);-ms-transform:rotate(10.8deg);-o-transform:rotate(10.8deg);-webkit-transform:rotate(10.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p4 .bar{transform:rotate(14.4deg);-moz-transform:rotate(14.4deg);-ms-transform:rotate(14.4deg);-o-transform:rotate(14.4deg);-webkit-transform:rotate(14.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p5 .bar{transform:rotate(18deg);-moz-transform:rotate(18deg);-ms-transform:rotate(18deg);-o-transform:rotate(18deg);-webkit-transform:rotate(18deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p6 .bar{transform:rotate(21.6deg);-moz-transform:rotate(21.6deg);-ms-transform:rotate(21.6deg);-o-transform:rotate(21.6deg);-webkit-transform:rotate(21.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p7 .bar{transform:rotate(25.2deg);-moz-transform:rotate(25.2deg);-ms-transform:rotate(25.2deg);-o-transform:rotate(25.2deg);-webkit-transform:rotate(25.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p8 .bar{transform:rotate(28.8deg);-moz-transform:rotate(28.8deg);-ms-transform:rotate(28.8deg);-o-transform:rotate(28.8deg);-webkit-transform:rotate(28.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p9 .bar{transform:rotate(32.4deg);-moz-transform:rotate(32.4deg);-ms-transform:rotate(32.4deg);-o-transform:rotate(32.4deg);-webkit-transform:rotate(32.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p10 .bar{transform:rotate(36deg);-moz-transform:rotate(36deg);-ms-transform:rotate(36deg);-o-transform:rotate(36deg);-webkit-transform:rotate(36deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p11 .bar{transform:rotate(39.6deg);-moz-transform:rotate(39.6deg);-ms-transform:rotate(39.6deg);-o-transform:rotate(39.6deg);-webkit-transform:rotate(39.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p12 .bar{transform:rotate(43.2deg);-moz-transform:rotate(43.2deg);-ms-transform:rotate(43.2deg);-o-transform:rotate(43.2deg);-webkit-transform:rotate(43.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p13 .bar{transform:rotate(46.8deg);-moz-transform:rotate(46.8deg);-ms-transform:rotate(46.8deg);-o-transform:rotate(46.8deg);-webkit-transform:rotate(46.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p14 .bar{transform:rotate(50.4deg);-moz-transform:rotate(50.4deg);-ms-transform:rotate(50.4deg);-o-transform:rotate(50.4deg);-webkit-transform:rotate(50.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p15 .bar{transform:rotate(54deg);-moz-transform:rotate(54deg);-ms-transform:rotate(54deg);-o-transform:rotate(54deg);-webkit-transform:rotate(54deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p16 .bar{transform:rotate(57.6deg);-moz-transform:rotate(57.6deg);-ms-transform:rotate(57.6deg);-o-transform:rotate(57.6deg);-webkit-transform:rotate(57.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p17 .bar{transform:rotate(61.2deg);-moz-transform:rotate(61.2deg);-ms-transform:rotate(61.2deg);-o-transform:rotate(61.2deg);-webkit-transform:rotate(61.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p18 .bar{transform:rotate(64.8deg);-moz-transform:rotate(64.8deg);-ms-transform:rotate(64.8deg);-o-transform:rotate(64.8deg);-webkit-transform:rotate(64.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p19 .bar{transform:rotate(68.4deg);-moz-transform:rotate(68.4deg);-ms-transform:rotate(68.4deg);-o-transform:rotate(68.4deg);-webkit-transform:rotate(68.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p20 .bar{transform:rotate(72deg);-moz-transform:rotate(72deg);-ms-transform:rotate(72deg);-o-transform:rotate(72deg);-webkit-transform:rotate(72deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p21 .bar{transform:rotate(75.6deg);-moz-transform:rotate(75.6deg);-ms-transform:rotate(75.6deg);-o-transform:rotate(75.6deg);-webkit-transform:rotate(75.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p22 .bar{transform:rotate(79.2deg);-moz-transform:rotate(79.2deg);-ms-transform:rotate(79.2deg);-o-transform:rotate(79.2deg);-webkit-transform:rotate(79.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p23 .bar{transform:rotate(82.8deg);-moz-transform:rotate(82.8deg);-ms-transform:rotate(82.8deg);-o-transform:rotate(82.8deg);-webkit-transform:rotate(82.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p24 .bar{transform:rotate(86.4deg);-moz-transform:rotate(86.4deg);-ms-transform:rotate(86.4deg);-o-transform:rotate(86.4deg);-webkit-transform:rotate(86.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p25 .bar{transform:rotate(90deg);-moz-transform:rotate(90deg);-ms-transform:rotate(90deg);-o-transform:rotate(90deg);-webkit-transform:rotate(90deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p26 .bar{transform:rotate(93.6deg);-moz-transform:rotate(93.6deg);-ms-transform:rotate(93.6deg);-o-transform:rotate(93.6deg);-webkit-transform:rotate(93.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p27 .bar{transform:rotate(97.2deg);-moz-transform:rotate(97.2deg);-ms-transform:rotate(97.2deg);-o-transform:rotate(97.2deg);-webkit-transform:rotate(97.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p28 .bar{transform:rotate(100.8deg);-moz-transform:rotate(100.8deg);-ms-transform:rotate(100.8deg);-o-transform:rotate(100.8deg);-webkit-transform:rotate(100.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p29 .bar{transform:rotate(104.4deg);-moz-transform:rotate(104.4deg);-ms-transform:rotate(104.4deg);-o-transform:rotate(104.4deg);-webkit-transform:rotate(104.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p30 .bar{transform:rotate(108deg);-moz-transform:rotate(108deg);-ms-transform:rotate(108deg);-o-transform:rotate(108deg);-webkit-transform:rotate(108deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p31 .bar{transform:rotate(111.6deg);-moz-transform:rotate(111.6deg);-ms-transform:rotate(111.6deg);-o-transform:rotate(111.6deg);-webkit-transform:rotate(111.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p32 .bar{transform:rotate(115.2deg);-moz-transform:rotate(115.2deg);-ms-transform:rotate(115.2deg);-o-transform:rotate(115.2deg);-webkit-transform:rotate(115.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p33 .bar{transform:rotate(118.8deg);-moz-transform:rotate(118.8deg);-ms-transform:rotate(118.8deg);-o-transform:rotate(118.8deg);-webkit-transform:rotate(118.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p34 .bar{transform:rotate(122.4deg);-moz-transform:rotate(122.4deg);-ms-transform:rotate(122.4deg);-o-transform:rotate(122.4deg);-webkit-transform:rotate(122.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p35 .bar{transform:rotate(126deg);-moz-transform:rotate(126deg);-ms-transform:rotate(126deg);-o-transform:rotate(126deg);-webkit-transform:rotate(126deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p36 .bar{transform:rotate(129.6deg);-moz-transform:rotate(129.6deg);-ms-transform:rotate(129.6deg);-o-transform:rotate(129.6deg);-webkit-transform:rotate(129.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p37 .bar{transform:rotate(133.2deg);-moz-transform:rotate(133.2deg);-ms-transform:rotate(133.2deg);-o-transform:rotate(133.2deg);-webkit-transform:rotate(133.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p38 .bar{transform:rotate(136.8deg);-moz-transform:rotate(136.8deg);-ms-transform:rotate(136.8deg);-o-transform:rotate(136.8deg);-webkit-transform:rotate(136.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p39 .bar{transform:rotate(140.4deg);-moz-transform:rotate(140.4deg);-ms-transform:rotate(140.4deg);-o-transform:rotate(140.4deg);-webkit-transform:rotate(140.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p40 .bar{transform:rotate(144deg);-moz-transform:rotate(144deg);-ms-transform:rotate(144deg);-o-transform:rotate(144deg);-webkit-transform:rotate(144deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p41 .bar{transform:rotate(147.6deg);-moz-transform:rotate(147.6deg);-ms-transform:rotate(147.6deg);-o-transform:rotate(147.6deg);-webkit-transform:rotate(147.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p42 .bar{transform:rotate(151.2deg);-moz-transform:rotate(151.2deg);-ms-transform:rotate(151.2deg);-o-transform:rotate(151.2deg);-webkit-transform:rotate(151.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p43 .bar{transform:rotate(154.8deg);-moz-transform:rotate(154.8deg);-ms-transform:rotate(154.8deg);-o-transform:rotate(154.8deg);-webkit-transform:rotate(154.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p44 .bar{transform:rotate(158.4deg);-moz-transform:rotate(158.4deg);-ms-transform:rotate(158.4deg);-o-transform:rotate(158.4deg);-webkit-transform:rotate(158.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p45 .bar{transform:rotate(162deg);-moz-transform:rotate(162deg);-ms-transform:rotate(162deg);-o-transform:rotate(162deg);-webkit-transform:rotate(162deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p46 .bar{transform:rotate(165.6deg);-moz-transform:rotate(165.6deg);-ms-transform:rotate(165.6deg);-o-transform:rotate(165.6deg);-webkit-transform:rotate(165.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p47 .bar{transform:rotate(169.2deg);-moz-transform:rotate(169.2deg);-ms-transform:rotate(169.2deg);-o-transform:rotate(169.2deg);-webkit-transform:rotate(169.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p48 .bar{transform:rotate(172.8deg);-moz-transform:rotate(172.8deg);-ms-transform:rotate(172.8deg);-o-transform:rotate(172.8deg);-webkit-transform:rotate(172.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p49 .bar{transform:rotate(176.4deg);-moz-transform:rotate(176.4deg);-ms-transform:rotate(176.4deg);-o-transform:rotate(176.4deg);-webkit-transform:rotate(176.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p50 .bar{transform:rotate(180deg);-moz-transform:rotate(180deg);-ms-transform:rotate(180deg);-o-transform:rotate(180deg);-webkit-transform:rotate(180deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p51 .bar{transform:rotate(183.6deg);-moz-transform:rotate(183.6deg);-ms-transform:rotate(183.6deg);-o-transform:rotate(183.6deg);-webkit-transform:rotate(183.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p52 .bar{transform:rotate(187.2deg);-moz-transform:rotate(187.2deg);-ms-transform:rotate(187.2deg);-o-transform:rotate(187.2deg);-webkit-transform:rotate(187.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p53 .bar{transform:rotate(190.8deg);-moz-transform:rotate(190.8deg);-ms-transform:rotate(190.8deg);-o-transform:rotate(190.8deg);-webkit-transform:rotate(190.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p54 .bar{transform:rotate(194.4deg);-moz-transform:rotate(194.4deg);-ms-transform:rotate(194.4deg);-o-transform:rotate(194.4deg);-webkit-transform:rotate(194.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p55 .bar{transform:rotate(198deg);-moz-transform:rotate(198deg);-ms-transform:rotate(198deg);-o-transform:rotate(198deg);-webkit-transform:rotate(198deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p56 .bar{transform:rotate(201.6deg);-moz-transform:rotate(201.6deg);-ms-transform:rotate(201.6deg);-o-transform:rotate(201.6deg);-webkit-transform:rotate(201.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p57 .bar{transform:rotate(205.2deg);-moz-transform:rotate(205.2deg);-ms-transform:rotate(205.2deg);-o-transform:rotate(205.2deg);-webkit-transform:rotate(205.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p58 .bar{transform:rotate(208.8deg);-moz-transform:rotate(208.8deg);-ms-transform:rotate(208.8deg);-o-transform:rotate(208.8deg);-webkit-transform:rotate(208.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p59 .bar{transform:rotate(212.4deg);-moz-transform:rotate(212.4deg);-ms-transform:rotate(212.4deg);-o-transform:rotate(212.4deg);-webkit-transform:rotate(212.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p60 .bar{transform:rotate(216deg);-moz-transform:rotate(216deg);-ms-transform:rotate(216deg);-o-transform:rotate(216deg);-webkit-transform:rotate(216deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p61 .bar{transform:rotate(219.6deg);-moz-transform:rotate(219.6deg);-ms-transform:rotate(219.6deg);-o-transform:rotate(219.6deg);-webkit-transform:rotate(219.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p62 .bar{transform:rotate(223.2deg);-moz-transform:rotate(223.2deg);-ms-transform:rotate(223.2deg);-o-transform:rotate(223.2deg);-webkit-transform:rotate(223.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p63 .bar{transform:rotate(226.8deg);-moz-transform:rotate(226.8deg);-ms-transform:rotate(226.8deg);-o-transform:rotate(226.8deg);-webkit-transform:rotate(226.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p64 .bar{transform:rotate(230.4deg);-moz-transform:rotate(230.4deg);-ms-transform:rotate(230.4deg);-o-transform:rotate(230.4deg);-webkit-transform:rotate(230.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p65 .bar{transform:rotate(234deg);-moz-transform:rotate(234deg);-ms-transform:rotate(234deg);-o-transform:rotate(234deg);-webkit-transform:rotate(234deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p66 .bar{transform:rotate(237.6deg);-moz-transform:rotate(237.6deg);-ms-transform:rotate(237.6deg);-o-transform:rotate(237.6deg);-webkit-transform:rotate(237.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p67 .bar{transform:rotate(241.2deg);-moz-transform:rotate(241.2deg);-ms-transform:rotate(241.2deg);-o-transform:rotate(241.2deg);-webkit-transform:rotate(241.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p68 .bar{transform:rotate(244.8deg);-moz-transform:rotate(244.8deg);-ms-transform:rotate(244.8deg);-o-transform:rotate(244.8deg);-webkit-transform:rotate(244.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p69 .bar{transform:rotate(248.4deg);-moz-transform:rotate(248.4deg);-ms-transform:rotate(248.4deg);-o-transform:rotate(248.4deg);-webkit-transform:rotate(248.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p70 .bar{transform:rotate(252deg);-moz-transform:rotate(252deg);-ms-transform:rotate(252deg);-o-transform:rotate(252deg);-webkit-transform:rotate(252deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p71 .bar{transform:rotate(255.6deg);-moz-transform:rotate(255.6deg);-ms-transform:rotate(255.6deg);-o-transform:rotate(255.6deg);-webkit-transform:rotate(255.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p72 .bar{transform:rotate(259.2deg);-moz-transform:rotate(259.2deg);-ms-transform:rotate(259.2deg);-o-transform:rotate(259.2deg);-webkit-transform:rotate(259.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p73 .bar{transform:rotate(262.8deg);-moz-transform:rotate(262.8deg);-ms-transform:rotate(262.8deg);-o-transform:rotate(262.8deg);-webkit-transform:rotate(262.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p74 .bar{transform:rotate(266.4deg);-moz-transform:rotate(266.4deg);-ms-transform:rotate(266.4deg);-o-transform:rotate(266.4deg);-webkit-transform:rotate(266.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p75 .bar{transform:rotate(270deg);-moz-transform:rotate(270deg);-ms-transform:rotate(270deg);-o-transform:rotate(270deg);-webkit-transform:rotate(270deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p76 .bar{transform:rotate(273.6deg);-moz-transform:rotate(273.6deg);-ms-transform:rotate(273.6deg);-o-transform:rotate(273.6deg);-webkit-transform:rotate(273.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p77 .bar{transform:rotate(277.2deg);-moz-transform:rotate(277.2deg);-ms-transform:rotate(277.2deg);-o-transform:rotate(277.2deg);-webkit-transform:rotate(277.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p78 .bar{transform:rotate(280.8deg);-moz-transform:rotate(280.8deg);-ms-transform:rotate(280.8deg);-o-transform:rotate(280.8deg);-webkit-transform:rotate(280.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p79 .bar{transform:rotate(284.4deg);-moz-transform:rotate(284.4deg);-ms-transform:rotate(284.4deg);-o-transform:rotate(284.4deg);-webkit-transform:rotate(284.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p80 .bar{transform:rotate(288deg);-moz-transform:rotate(288deg);-ms-transform:rotate(288deg);-o-transform:rotate(288deg);-webkit-transform:rotate(288deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p81 .bar{transform:rotate(291.6deg);-moz-transform:rotate(291.6deg);-ms-transform:rotate(291.6deg);-o-transform:rotate(291.6deg);-webkit-transform:rotate(291.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p82 .bar{transform:rotate(295.2deg);-moz-transform:rotate(295.2deg);-ms-transform:rotate(295.2deg);-o-transform:rotate(295.2deg);-webkit-transform:rotate(295.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p83 .bar{transform:rotate(298.8deg);-moz-transform:rotate(298.8deg);-ms-transform:rotate(298.8deg);-o-transform:rotate(298.8deg);-webkit-transform:rotate(298.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p84 .bar{transform:rotate(302.4deg);-moz-transform:rotate(302.4deg);-ms-transform:rotate(302.4deg);-o-transform:rotate(302.4deg);-webkit-transform:rotate(302.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p85 .bar{transform:rotate(306deg);-moz-transform:rotate(306deg);-ms-transform:rotate(306deg);-o-transform:rotate(306deg);-webkit-transform:rotate(306deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p86 .bar{transform:rotate(309.6deg);-moz-transform:rotate(309.6deg);-ms-transform:rotate(309.6deg);-o-transform:rotate(309.6deg);-webkit-transform:rotate(309.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p87 .bar{transform:rotate(313.2deg);-moz-transform:rotate(313.2deg);-ms-transform:rotate(313.2deg);-o-transform:rotate(313.2deg);-webkit-transform:rotate(313.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p88 .bar{transform:rotate(316.8deg);-moz-transform:rotate(316.8deg);-ms-transform:rotate(316.8deg);-o-transform:rotate(316.8deg);-webkit-transform:rotate(316.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p89 .bar{transform:rotate(320.4deg);-moz-transform:rotate(320.4deg);-ms-transform:rotate(320.4deg);-o-transform:rotate(320.4deg);-webkit-transform:rotate(320.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p90 .bar{transform:rotate(324deg);-moz-transform:rotate(324deg);-ms-transform:rotate(324deg);-o-transform:rotate(324deg);-webkit-transform:rotate(324deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p91 .bar{transform:rotate(327.6deg);-moz-transform:rotate(327.6deg);-ms-transform:rotate(327.6deg);-o-transform:rotate(327.6deg);-webkit-transform:rotate(327.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p92 .bar{transform:rotate(331.2deg);-moz-transform:rotate(331.2deg);-ms-transform:rotate(331.2deg);-o-transform:rotate(331.2deg);-webkit-transform:rotate(331.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p93 .bar{transform:rotate(334.8deg);-moz-transform:rotate(334.8deg);-ms-transform:rotate(334.8deg);-o-transform:rotate(334.8deg);-webkit-transform:rotate(334.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p94 .bar{transform:rotate(338.4deg);-moz-transform:rotate(338.4deg);-ms-transform:rotate(338.4deg);-o-transform:rotate(338.4deg);-webkit-transform:rotate(338.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p95 .bar{transform:rotate(342deg);-moz-transform:rotate(342deg);-ms-transform:rotate(342deg);-o-transform:rotate(342deg);-webkit-transform:rotate(342deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p96 .bar{transform:rotate(345.6deg);-moz-transform:rotate(345.6deg);-ms-transform:rotate(345.6deg);-o-transform:rotate(345.6deg);-webkit-transform:rotate(345.6deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p97 .bar{transform:rotate(349.2deg);-moz-transform:rotate(349.2deg);-ms-transform:rotate(349.2deg);-o-transform:rotate(349.2deg);-webkit-transform:rotate(349.2deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p98 .bar{transform:rotate(352.8deg);-moz-transform:rotate(352.8deg);-ms-transform:rotate(352.8deg);-o-transform:rotate(352.8deg);-webkit-transform:rotate(352.8deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p99 .bar{transform:rotate(356.4deg);-moz-transform:rotate(356.4deg);-ms-transform:rotate(356.4deg);-o-transform:rotate(356.4deg);-webkit-transform:rotate(356.4deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100.p100 .bar{transform:rotate(360deg);-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-o-transform:rotate(360deg);-webkit-transform:rotate(360deg);transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100:hover{cursor:default}.c100:hover>span{width:3.33em;line-height:3.33em;font-size:.3em;color:#fff;transition:.2s ease-out;-moz-transition:.2s ease-out;-webkit-transition:.2s ease-out;-o-transition:.2s ease-out}.c100:hover:after{top:.07em;left:.07em;width:.86em;height:.86em}.c100.blue .bar,.c100.blue .fill{border-color:red!important}"]
                    }] }
        ];
        /** @nocollapse */
        LoadingCircleComponent.ctorParameters = function () { return []; };
        return LoadingCircleComponent;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        LoadingCircleComponent.prototype._percentage;
        /** @type {?} */
        LoadingCircleComponent.prototype.ngClassObj;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/components/akiry-player/akiry-player.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var DEFAULT_TIME_TO_HIDE_CONTROLS = 3000;
    /**
     * @record
     */
    function ControlsListener() { }
    if (false) {
        /** @type {?|undefined} */
        ControlsListener.prototype.showControls;
        /** @type {?|undefined} */
        ControlsListener.prototype.timeToHide;
    }
    var AkiryPlayerComponent = /** @class */ (function () {
        function AkiryPlayerComponent(akiryConsole, _renderer) {
            this.akiryConsole = akiryConsole;
            this._renderer = _renderer;
            this._consoleCallerCount = 0;
            this._timeToHideControls = 0;
            this._isShowing = false;
            this.setConsoleCaller();
        }
        /**
         * @return {?}
         */
        AkiryPlayerComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            this.setEvents();
            this.setPlayerInternalCallbacks();
        };
        /**
         * @return {?}
         */
        AkiryPlayerComponent.prototype.play = /**
         * @return {?}
         */
        function () {
            this._akiryPlayer.playOrPause();
        };
        /**
         * @param {?} url
         * @param {?} opts
         * @return {?}
         */
        AkiryPlayerComponent.prototype.load = /**
         * @param {?} url
         * @param {?} opts
         * @return {?}
         */
        function (url, opts) {
            this._akiryPlayer.load(url, opts);
        };
        /**
         * @return {?}
         */
        AkiryPlayerComponent.prototype.release = /**
         * @return {?}
         */
        function () {
            this._akiryPlayer.release();
        };
        /**
         * @private
         * @return {?}
         */
        AkiryPlayerComponent.prototype.setConsoleCaller = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            document.addEventListener('touchstart', (/**
             * @param {?} zEvent
             * @return {?}
             */
            function (zEvent) {
                if (_this._consoleCallerCount > 1) {
                    _this.akiryConsole.isVisible = !_this.akiryConsole.isVisible;
                    _this._consoleCallerCount = 0;
                }
                _this._lastClickInit = new Date();
            }));
            document.addEventListener('touchend', (/**
             * @param {?} zEvent
             * @return {?}
             */
            function (zEvent) {
                if (_this._lastClickInit && Date.now() - _this._lastClickInit.valueOf() > 4000) {
                    _this._consoleCallerCount++;
                }
                _this._lastClickInit = undefined;
            }));
            document.addEventListener('keydown', (/**
             * @param {?} zEvent
             * @return {?}
             */
            function (zEvent) {
                // console.log('keydown - SHOW')
                _this.showControls();
                if (zEvent.ctrlKey && zEvent.altKey && zEvent.code === 'KeyD') {
                    _this.akiryConsole.isVisible = !_this.akiryConsole.isVisible;
                }
            }));
        };
        /**
         * @private
         * @return {?}
         */
        AkiryPlayerComponent.prototype.setEvents = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            /** @type {?} */
            var section = this.section.nativeElement;
            section.addEventListener('mouseover', (/**
             * @param {?} zEvent
             * @return {?}
             */
            function (zEvent) {
                // console.log('over - SHOW')
                _this.showControls();
            }));
            section.addEventListener('mousemove', (/**
             * @param {?} zEvent
             * @return {?}
             */
            function (zEvent) {
                // console.log('move - SHOW')
                _this.showControls();
            }));
            // section.addEventListener('mouseleave', (zEvent) => {
            //   // console.log('leave - HIDE')
            //   this.hideControls();
            // });
            section.addEventListener('click', (/**
             * @param {?} zEvent
             * @return {?}
             */
            function (zEvent) {
                // this._akiryPlayer.playOrPause();
                _this.showControls();
            }));
        };
        /**
         * @return {?}
         */
        AkiryPlayerComponent.prototype.showControls = /**
         * @return {?}
         */
        function () {
            this._timeToHideControls =
                Date.now() + (this.controlsListener && this.controlsListener.timeToHide ?
                    this.controlsListener.timeToHide : DEFAULT_TIME_TO_HIDE_CONTROLS);
            if (!this._isShowing) {
                this._renderer.setStyle(this.topControls.nativeElement, 'opacity', 1);
                this._renderer.setStyle(this.bottomControls.nativeElement, 'opacity', 1);
                if (this.controlsListener && this.controlsListener.showControls) {
                    this.controlsListener.showControls(true);
                }
                this._isShowing = true;
                this.controlTimerWatch();
            }
        };
        /**
         * @return {?}
         */
        AkiryPlayerComponent.prototype.hideControls = /**
         * @return {?}
         */
        function () {
            this._renderer.setStyle(this.topControls.nativeElement, 'opacity', 0);
            this._renderer.setStyle(this.bottomControls.nativeElement, 'opacity', 0);
            if (this.controlsListener && this.controlsListener.showControls) {
                this.controlsListener.showControls(false);
            }
            this._isShowing = false;
        };
        /**
         * @private
         * @return {?}
         */
        AkiryPlayerComponent.prototype.controlTimerWatch = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            if (Date.now() > this._timeToHideControls) {
                this.hideControls();
            }
            else {
                setTimeout((/**
                 * @return {?}
                 */
                function () {
                    _this.controlTimerWatch();
                }), 500);
            }
        };
        /**
         * @return {?}
         */
        AkiryPlayerComponent.prototype.clickPlayButton = /**
         * @return {?}
         */
        function () {
            this._akiryPlayer.playOrPause();
            return false;
        };
        /**
         * @return {?}
         */
        AkiryPlayerComponent.prototype.clickFullscreenButton = /**
         * @return {?}
         */
        function () {
            this._akiryPlayer.fullscreen();
        };
        /**
         * @private
         * @return {?}
         */
        AkiryPlayerComponent.prototype.setPlayerInternalCallbacks = /**
         * @private
         * @return {?}
         */
        function () {
            var _this = this;
            this._akiryPlayer.setOnTimeUpdatedListener((/**
             * @param {?} currentTime
             * @param {?} bufferedTime
             * @return {?}
             */
            function (currentTime, bufferedTime) {
                /** @type {?} */
                var timeInBuffer = bufferedTime < currentTime ? 0 : bufferedTime - currentTime;
                _this.libLoading.percentage = timeInBuffer / 15 < 1 ? Math.ceil(((5 + timeInBuffer) / 20) * 100) : 100;
                if (_this.libLoading.percentage >= 100 && ((/** @type {?} */ (_this.divLoading.nativeElement))).style.display !== 'none') {
                    _this._renderer.setStyle(_this.divLoading.nativeElement, 'display', 'none');
                    _this._akiryPlayer.play();
                }
            }));
            this._akiryPlayer.setOnWaitingListener((/**
             * @return {?}
             */
            function () {
                _this.startLoading();
            }));
        };
        /**
         * @private
         * @return {?}
         */
        AkiryPlayerComponent.prototype.startLoading = /**
         * @private
         * @return {?}
         */
        function () {
            this.libLoading.percentage = 0;
            this._renderer.setStyle(this.divLoading.nativeElement, 'display', 'flex');
            this._akiryPlayer.pause();
        };
        AkiryPlayerComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'lib-akiry-player',
                        template: "<section #sectionControl>\n  <video libAkiryPlayer #akiryPlayer id='akiryPlayerId' preload playsinline controls>\n    <!-- <p class=\"vjs-no-js\">To view this video, please enable JavaScript, and consider upgrading to a web browser that <a href=\"http://videojs.com/html5-video-support/\">supports HTML5 video</a></p> -->\n    <!-- <source\n      src=\"http://10.42.0.1/videos/live3/live3.m3u8\"\n      type=\"application/x-mpegURL\"> -->\n  </video>\n\n  <div class='akiry-player-warns' #divLoading>\n    <div class='put-center'>\n      <lib-loading-circle #libLoading></lib-loading-circle>\n    </div>\n  </div>\n\n  <div class='akiry-player-controls'>\n    <div class='top' #topControls>\n\n    </div>\n    <div class='put-center' #centerControls>\n      \n    </div>\n    <div class='put-bottom'>\n      <div class='bottom' #bottomControls>\n        <button (click)='clickPlayButton()'>PLAY</button>\n        <button (click)='clickFullscreenButton()'>FULLSCREEN</button>\n      </div>\n      <div class='transparent-body'></div>\n    </div>\n  </div>\n\n  <!-- <lib-akiry-player-console *ngIf=\"akiryConsole.isVisible\"></lib-akiry-player-console> -->\n</section>",
                        styles: ["section{background-color:#000;display:-webkit-box;display:flex;-webkit-box-pack:center;justify-content:center;width:100%;height:100vh}video{width:100%;height:100vh}.video-js{position:relative!important;width:100%!important;height:auto!important}.akiry-player-warns{position:absolute;width:100%;height:100vh;background-color:rgba(255,0,0,0);padding:3% 5% 0;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center}.akiry-player-warns .put-center{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center}.akiry-player-controls{position:absolute;width:100%;height:100vh;background-color:rgba(255,0,0,0);padding:3% 5% 0;display:none;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:justify;justify-content:space-between}.akiry-player-controls .top{background-color:rgba(0,255,0,.5);width:100%;height:100px;-webkit-transition:.2s linear;transition:.2s linear;opacity:0}.akiry-player-controls .put-center{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-pack:center;justify-content:center;-webkit-box-align:center;align-items:center}.akiry-player-controls .put-bottom{width:100%;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:reverse;flex-direction:column-reverse}.akiry-player-controls .put-bottom .transparent-body{width:100%;height:100%}.akiry-player-controls .put-bottom .bottom{background-color:rgba(0,0,255,.5);width:100%;height:70px;-webkit-transition:.2s linear;transition:.2s linear;opacity:0}"]
                    }] }
        ];
        /** @nocollapse */
        AkiryPlayerComponent.ctorParameters = function () { return [
            { type: AkiryPlayerConsoleService },
            { type: core.Renderer2 }
        ]; };
        AkiryPlayerComponent.propDecorators = {
            _akiryPlayer: [{ type: core.ViewChild, args: [AkiryPlayerDirective, { static: true },] }],
            topControls: [{ type: core.ViewChild, args: ['topControls', { static: true },] }],
            bottomControls: [{ type: core.ViewChild, args: ['bottomControls', { static: true },] }],
            section: [{ type: core.ViewChild, args: ['sectionControl', { static: true },] }],
            libLoading: [{ type: core.ViewChild, args: ['libLoading', { static: true },] }],
            divLoading: [{ type: core.ViewChild, args: ['divLoading', { static: true },] }],
            controlsListener: [{ type: core.Input }]
        };
        return AkiryPlayerComponent;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype._akiryPlayer;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype.topControls;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype.bottomControls;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype.section;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype.libLoading;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype.divLoading;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype.controlsListener;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype._consoleCallerCount;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype._lastClickInit;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype._timeToHideControls;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype._isShowing;
        /** @type {?} */
        AkiryPlayerComponent.prototype.akiryConsole;
        /**
         * @type {?}
         * @private
         */
        AkiryPlayerComponent.prototype._renderer;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/components/akiry-player-console/akiry-player-console.component.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AkiryPlayerConsoleComponent = /** @class */ (function () {
        function AkiryPlayerConsoleComponent(akiryConsole) {
            this.akiryConsole = akiryConsole;
        }
        /**
         * @return {?}
         */
        AkiryPlayerConsoleComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () { };
        AkiryPlayerConsoleComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'lib-akiry-player-console',
                        template: "<section>\n    <h4>Akiry Secret Console Logs</h4>\n    <div class='fixed-infos'>\n        {{akiryConsole.playerStatusInfos}}\n    </div>\n    <div class='rolling-infos'>\n        <p *ngFor='let log of akiryConsole.logs'>{{log}}</p>\n    </div>\n</section>",
                        styles: ["section{position:fixed!important;top:20vh!important;left:5%!important;z-index:100;background-color:rgba(0,0,0,.5);color:#fff;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-align:start;align-items:flex-start;-webkit-box-pack:start;justify-content:flex-start;width:90%;margin:0;height:60vh}.fixed-infos{width:100%;font-size:12px;white-space:pre-line}.rolling-infos{overflow-y:scroll;width:100%}"]
                    }] }
        ];
        /** @nocollapse */
        AkiryPlayerConsoleComponent.ctorParameters = function () { return [
            { type: AkiryPlayerConsoleService }
        ]; };
        return AkiryPlayerConsoleComponent;
    }());
    if (false) {
        /** @type {?} */
        AkiryPlayerConsoleComponent.prototype.akiryConsole;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/akiry-player.module.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AkiryPlayerModule = /** @class */ (function () {
        function AkiryPlayerModule() {
        }
        AkiryPlayerModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [
                            platformBrowser.BrowserModule,
                        ],
                        declarations: [
                            AkiryPlayerComponent,
                            AkiryPlayerConsoleComponent,
                            AkiryPlayerDirective,
                            LoadingCircleComponent,
                        ],
                        providers: [
                            AkiryPlayerConsoleService,
                        ],
                        exports: [
                            AkiryPlayerComponent,
                        ],
                    },] }
        ];
        return AkiryPlayerModule;
    }());

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/browser/console-logs-manager.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var printConsoleLogs = (/**
     * @param {?=} logElementId
     * @param {?=} breakline
     * @return {?}
     */
    function (logElementId, breakline) {
        // Reference to an output container, use 'pre' styling for JSON output
        /** @type {?} */
        var output = logElementId ? document.getElementById(logElementId) : document.createElement('pre');
        /** @type {?} */
        var breakline = breakline ? breakline : '<br/>';
        if (!logElementId)
            document.body.appendChild(output);
        // Reference to native method(s)
        /** @type {?} */
        var oldLog = console.log;
        console.log = (/**
         * @param {...?} items
         * @return {?}
         */
        function () {
            var items = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                items[_i] = arguments[_i];
            }
            // Call native method first
            oldLog.apply(this, items);
            // Use JSON to transform objects, all others display normally
            items.forEach((/**
             * @param {?} item
             * @param {?} i
             * @return {?}
             */
            function (item, i) {
                items[i] = (typeof item === 'object' ? JSON.stringify(item, null, 4) : item);
            }));
            output.innerHTML += items.join(' ') + breakline + '-> ';
        });
        // You could even allow Javascript input...
        /**
         * @param {?} data
         * @return {?}
         */
        function consoleInput(data) {
            // Print it to console as typed
            console.log(data + breakline);
            try {
                console.log(eval(data));
            }
            catch (e) {
                console.log(e.stack);
            }
        }
    });
    var ɵ0 = printConsoleLogs;

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/services/akiry-player/akiry-player.service.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AkiryPlayerService = /** @class */ (function () {
        function AkiryPlayerService() {
        }
        AkiryPlayerService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root',
                    },] }
        ];
        /** @nocollapse */
        AkiryPlayerService.ctorParameters = function () { return []; };
        /** @nocollapse */ AkiryPlayerService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function AkiryPlayerService_Factory() { return new AkiryPlayerService(); }, token: AkiryPlayerService, providedIn: "root" });
        return AkiryPlayerService;
    }());

    exports.AkiryPlayerComponent = AkiryPlayerComponent;
    exports.AkiryPlayerModule = AkiryPlayerModule;
    exports.AkiryPlayerService = AkiryPlayerService;
    exports.C = C;
    exports.DEFAULT_TIME_TO_HIDE_CONTROLS = DEFAULT_TIME_TO_HIDE_CONTROLS;
    exports.iOSTools = iOSTools;
    exports.printConsoleLogs = printConsoleLogs;
    exports.ɵa = AkiryPlayerDirective;
    exports.ɵb = AkiryPlayerConsoleService;
    exports.ɵc = AkiryPlayerConsoleComponent;
    exports.ɵd = LoadingCircleComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=akiry-player.umd.js.map
